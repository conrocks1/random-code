def dirlink():
    components().okbox('Select the Directory', ['Select the folder to create the link for.'])
    ddir = files('return', 'directory')
    name = components().inputbox('Name the Link', ['Enter a name for your link.', 'This will be the Applications icon name.'])
    cmd = components().inputbox('Name the Command', ['Enter a name for your command. No spaces are allowed.', 'This will be the executable command.'])
    os.mkdir('res/' + cmd + '/')
    os.mkdir('data/' + cmd + '/')
    rf = open('res/' + cmd + '/req.txt', 'w')
    print >> rf, 'req.txt'
    print >> rf, 'PythonOS.files("browse", "file", "' + ddir + '")'
    print >> rf, cmd
    print >> rf, 'res/dirlink/dl.png'
    print >> rf, name
    print >> rf, '1.0'
    c.addcmd(cmd, 'PythonOS.files("browse", "file", "' + ddir + '")')
    rf.close()
    scmd = cmd.split(' .,  -')
    ncmd = ''
    for part in scmd:
        scmd = part + '_'
        pvar.append('INSTALLEDAPPS', cmd, 'GLOBAL')
    components().okbox('Link added', ['Restart Python OS to use the link.', 'The link is installed as a command.', 'Use uninstall to remove it.'])
