def hola_greeter():
    global toolbarplugs
    global toolbarplugstarters
    gserver()
    gui().boot(True)
    toolbarplugs = []
    toolbarplugstarters = []
    hbgimg = pygame.transform.smoothscale(pygame.image.load('res/hola-greeter/hola-bg.png'), (width, height))
    enter = pygame.image.load('res/hola-greeter/enter.png')
    entercenter = centerobj(40, 40, screen)
    unb = components().inputbutton(50, 50, 'User Name')
    unbcenter = centerobj(unb[1] - unb[0], unb[3] - unb[2], screen)
    pwb = components().inputbutton(50, 50, 'Password')
    pwbcenter = centerobj(pwb[1] - pwb[0], pwb[3] - pwb[2], screen)
    uname = ''
    pwd = ''
    lps().new('Hola! Greeter')
    while True:
        clock.tick(fps)
        appsurface.blit(hbgimg, [0, 0])
        if uname == '':
            unb = components().inputbutton(unbcenter[0] - 200, unbcenter[1], 'User Name')
        elif uname != '':
            unb = components().inputbutton(unbcenter[0] - 200, unbcenter[1], uname)
        pwb = components().inputbutton(pwbcenter[0] + 200, pwbcenter[1], 'Password')
        enb = components().imagebutton(entercenter[0], entercenter[1], enter)
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                commands.parsecmd(['killg'])
                return
            if event.type == pygame.MOUSEBUTTONDOWN:
                mousepos = pygame.mouse.get_pos()
                if mousepos[0] >= unb[0] and mousepos[0] <= unb[1]:
                    if mousepos[1] >= unb[2] and mousepos[1] <= unb[3]:
                        uname = components().getinput(unbcenter[0] - 200, unbcenter[1], uname)
                if mousepos[0] >= pwb[0] and mousepos[0] <= pwb[1]:
                    if mousepos[1] >= pwb[2] and mousepos[1] <= pwb[3]:
                        pwd = components().getinput(pwbcenter[0] + 200, pwbcenter[1], pwd)
                if mousepos[0] >= enb[0] and mousepos[0] <= enb[1]:
                    if mousepos[1] >= enb[2] and mousepos[1] <= enb[3]:
                        lps().end()
                        lps().end()
                        return uname, pwd

