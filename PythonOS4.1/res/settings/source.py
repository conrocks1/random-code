def settings():
    lps().new('Settings')
    global fps
    global bg
    global screen
    global width
    global height
    global full
    global appsurface
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        b1 = components().button(20, 20, 'Change FPS of applications')
        b2 = components().button(20, b1[3] + 2, 'Change the Desktop background')
        b3 = components().button(20, b2[3] + 2, 'Graphics Auto-Start')
        b4 = components().button(20, b3[3] + 2, 'Toggle Fullscreen')
        b5 = components().button(20, b4[3] + 2, 'Change Copy/Move chunk size')
        b6 = components().button(20, b5[3] + 2, 'Toggle Debug Mode')
        b7 = components().button(20, b6[3] + 2, 'Applications Background Transparency')
        screen.blit(appsurface, [2, 20])
	components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.VIDEORESIZE:
                width = event.w
                height = event.h
                appsurface = pygame.Surface((width - 2, height - 20))
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        return
                if mousepos[0] >= b1[0] and mousepos[0] <= b1[1]:
                    if mousepos[1] >= b1[2] and mousepos[1] <= b1[3]:
                        pvar.make('FPS', components().inputbox('Set the FPS of applications', ['Enter the Flips Per Second of applications.', 'Default: 10']), 'SETTINGS')
                        fps = int(pvar.read('FPS', 'SETTINGS')[0])
                if mousepos[0] >= b2[0] and mousepos[0] <= b2[1]:
                    if mousepos[1] >= b2[2] and mousepos[1] <= b2[3]:
                        path = files('return')
                        if c.readfile(path, 'r') != None:
                            pvar.make('DESKTOPBG', path, 'SETTINGS')
                            bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
                if mousepos[0] >= b3[0] and mousepos[0] <= b3[1]:
                    if mousepos[1] >= b3[2] and mousepos[1] <= b3[3]:
                        answer = components().ynbox('Start Graphics at login?', ['Do you want to start the Desktop when you log in?'])
                        if answer == 'Yes':
                            pvar.make('RUNG', 'True', 'SETTINGS')
                        if answer == 'No':
                            pvar.make('RUNG', 'False', 'SETTINGS')
                if mousepos[0] >= b4[0] and mousepos[0] <= b4[1]:
                    if mousepos[1] >= b4[2] and mousepos[1] <= b4[3]:
                        full = c.toggle_full()
                        if full == True:
                            pvar.make('FULLSCREEN', 'yes', 'SETTINGS')
                        if full == False:
                            pvar.make('FULLSCREEN', 'no', 'SETTINGS')
                        width = screen.get_width()
                        height = screen.get_height()
                        appsurface = pygame.Surface((width - 2, height - 20))
                if mousepos[0] >= b5[0] and mousepos[0] <= b5[1]:
                    if mousepos[1] >= b5[2] and mousepos[1] <= b5[3]:
                        pvar.make('BUFFERSIZE', components().inputbox('Set the Copy/Move Buffer Size', ['Enter the buffer size.', 'Default: 1000']), 'COPY')
                if mousepos[0] >= b6[0] and mousepos[0] <= b6[1]:
                    if mousepos[1] >= b6[2] and mousepos[1] <= b6[3]:
                        ans = components().ynbox('Debug Mode', ['Do you want debug mode to be on?', 'For this to work, debugd needs to be installed.'])
                        if ans == 'Yes':
                            pvar.make('DEBUGMODE', 'True', 'KERNEL')
                            c.varman().make('debugmode', True)
                            daemonman().add('debugd')
                        if ans != 'Yes':
                            pvar.make('DEBUGMODE', 'False', 'KERNEL')
                            c.varman().make('debugmode', False)
                if mousepos[0] >= b7[0] and mousepos[0] <= b7[1]:
                    if mousepos[1] >= b7[2] and mousepos[1] <= b7[3]:
                        ans = components().ynbox('Transparent Background', ['Do you want the Apps background to be transparent?'])
                        if ans == 'Yes':
                            pvar.make('TRANSPARENT', 'True', 'APPS')
                        if ans != 'Yes':
                            pvar.make('TRANSPARENT', 'False', 'APPS')
