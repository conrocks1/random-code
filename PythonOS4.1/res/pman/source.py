class pman():
    def category(self, cat, opts):
        print 'Working...'
        import urllib
        mirrors = c.readfile('res/pman/mirrors.txt')
        cmir = 0
        for mirror in mirrors:
            try:
                code = urllib.urlopen(mirror + cat + '.txt').code
                if (code / 100 >= 4):
                   raise RuntimeError
                urllib.urlretrieve(mirror + cat + '.txt', 'temp/cat.txt')
            except:
                c.msg(cat + ' is not available on mirror #' + str(cmir + 1), 'PMAN')
                cmir = cmir + 1
                continue
            if opts == 'v':
                if shell.grunning == True:
                    components().okbox('Category downloaded.', ['Category downloaded to temp/cat.txt'])
                if shell.grunning == False:
                    print 'Apps in this category:'
                    for ln in c.readfile('temp/cat.txt'):
                        print ln
            if opts == 'ri':
                for app in c.readfile('temp/cat.txt'):
                    pman().getapp(app)
            if opts == 'u':
                for app in c.readfile('temp/cat.txt'):
                    c.uninstall(app, '-n')
            return
    def printver(self):
        print 'PMan: Application Manager for Python OS 4x.'
        print '---------       Version 1.1'
        print '| .zip  |       For Python OS 4.1+'
        print '| $app  |       By Adam Furman.'
        print '---------       Server http://adamfurman.privatepage.sk/PythonOS/Files/Applications/'
    def install(self, path, silent = False):
        commands.parsecmd(['install', path])
        if silent == False:
            if shell.grunning == True:
                components().okbox('Install finished', ['The application has been installed'])
            if shell.grunning == False:
                c.msg('Application Installed', 'PMAN')
        if shell.grunning == True:
            reload_data(True)
        if shell.grunning == False:
            commands.parsecmd('reload-pyos')
    def uninstall(self, name):
        commands.parsecmd(['uninstall', name])
        if shell.grunning == True:
            components().okbox('Uninstall finished', ['The application has been uninstalled'])
        if shell.grunning == False:
            c.msg('Application Uninstalled', 'PMAN')
        if shell.grunning == True:
            reload_data(True)
        if shell.grunning == False:
            commands.parsecmd('reload-pyos')
    def updateapps(self):
        if shell.grunning == True:
            components().hangscreen('Updating Apps', ['PMan is querying, downloading, and applying updates.'])
        c.msg('Querying Updates', 'PMAN')
        import shutil
        shutil.copy('var/GLOBAL-INSTALLEDAPPS.var', 'data/pman/apps.var')
        import urllib
        il = pvar.read('INSTALLEDAPPS', 'GLOBAL')
        av = []
        for app in il:
            apv = float(c.readfile('res/' + app + '/req.txt')[5])
            av.append([app, apv])
        mirrors = c.readfile('res/pman/mirrors.txt')
        cmir = 1
        for mirror in mirrors:
            print 'Mirror #' + str(cmir) + ': ' + mirror
            cmir = cmir + 1
        cmir = 1
        for tra in il:
            c.msg('Checking updates for ' + tra, 'PMAN')
            cmir = 1
            for mirror in mirrors:
                try:
                    code = urllib.urlopen(mirror + tra + '/req.txt').code
                    if (code / 100 >= 4):
                       raise RuntimeError
                    urllib.urlretrieve(mirror + tra + '/req.txt', 'temp/netreq.txt')
                    laver = float(c.readfile('res/' + tra + '/req.txt')[5])
                    sver = float(c.readfile('temp/netreq.txt')[5])
                    if sver > laver:
                        commands.parsecmd(['uninstall', tra, '-kd'])
                        c.msg('Downloading updates for ' + tra, 'PMAN')
                        pman().getapp(tra, True)
                        c.deletefile('data/pman/app.zip')
                        shutil.copy('data/pman/apps.var', 'var/GLOBAL-INSTALLEDAPPS.var')
                        c.msg('Updated ' + tra, 'PMAN')
                except:
                    c.msg(tra + ' is not available on mirror #' + str(cmir), 'PMAN')
                    cmir = cmir + 1
                    continue
        if shell.grunning == True:
            reload_data(True)
        if shell.grunning == False:
            commands.parsecmd('reload-pyos')
    def getapp(self, name, ows = False):
        if ows == False:
            if shell.grunning == True:
                components().hangscreen('Downloading App', ['PMan is downloading the requested app.'])
        import urllib
        c.msg('Downloading', 'PMAN')
        mirrors = c.readfile('res/pman/mirrors.txt')
        cmir = 1
        for mirror in mirrors:
            print 'Mirror #' + str(cmir) + ': ' + mirror
            cmir = cmir + 1
        cmir = 1
        for mirror in mirrors:
            try:
                print 'Trying mirror #' + str(cmir)
                code = urllib.urlopen(mirror + name + '/'+ name + '.zip').code
                if (code / 100 >= 4):
                   raise RuntimeError
                urllib.urlretrieve(mirror + name + '/' + name + '.zip', 'data/pman/app.zip')
                break
            except:
                cmir = cmir + 1
                pass
        c.msg('Installing', 'PMAN')
        pman().install('data/pman/app.zip', True)
    def pgui(self):
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        lps().new('Application Manager')
        installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
        inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
        pages = [[[]]]
        h = 70
        w = 22
        page = 0
        subpage = 0
        for item in installed:
            if h < height - 20:
                pages[page][subpage].append(item)
            elif h >= height - 20 and w < width - 300:
                h = 70
                w = w + 300
                subpage = subpage + 1
                pages[page].append([])
                pages[page][subpage].append(item)
            elif w >= width - 300:
                pages.append([[]])
                page = page + 1
                subpage = 0
                pages[page][subpage].append(item)
                h = 70
                w = 22
            h = h + 15
            h = 70
            w = 22
            page = 0
            subpage = 0
        while True:
            clock.tick(fps)
            appsurface.fill([50, 50, 50])
            subpage = 0
            w = 2
            h = 70
            for i in pages[page][subpage]:
                appsurface.blit(font.render(i, 1, (200, 200, 200)), [w, h])
                if h >= height - 50:
                    subpage = subpage + 1
                    w = w + 300
                    h = 70 - 15
                h = h + 15
            ub = components().button(2, 2, 'Uninstall')
            ib = components().button(ub[1] + 2, 2, 'Install')
            upb = components().button(ib[1] + 2, 2, 'Check for Updates')
            fb = components().button(upb[1] + 2, 2, 'Fetch App')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            mousepos = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return
                    if event.key == pygame.K_LEFT:
                        if page != 0:
                            page = page - 1
                    if event.key == pygame.K_RIGHT:
                        if len(pages) - 1 != page:
                            page = page + 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return
                        if mousepos[0] >= ub[0] and mousepos[0] <= ub[1]:
                            if mousepos[1] >= ub[2] and mousepos[1] <= ub[3]:
                                name = components().inputbox('Application Name', ['Enter the name of the application to uninstall.'])
                                if name != 'Cancel':
                                    ask = components().ynbox('Are you sure?', ['Are you sure you want to uninstall:', name, "This will delete all it's data"])
                                    if ask == 'Yes':
                                        pman().uninstall(name)
                                        installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                        sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                        for a in sidemenuitems:
                                            if a == name:
                                                pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                                for ca in sidemenuitems:
                                                    if ca != name and ca != 'appmenu':
                                                        pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                        sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                        sidemenufinalitems = []
                                        for i in sidemenuitems:
                                            rf = c.readfile('res/' + i + '/req.txt', 'r')
                                            icon = rf[3]
                                            name = rf[4]
                                            execp = rf[2]
                                            sidemenufinalitems.append([name, icon, execp])
                                        installedappsfinal = []
                                        for inst in installedapps:
                                            rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                            icon = rf[3]
                                            name = rf[4]
                                            execp = rf[2]
                                            if rf[3] != 'noicon':
                                                installedappsfinal.append([name, icon, execp])
                                        installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                        inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                        pages = [[[]]]
                                        h = 70
                                        w = 22
                                        page = 0
                                        subpage = 0
                                        for item in installed:
                                            if h < height - 20:
                                                pages[page][subpage].append(item)
                                            elif h >= height - 20 and w < width - 300:
                                                h = 70
                                                w = w + 300
                                                subpage = subpage + 1
                                                pages[page].append([])
                                                pages[page][subpage].append(item)
                                            elif w >= width - 300:
                                                pages.append([[]])
                                                page = page + 1
                                                subpage = 0
                                                pages[page][subpage].append(item)
                                                h = 70
                                                w = 22
                                            h = h + 15
                                            h = 70
                                            w = 22
                                            page = 0
                                            subpage = 0
                        if mousepos[0] >= ib[0] and mousepos[0] <= ib[1]:
                            if mousepos[1] >= ib[2] and mousepos[1] <= ib[3]:
                                components().okbox('Application Path', ['Enter the filepath of the application to install.', 'This should be a .zip file.'])
                                path = files('return')
                                pman().install(path)
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in installed:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                        if mousepos[0] >= upb[0] and mousepos[0] <= upb[1]:
                            if mousepos[1] >= upb[2] and mousepos[1] <= upb[3]:
                                pman().updateapps()
                        if mousepos[0] >= fb[0] and mousepos[0] <= fb[1]:
                            if mousepos[1] >= fb[2] and mousepos[1] <= fb[3]:
                                an = components().inputbox('App Name', ['Enter the name of the app you wish to get.'])
                                pman().getapp(an)
                                installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in installed:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
