def nestedsession():
    import shell
    if shell.grunning == True:
	yes = False
        if components().ynbox('Launch a Nesdted Session?', ['Launching a nested session will freeze graphics.']) == 'Yes':
            yes = True
    if shell.grunning == False:
        yes = True
    if yes == True:
        import runpy
        import pvar
        pvar.make('NSR', 'yes', 'NESTEDSESSION')
        runpy.run_path('kernel.py')
        pvar.destroy('NSR', 'NESTEDSESSION')
