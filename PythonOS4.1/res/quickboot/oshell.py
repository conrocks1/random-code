import pvar
import PythonOS
import constants
import commands
import sys
import traceback
import shutil

grunning = False
cdir = ''
user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]

class interact():
    def sh(self, cmd = ''):
        shutil.copy2('data/system/' + user + '/SIDEMENU-SIDEMENUITEMS.var', 'var/SIDEMENU-SIDEMENUITEMS.var')
        shutil.copy2('data/system/' + user + '/SETTINGS-FULLSCREEN.var', 'var/SETTINGS-FULLSCREEN.var')
        shutil.copy2('data/system/' + user + '/SETTINGS-DESKTOPBG.var', 'var/SETTINGS-DESKTOPBG.var')
        shutil.copy2('data/system/' + user + '/SETTINGS-FPS.var', 'var/SETTINGS-FPS.var')
        shutil.copy2('PythonOS.py', 'data/system/PythonOS.py')
        shutil.copy2('commands.py', 'data/system/commands.py')
        shutil.copy2('kernel.py', 'data/system/kernel.py')
        shutil.copy2('constants.py', 'data/system/constants.py')
        shutil.copy2('shell.py', 'data/system/shell.py')
        shutil.rmtree('data/system/varbackup/', True)
        shutil.copytree('var/', 'data/system/varbackup/')
        while cmd != 'exit':
            cmd = raw_input (user + '@pythonos ' + cdir + '$ ')
            scmd = cmd.split()
            try:
                commands.parsecmd(scmd)
                constants.logentry('SHELL', 'The command "' + cmd + '" ran without errors.')
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                nf = constants.makefile('temp/traceback.txt')
                tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
                for ln in tb:
                    print >> nf, ln
                nf.close()
                constants.logentry('SHELL', 'The command "' + cmd + '" encountered an error. The error is logged at temp/traceback.txt')
                if cmd != 'startg':
                    constants.msg('The command ' + cmd + ' has crashed. Error Traceback:', 'SHELL')
                    traceback.print_exception(exc_type, exc_value, exc_traceback)
                    constants.msg('This traceback is saved to temp/traceback.txt.', 'SHELL')
                if cmd == 'startg':
                    constants.msg('Graphics session ended.', 'SHELL')
                    global grunning
                    grunning = False
        shutil.copy2('var/SIDEMENU-SIDEMENUITEMS.var', 'data/system/' + user + '/SIDEMENU-SIDEMENUITEMS.var')
        shutil.copy2('var/SETTINGS-FULLSCREEN.var', 'data/system/' + user + '/SETTINGS-FULLSCREEN.var')
        shutil.copy2('var/SETTINGS-DESKTOPBG.var', 'data/system/' + user + '/SETTINGS-DESKTOPBG.var')
        shutil.copy2('var/SETTINGS-FPS.var', 'data/system/' + user + '/SETTINGS-FPS.var')
        shutil.copy2('var/SETTINGS-RUNG.var', 'data/system/' + user + '/SETTINGS-RUNG.var')
        
    def signon(self):
        print ''
        print 'Python OS Login'
        uname = raw_input ('User Name : ')
        pwd = raw_input ('Password  : ')
        users = pvar.read('USERS', 'GLOBAL')
        found = False
        for user2 in users:
            if user2 == uname:
                found = True
        if found == True:
            rpass = constants.shiftchar(constants.readfile('user/' + uname + '/' + uname + '.pwd', 'rU')[1], -1)  
            if rpass == pwd:
                constants.msg('Now signed in as ' + uname, 'SHELL')
                pvar.append('CURRENTUSER', uname, 'GLOBAL')
                global user
                user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]
                print ''
                shutil.copy2('data/system/' + uname + '/SETTINGS-RUNG.var', 'var/SETTINGS-RUNG.var')
                if pvar.read('RUNG', 'SETTINGS')[0] == 'True':
                    commands.parsecmd(['startg'])
                return uname
            if rpass != pwd:
                constants.msg('Incorrect Password', 'SHELL')
                interact().signon()
                return uname
        if found == False:
            constants.msg('No such user: ' + uname, 'SHELL')
            return interact().signon()
                
        
