import pvar
import PythonOS
import constants
import commands
import sys
import traceback
import shutil

grunning = False
cdir = ''
user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]

class interact():
    def sh(self, cmd = ''):
        l = 0
        sending = False
        rg = pvar.read('RUNG', 'SETTINGS')
        while cmd != 'exit':
            if rg[0] == 'True' and l == 0 and constants.checkapp('hola-greeter') == False:
                cmd = 'startg'
                sending = True
                l = 1
            elif rg[0] == 'True' and l == 0 and constants.checkapp('hola-greeter') == True:
                sending = True
                cmd = 'gboot'
                l = 1
            if sending == False:
                cmd = raw_input (user + '@pythonos ' + cdir + '$ ')
            scmd = cmd.split()
            try:
                ret = commands.parsecmd(scmd)
                if len(cmd.split()) > 0:
                    constants.logentry('SHELL', 'The command "' + scmd[0] + '" ran without errors.')
                if ret == 'reload':
                    reload(commands)
                if sending == True:
                    sending = False
            except:
                if sending == True:
                    sending = False
                if cmd != 'startg' and cmd != 'gboot':
                    constants.msg('The command ' + scmd[0] + ' has crashed. Error Traceback:', 'SHELL')
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    traceback.print_exception(exc_type, exc_value, exc_traceback)
                    nf = constants.makefile('temp/traceback.txt')
                    tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
                    for ln in tb:
                        print >> nf, ln
                    nf.close()
                    constants.msg('This traceback is saved to temp/traceback.txt.', 'SHELL')
                    constants.logentry('SHELL', 'The command "' + cmd.split()[0] + '" encountered an error. The error is logged at temp/traceback.txt')
                if cmd == 'startg' or cmd == 'gboot':
                    constants.msg('Graphics session ended.', 'SHELL')
                    constants.logentry('SHELL', 'GServer ended.')
                    global grunning
                    grunning = False
                    l = 1
        copyto()
    def signon(self):
        global user
        print ''
        print 'Python OS Login'
        try:
            bf = open('boot.kstr', 'rU')
            bf.close()
            bfound = True
        except IOError:
            bfound = False
        if bfound == False:
            rg = pvar.read('RUNG', 'SETTINGS')
            if rg[0] == 'True':
                if constants.checkapp('hola-greeter') == True:
                    uname, pwd = PythonOS.hola_greeter()
            elif constants.checkapp('hola-greeter') == False or rg[0] != 'True':
                uname = raw_input ('User Name : ')
                pwd = raw_input ('Password  : ')
            users = pvar.read('USERS', 'GLOBAL')
            found = False
            for user2 in users:
                if user2 == uname:
                    found = True
            if found == True:
                rpass = constants.shiftchar(constants.readfile('user/' + uname + '/' + uname + '.pwd', 'rU')[1], -1)  
                if rpass == pwd:
                    constants.msg('Now signed in as ' + uname, 'SHELL')
                    pvar.append('CURRENTUSER', uname, 'GLOBAL')
                    user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]
                    print ''
                    constants.logentry('SHELL', 'User ' + uname + ' signed in.')
                    copyfrom()
                    return uname
                if rpass != pwd:
                    constants.msg('Incorrect Password', 'SHELL')
                    interact().signon()
                    return uname
            if found == False:
                constants.msg('No such user: ' + uname, 'SHELL')
                return interact().signon()
        if bfound == True:
            uname = PythonOS.quickboot().up()
            constants.msg('Now signed in as ' + uname, 'SHELL')
            constants.logentry('SHELL', 'User ' + uname + ' signed in. This was initiated by QuickBoot.')
            pvar.append('CURRENTUSER', uname, 'GLOBAL')
            user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]
            print ''
            return uname
def copyfrom():
    shutil.rmtree('var/')
    shutil.copytree('data/system/' + user + '/', 'var/')
    shutil.copy('PythonOS.py', 'data/system/PythonOS.py')
    shutil.copy('commands.py', 'data/system/commands.py')
    shutil.copy('kernel.py', 'data/system/kernel.py')
    shutil.copy('constants.py', 'data/system/constants.py')
    shutil.copy('shell.py', 'data/system/shell.py')
    shutil.rmtree('data/system/varbackup/', True)
    shutil.copytree('var/', 'data/system/varbackup/')
def copyto():
    shutil.rmtree('data/system/' + user + '/')
