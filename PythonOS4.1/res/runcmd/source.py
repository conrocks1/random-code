def runcmd(cmd = ''):
    import shell
    if cmd == '':
        if shell.grunning == True:
            cmd = components().inputbox('Enter the Command to Run.', ['Enter the command you wish to run.', 'Make your Python Shell visible for text output.'])
            if cmd == 'Cancel':
                return
        if shell.grunning == False:
            cmd = raw_input ('Enter the command you wish to run: ')
    commands.parsecmd(cmd.split())
