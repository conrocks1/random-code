def reload_data(silent = False):
    global sidemenufinalitems
    global sidemenuitems
    global installedapps
    global installedappsfinal
    global toolbarplugstarters
    global toolbarplugs
    global width
    global height
    global appsurface
    global commands
    if silent == False:
        screen.fill([200, 200, 200])
        screen.blit(font.render('Indexing', 1, (50, 50, 50)), [2, 2])
        pygame.display.update()
    c.index()
    if silent == False:
        screen.blit(font.render('Reloading Apps', 1, (50, 50, 50)), [2, 22])
        pygame.display.update()
    installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    installedappsfinal = []
    for inst in installedapps:
        rf = c.readfile('res/' + inst + '/req.txt', 'rU')
        icon = rf[3]
        name = rf[4]
        execp = rf[2]
        if rf[3].startswith('noicon') == False:
            installedappsfinal.append([name, icon, execp])
    if silent == False:
        screen.blit(font.render('Reloading Menu Shortcuts', 1, (50, 50, 50)), [2, 42])
        pygame.display.update()
    sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
    sidemenufinalitems = []
    for i in sidemenuitems:
        rf = c.readfile('res/' + i + '/req.txt', 'rU')
        icon = rf[3]
        name = rf[4]
        execp = rf[2]
        sidemenufinalitems.append([name, icon, execp])
    if silent == False:
        screen.blit(font.render('Reloading Toolbar Plugins', 1, (50, 50, 50)), [2, 62])
        pygame.display.update()
    toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
    toolbarplugs = []
    for plg in toolbarplugstarters:
        rf = c.readfile('res/' + plg + '/toolbar.txt')
        icon = rf[0]
        run = rf[1]
        toolbarplugs.append([icon, run])
    if silent == False:
        screen.blit(font.render('Refreshing Screen', 1, (50, 50, 50)), [2, 82])
        pygame.display.update()
    width = pygame.display.Info().current_w
    height = pygame.display.Info().current_h
    appsurface = pygame.Surface((width - 2, height - 20))
    c.deletefile('commands.pyc')
    reload(commands)
    commands.parsecmd(['reload-pyos'])
    if silent == False:
        components().okbox('Finished.', ['Your Python OS system has been refreshed.', 'This cannot replace a restart.', 'To clean up junk files, use the cleanup app.'])
    return
