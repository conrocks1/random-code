print 'Restoring Apps List'
import os
try:
    os.remove('res/__init__.py')
    os.remove('res/__init__.pyc')
except:
    pass
import constants
import pvar
pvar.make('INSTALLEDAPPS', 'system', 'GLOBAL')
cont = os.listdir('res/')
for app in cont:
    if app != 'system':
        pvar.append('INSTALLEDAPPS', app, 'GLOBAL')
constants.makefile('res/__init__.py')
print 'Done'
