import constants as c
import pvar
import commands
try:
    import pygame
except:
    c.msg('No Pygame.', 'ERROR')
import datetime
import os
import random
import time
import shell
appsurface = None
screen = None
toolbarplugs = []

def gserver():
    c.msg('Gserver v1.1', 'GSERVER')
    import shell
    global width
    global height
    global font
    global largefont
    global smallfont
    global bfont
    global largebfont
    global smallbfont
    global ifont
    global largeifont
    global smallifont
    global clock
    global screen
    global appsurface
    global full
    global ctheme
    ctheme = None
    global processes
    processes = ['Python OS System']
    global daemons
    daemons = []
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    pygame.mixer.init()
    pygame.display.set_caption('Python OS')
    pygame.display.set_icon(pygame.image.load('res/system/graphics/icons/windowicon.png'))
    try:
        width = pygame.display.Info().current_w
        height = pygame.display.Info().current_h
    except:
        width = 1366
        height = 768
    c.logentry('GSERVER', 'Width: ' + str(width) + ' Height: ' + str(height))
    c.logentry('GSERVER', 'Display Driver: ' + str(pygame.display.get_driver()))
    pfull = pvar.read('FULLSCREEN', 'SETTINGS')[0]
    if pfull != 'yes':
        screen = pygame.display.set_mode([width, height], pygame.RESIZABLE)
        full = False
    if pfull ==  'yes':
        screen = pygame.display.set_mode([width, height], pygame.RESIZABLE | pygame.HWSURFACE | pygame.FULLSCREEN)
        full = True
    appsurface = pygame.Surface([width, height - 20])
    font = pygame.font.Font('res/system/Ubuntu-R.ttf', 14)
    largefont = pygame.font.Font('res/system/Ubuntu-R.ttf', 40)
    smallfont = pygame.font.Font('res/system/Ubuntu-R.ttf', 8)
    bfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 14)
    largebfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 40)
    smallbfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 8)
    ifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 14)
    largeifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 40)
    smallifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 8)
    clock = pygame.time.Clock()
    shell.grunning = True

def centerobj(ow, oh, surface):
    s1 = ow / 2
    s2 = oh / 2
    rbw = (surface.get_width() / 2) - s1
    rbh = (surface.get_height() / 2) - s2
    return [rbw, rbh]

class components():
    def button(self, x, y, text):
        ftxt = font.render(text, 1, [25, 25, 25])
        button = pygame.transform.smoothscale(buttonimg, (ftxt.get_width() + 8, ftxt.get_height() + 4))
        appsurface.blit(button, [x, y])
        center = centerobj(button.get_width(), button.get_height(), button)
        appsurface.blit(ftxt, [x + center[0], y + center[1]])
        tret = [x + 2, x + 2 + button.get_width(), y + 20, y + 20 + button.get_height()]
        return tret
    def toolbar(self):
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        bw = width - 35
        for plug in toolbarplugs:
            ico = pygame.image.load(plug[0])
            bw = (bw - 2) - ico.get_width()
            screen.blit(ico, [bw, 0])
        mp = pygame.mouse.get_pos()
        if mp[1] <= 20 and mp[0] >= bw - 10:
            toolbargui()
        daemonman().run()
        components().mouse()
    def mouse(self):
        if ctheme == 'darktheme':
            color = [100, 100, 100]
        if ctheme == 'lighttheme':
            color = [50, 50, 50]
        if ctheme == 'blacktheme':
            color = [200, 200, 200]
        if ctheme == 'whitetheme':
            color = [50, 50, 50]
        pygame.draw.circle(screen, color, pygame.mouse.get_pos(), 3)
    def hangscreen(self, title, content):
        screen.fill([200, 200, 200])
        screen.blit(largefont.render('Please Wait... Python OS is working.', 1, (0, 100, 20)), [20, 20])
        screen.blit(bfont.render(title, 1, (50, 50, 50)), [20, 60])
        bh = 100
        for i in content:
            i2 = font.render(i, 1, (50, 50, 50))
            screen.blit(i2, [20, bh])
            bh = bh + 16
        screen.blit(ifont.render('Python OS may appear to freeze during this process.', 1, (50, 50, 50)), [20, 2])
        pygame.display.update()
    def okbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/okbox-bg.jpg')
        obg = pygame.image.load('temp/okbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        dp = centerobj(400, 300, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'OK')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        lps().end()
                        return 'OK'
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return 'OK'
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def inputbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/inputbox-bg.jpg')
        obg = pygame.image.load('temp/inputbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        dp = centerobj(400, 300, screen)
        reply = ''
        txt = 'Enter text here.'
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            if reply != '':
                ib = components().inputbutton(dp[0] + centerobj(300, 250, dialogimg)[0], dp[1] + 200, reply)
            if reply == '':
                ib = components().inputbutton(dp[0] + centerobj(300, 250, dialogimg)[0], dp[1] + 200, txt)
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'OK')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        lps().end()
                        return reply
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                    if event.key == pygame.K_DELETE:
                        reply = ''
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return reply
                        if mousepos[0] >= ib[0] and mousepos[0] <= ib[1]:
                            if mousepos[1] >= ib[2] and mousepos[1] <= ib[3]:
                                reply = components().getinput(ib[0] - 2, ib[2] - 20, reply)
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def ynbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/ynbox-bg.jpg')
        obg = pygame.image.load('temp/ynbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        selected = 1
        dp = centerobj(400, 300, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'Yes')
            bp1 = components().button(dp[0] + 60, dp[1] + 255, 'No')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        if selected == 0:
                            lps().end()
                            return 'No'
                        if selected == 1:
                            lps().end()
                            return 'Yes'
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                    if event.key == pygame.K_RIGHT:
                        if selected == 1:
                            selected = 0
                    if event.key == pygame.K_LEFT:
                        if selected == 0:
                            selected = 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return 'Yes'
                        if mousepos[0] >= bp1[0] and mousepos[0] <= bp1[1]:
                            if mousepos[1] >= bp1[2] and mousepos[1] <= bp1[3]:
                                lps().end()
                                return 'No'
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def menu(self):
        if pygame.mouse.get_pos()[0] <= 2 and pygame.mouse.get_pos()[1] > 21:
            mb = pygame.image.save(appsurface, 'temp/sidemenu-bg.jpg')
            mb = pygame.image.load('temp/sidemenu-bg.jpg')
            clickables = []
            cp = 21
            for mitem in sidemenufinalitems:
                clickables.append([[1, 49, cp, cp + 49], mitem[2]])
                cp = cp + 49
            while True:
                clock.tick(fps)
                screen.blit(pygame.transform.smoothscale(sidemenuimg, [50, height - 20]), [0, 20])
                screen.blit(mb, [50, 20])
                cp = 21
                for mitem in sidemenufinalitems:
                    screen.blit(pygame.image.load(mitem[1]), [1, cp])
                    cp = cp + 49
                components().toolbar()
                mousepos = pygame.mouse.get_pos()
                if mousepos[0] >= 80:
                    pygame.draw.rect(screen, [50, 50, 50], [0, 20, 2, height - 20])
                    pygame.display.update()
                    return
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        cc = 0
                        while cc <= len(clickables) - 1:
                            obj = clickables[cc]
                            if mousepos[0] >= obj[0][0] and mousepos[0] <= obj[0][1]:
                                if mousepos[1] >= obj[0][2] and mousepos[1] <= obj[0][3]:
                                    if event.button == 1:
                                        screen.fill([50, 50, 50])
                                        pygame.display.update()
                                        commands.parsecmd([obj[1]])
                                        screen.fill([50, 50, 50])
                                        pygame.display.update()
                                        return
                                    if event.button == 3:
                                        gui().rcm_sidemenu(obj[1])
                            cc = cc + 1
                        if mousepos[0] >= 50 and mousepos[0] <= width:
                            if mousepos[1] >= 20 and mousepos[1] <= height:
                                if event.button == 1:
                                    screen.fill([50, 50, 50])
                                    pygame.display.update()
                                    return
    def inputbutton(self, x, y, text):
        ftxt = font.render(text, 1, [25, 25, 25])
        button = pygame.transform.smoothscale(inputboximg, (ftxt.get_width() + 26, ftxt.get_height() + 2))
        appsurface.blit(button, [x, y])
        center = centerobj(button.get_width(), button.get_height(), button)
        appsurface.blit(ftxt, [x + center[0], y + center[1]])
        tret = [x + 2, x + 2 + button.get_width(), y + 20, y + 20 + button.get_height()]
        return tret
    def imagebutton(self, x, y, image, targetsurf=appsurface):
        global appsurface
        if targetsurf == None:
            appsurface.blit(image, [x, y])
            tret = [x + 2, x + 2 + image.get_width(), y + 20, y + 20 + image.get_height()]
        elif targetsurf != None:
            targetsurf.blit(image, [x, y])
            tret = [x, x + image.get_width(), y, y + image.get_height()]
        return tret
    def getinput(self, wpix, hpix, suin, odr = False):
        global copied
        uinput = []
        uin = suin
        uinput = []
        for i in suin:
            uinput.append(i)
        shift = False
        done = False
        pygame.image.save(appsurface, 'temp/getinput.jpg')
        gbg = pygame.image.load('temp/getinput.jpg')
        color = [50, 50, 50]
        ibi = inputboximg
        while done != True:
            clock.tick(10)
            appsurface.blit(gbg, [0, 0])
            text = font.render(str(uin), 1, color)
            if text.get_width() >= ibi.get_width():
                ibi = pygame.transform.smoothscale(ibi, [text.get_width() + 2, ibi.get_height()])
            appsurface.blit(ibi, [wpix, hpix])
            appsurface.blit(text, [wpix + 2, hpix + 2])
            screen.blit(appsurface, [2, 20])
            components().menu()
            components().toolbar()
            pygame.display.update()
            mousepos = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.VIDEORESIZE:
                    width = event.w
                    height = event.h
                    gbg = pygame.transform.smoothscale(gbg, (width, height))
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] < wpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                    elif mousepos[0] > text.get_width() + 2:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                    elif mousepos[0] < hpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                    elif mousepos[0] > hpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        done = True
                    if event.key == pygame.K_RETURN:
                        done = True
                    if event.key == pygame.K_a:
                        if shift == False:
                            uinput.append('a')
                        if shift == True:
                            uinput.append('A')
                            shift = False
                    if event.key == pygame.K_b:
                        if shift == False:
                            uinput.append('b')
                        if shift == True:
                            uinput.append('B')
                            shift = False
                    if event.key == pygame.K_c:
                        if shift == False:
                            uinput.append('c')
                        if shift == True:
                            uinput.append('C')
                            shift = False
                    if event.key == pygame.K_d:
                        if shift == False:
                            uinput.append('d')
                        if shift == True:
                            uinput.append('D')
                            shift = False
                    if event.key == pygame.K_e:
                        if shift == False:
                            uinput.append('e')
                        if shift == True:
                            uinput.append('E')
                            shift = False
                    if event.key == pygame.K_f:
                        if shift == False:
                            uinput.append('f')
                        if shift == True:
                            uinput.append('F')
                            shift = False
                    if event.key == pygame.K_g:
                        if shift == False:
                            uinput.append('g')
                        if shift == True:
                            uinput.append('G')
                            shift = False
                    if event.key == pygame.K_h:
                        if shift == False:
                            uinput.append('h')
                        if shift == True:
                            uinput.append('H')
                            shift = False
                    if event.key == pygame.K_i:
                        if shift == False:
                            uinput.append('i')
                        if shift == True:
                            uinput.append('I')
                            shift = False
                    if event.key == pygame.K_j:
                        if shift == False:
                            uinput.append('j')
                        if shift == True:
                            uinput.append('J')
                            shift = False
                    if event.key == pygame.K_k:
                        if shift == False:
                            uinput.append('k')
                        if shift == True:
                            uinput.append('K')
                            shift = False
                    if event.key == pygame.K_l:
                        if shift == False:
                            uinput.append('l')
                        if shift == True:
                            uinput.append('L')
                            shift = False
                    if event.key == pygame.K_m:
                        if shift == False:
                            uinput.append('m')
                        if shift == True:
                            uinput.append('M')
                            shift = False
                    if event.key == pygame.K_n:
                        if shift == False:
                            uinput.append('n')
                        if shift == True:
                            uinput.append('N')
                            shift = False
                    if event.key == pygame.K_o:
                        if shift == False:
                            uinput.append('o')
                        if shift == True:
                            uinput.append('O')
                            shift = False
                    if event.key == pygame.K_p:
                        if shift == False:
                            uinput.append('p')
                        if shift == True:
                            uinput.append('P')
                            shift = False
                    if event.key == pygame.K_q:
                        if shift == False:
                            uinput.append('q')
                        if shift == True:
                            uinput.append('Q')
                            shift = False
                    if event.key == pygame.K_r:
                        if shift == False:
                            uinput.append('r')
                        if shift == True:
                            uinput.append('R')
                            shift = False
                    if event.key == pygame.K_s:
                        if shift == False:
                            uinput.append('s')
                        if shift == True:
                            uinput.append('S')
                            shift = False
                    if event.key == pygame.K_t:
                        if shift == False:
                            uinput.append('t')
                        if shift == True:
                            uinput.append('T')
                            shift = False
                    if event.key == pygame.K_u:
                        if shift == False:
                            uinput.append('u')
                        if shift == True:
                            uinput.append('U')
                            shift = False
                    if event.key == pygame.K_v:
                        if shift == False:
                            uinput.append('v')
                        if shift == True:
                            uinput.append('V')
                            shift = False
                    if event.key == pygame.K_w:
                        if shift == False:
                            uinput.append('w')
                        if shift == True:
                            uinput.append('W')
                            shift = False
                    if event.key == pygame.K_x:
                        if shift == False:
                            uinput.append('x')
                        if shift == True:
                            uinput.append('X')
                            shift = False
                    if event.key == pygame.K_y:
                        if shift == False:
                            uinput.append('y')
                        if shift == True:
                            uinput.append('Y')
                            shift = False
                    if event.key == pygame.K_z:
                        if shift == False:
                            uinput.append('z')
                        if shift == True:
                            uinput.append('Z')
                            shift = False
                    if event.key == pygame.K_LSHIFT or event.key == pygame.K_RSHIFT:
                        shift = True
                    if event.key == pygame.K_INSERT:
                        if shift == False:
                            if copied == '':
                                copied = uin
                            elif copied != '':
                                uinput.append(copied)
                        if shift == True:
                            copied = ''
                    if event.key == pygame.K_SPACE:
                        uinput.append(' ')
                    if event.key == pygame.K_DELETE:
                        uinput = []
                        uin = ''
                        c.varman().make('input-specialchar', 'delete')
                        if odr == True:
                            done = True
                    if event.key == pygame.K_TAB:
                        uinput.append(' ')
                    if event.key == pygame.K_1:
                        if shift == True:
                            uinput.append('!')
                            shift = False
                        elif shift == False:
                            uinput.append('1')
                    if event.key == pygame.K_2:
                        if shift == True:
                            uinput.append('@')
                            shift = False
                        elif shift == False:
                            uinput.append('2')
                    if event.key == pygame.K_3:
                        if shift == True:
                            uinput.append('#')
                            shift = False
                        elif shift == False:
                            uinput.append('3')
                    if event.key == pygame.K_4:
                        if shift == True:
                            uinput.append('$')
                            shift = False
                        elif shift == False:
                            uinput.append('4')
                    if event.key == pygame.K_5:
                        if shift == True:
                            uinput.append('%')
                            shift = False
                        elif shift == False:
                            uinput.append('5')
                    if event.key == pygame.K_6:
                        if shift == True:
                            uinput.append('^')
                            shift = False
                        elif shift == False:
                            uinput.append('6')
                    if event.key == pygame.K_7:
                        if shift == True:
                            uinput.append('&')
                            shift = False
                        elif shift == False:
                            uinput.append('7')
                    if event.key == pygame.K_8:
                        if shift == True:
                            uinput.append('*')
                            shift = False
                        elif shift == False:
                            uinput.append('8')
                    if event.key == pygame.K_9:
                        if shift == True:
                            uinput.append('(')
                            shift = False
                        elif shift == False:
                            uinput.append('9')
                    if event.key == pygame.K_0:
                        if shift == True:
                            uinput.append(')')
                            shift = False
                        elif shift == False:
                            uinput.append('0')
                    if event.key == pygame.K_PERIOD:
                        if shift == False:
                            uinput.append('.')
                        elif shift == True:
                            uinput.append('>')
                            shift = False
                    if event.key == pygame.K_COMMA:
                        if shift == False:
                            uinput.append(',')
                        elif shift == True:
                            uinput.append('<')
                            shift = False
                    if event.key == pygame.K_SLASH:
                        if shift == False:
                            uinput.append('/')
                        elif shift == True:
                            uinput.append('?')
                            shift = False
                    if event.key == pygame.K_MINUS:
                        if shift == False:
                            uinput.append('-')
                        elif shift == True:
                            uinput.append('_')
                            shift = False
                    if event.key == pygame.K_SEMICOLON:
                        if shift == False:
                            uinput.append(';')
                        elif shift == True:
                            uinput.append(':')
                            shift = False
                    if event.key == pygame.K_BACKSPACE:
                        if len(uinput) >= 1:
                            uinput.pop()
                    if event.key == ord("'"):
                        if shift == False:
                            uinput.append("'")
                        elif shift == True:
                            uinput.append('"')
                            shift = False
            uin = ''
            for i in uinput:
                uin = uin + i
        uin = ''
        for i in uinput:
            uin = uin + i
        c.varman().make('input-exitmode', 'enter')
        return uin
class lps():
    def new(self, name):
        processes.append(name)
        c.logentry('lps', 'Application ' + name + ' started')
        return name
    def end(self):
        c.logentry('lps', 'Application ' + processes[len(processes) - 1] + ' ended')
        return processes.pop()
    def listall(self):
        return processes
        
class gui():
    def boot(self, nod = False):
        try:
            c.msg('Booting Python OS GUI', 'POSGUI')
            c.logentry('GUI', 'Starting Python OS GUI load.')
            global ctheme
            ctheme = 'darktheme'
            global processes
            processes = ['Python OS System']
            global mousepos
            global vol
            global full
            global copied
            copied = ''
            full = False
            vol = 0.5
            mousepos = centerobj(4, 4, screen)
            pygame.mouse.set_pos(mousepos)
            pygame.mouse.set_visible(False)
            pygame.key.set_repeat(50, 50)
            global sidemenufinalitems
            global sidemenuitems
            global installedapps
            global installedappsfinal
            installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
            sidemenufinalitems = []
            for i in sidemenuitems:
                rf = c.readfile('res/' + i + '/req.txt', 'rU')
                icon = rf[3]
                name = rf[4]
                execp = rf[2]
                try:
                    pygame.image.load(icon)
                except:
                    icon = 'res/system/graphics/icons/noicon.png'
                sidemenufinalitems.append([name, icon, execp])
            installedappsfinal = []
            for inst in installedapps:
                rf = c.readfile('res/' + inst + '/req.txt', 'rU')
                icon = rf[3]
                name = rf[4]
                execp = rf[2]
                if rf[3].startswith('noicon') == False:
                    try:
                        pygame.image.load(icon)
                    except:
                        icon = 'res/system/graphics/icons/noicon.png'
                    installedappsfinal.append([name, icon, execp])
            global toolbarplugstarters
            toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
            for plg in toolbarplugstarters:
                rf = c.readfile('res/' + plg + '/toolbar.txt')
                icon = rf[0]
                run = rf[1]
                try:
                    pygame.image.load(icon)
                except:
                    icon = 'res/system/graphics/icons/noicon.png'
                toolbarplugs.append([icon, run])
            global pythonpoweredl
            global pythonpowereds
            global bg
            global buttonimg
            global toolbarimg
            global dialogimg
            global sidemenuimg
            global closeicon
            global rightclickmenuimg
            global inputboximg
            inputboximg = pygame.image.load('res/system/graphics/input.png')
            rightclickmenuimg = pygame.image.load('res/system/graphics/right-click-menu.png')
            closeicon = pygame.image.load('res/system/graphics/icons/close.png')
            sidemenuimg = pygame.image.load('res/system/graphics/menu.png')
            pythonpoweredl = pygame.image.load('res/system/python-powered-l.png')
            pythonpowereds = pygame.image.load('res/system/python-powered-s.png')
            bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
            buttonimg = pygame.image.load('res/system/graphics/button.png')
            toolbarimg = pygame.image.load('res/system/graphics/toolbar.png')
            dialogimg = pygame.image.load('res/system/graphics/dialog.png')
            screen.fill([50, 50, 50])
            screen.blit(pygame.transform.smoothscale(bg, [width, height]), [0, 0])
            screen.blit(pythonpoweredl, centerobj(200, 80, screen))
            pygame.display.update()
            global red
            global green
            global blue
            global black
            global white
            global gray
            global lightgray
            global darkwhite
            red = [255, 0, 0]
            green = [0, 255, 0]
            blue = [0, 0, 255]
            black = [0, 0, 0]
            white = [255, 255, 255]
            gray = [50, 50, 50]
            lightgray = [100, 100, 100]
            darkwhite = [200, 200, 200]
            global darktheme
            global lighttheme
            global blacktheme
            global whitetheme
            darktheme = [gray, lightgray, darkwhite]
            lighttheme = [darkwhite, lightgray, gray]
            blacktheme = [black, gray, lightgray]
            whitetheme = [white, black, darkwhite]
            global fps
            fps = int(pvar.read('FPS', 'SETTINGS')[0])
            c.logentry('GUI', 'Finished.')
        except:
            print 'Python OS Graphics Loader has experienced a fatal error!'
            print 'Saving error code to temp/graphics-boot-traceback.txt'
            import sys
            import traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            nf = c.makefile('temp/graphics-boot-traceback.txt')
            tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
            for ln in tb:
                print >> nf, ln
            nf.close()
            c.logentry('GUI', 'Error while booting. Traceback at temp/graphics-boot-traceback.txt')
            print 'Traceback saved.'
            print 'Killing graphics...'
            commands.parsecmd(['killg'])
            print 'Exiting.'
            return
        if nod == False:
            try:
                daemonman().atstart()
            except:
                print 'Cannot start daemons!'
                print 'Saving error code to temp/daemon-boot-traceback.txt'
                import sys
                import traceback
                exc_type, exc_value, exc_traceback = sys.exc_info()
                nf = c.makefile('temp/daemon-boot-traceback.txt')
                tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
                for ln in tb:
                    print >> nf, ln
                nf.close()
                c.logentry('GUI', 'Cannot start all daemons. Traceback at temp/daemon-boot-traceback.txt')
                print 'Traceback saved.'
                print 'Killing graphics...'
                commands.parsecmd(['killg'])
                print 'Exiting.'
                return
            gui().desktop()
        return
    def desktop(self):
        global ctheme
        global bg
        global width
        global height
        lps().new('Home')
        ctheme = 'darktheme'
        bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
        apps = pvar.read('DESKTOPAPPS', 'SETTINGS')
        bg = pygame.transform.smoothscale(bg, [width - 2, height - 20])
        appicon = pygame.image.load('res/system/graphics/icons/grayapps.png')
        icntr = centerobj(49, 49, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(bg, [0, 0])
            abtn = components().imagebutton(icntr[0], height - 149, appicon, appsurface)
            screen.blit(appsurface, [2, 20])
            try:
                components().toolbar()
                components().menu()
            except:
                error()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    commands.parsecmd(['killg'])
                    lps().end()
                    return
                if event.type == pygame.VIDEORESIZE:
                    width = event.w
                    height = event.h
                    icntr = centerobj(49, 49, screen)
                    bg = pygame.transform.smoothscale(bg, [width - 2, height - 20])
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        if lps().listall()[len(lps().listall()) - 2] != 'Python OS System':
                            lps().end()
                            return
                        if lps().listall()[len(lps().listall()) - 2] == 'Python OS System':
                            pyexit()
                    if event.key == pygame.K_MENU:
                        mousepos = [1, 200]
                        pygame.mouse.set_pos(mousepos)
                    if event.key == pygame.K_RSUPER or event.key == pygame.K_LSUPER:
                        gui().apps()
                    if event.key == pygame.K_F1:
                        lps().end()
                        lps().end()
                        screen.fill([0, 0, 0])
                        pygame.display.update()
                        return
                    if event.key == pygame.K_F2:
                        gotfps = clock.get_fps()
                        c.logentry('INFO', 'FPS of Home is: ' + str(gotfps))
                        components().okbox('FPS Evaluation', ['Your framerate is: ' + str(gotfps) + '.', 'Python OS is set to reach: ' + str(fps), 'Change this in Settings >> Change FPS of Applications.'])
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                if lps().listall()[len(lps().listall()) - 2] != 'Python OS System':
                                    lps().end()
                                    return
                                if lps().listall()[len(lps().listall()) - 2] == 'Python OS System':
                                    pyexit()
                        if mousepos[0] >= abtn[0] and mousepos[0] <= abtn[1]:
                            if mousepos[1] >= abtn[2] and mousepos[1] <= abtn[3]:
                                gui().apps()
                    
    def apps(self):
        global ctheme
        global width
        global height
        lps().new('Applications')
        ctheme = 'darktheme'
        try:
            if pvar.read('TRANSPARENT', 'APPS')[0] == 'False':
                bg = pygame.transform.smoothscale(pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0]), [width, height - 20])
            if pvar.read('TRANSPARENT', 'APPS')[0] == 'True':
                pygame.image.save(appsurface, 'data/system/apps-bg.jpg')
                bg = pygame.transform.smoothscale(pygame.image.load('data/system/apps-bg.jpg'), [width, height - 20])
        except:
            components().okbox('Invalid Settings', ['Wallpaper/Transparency Settings invalid.'])
            bg = pygame.Surface([width - 2, height - 20])
            bg.fill([200, 200, 200])
        clickables = [[]]
        page = 0
        w = 40
        h = 10
        for a in installedappsfinal:
            clickables[page].append([[w, w + 50, h, h + 60], a[2], a[1], a[0]])
            if w + 100 >= width - 60 and h + 70 >= height - 80:
                clickables.append([])
                page = page + 1
                w = 40
                h = 10
            elif w + 100 < width - 60:
                w = w + 100
            elif w + 100 >= width - 60:
                w = 40
                h = h + 70
        page = 0
        while True:
            clock.tick(fps)
            appsurface.blit(bg, [0, 0])
            mousepos = pygame.mouse.get_pos()
            for app in clickables[page]:
                aicon = pygame.image.load(app[2])
                appsurface.blit(aicon, [app[0][0], app[0][2]])
                aname = font.render(app[3], 1, (25, 25, 25))
                tcenter = centerobj(aname.get_width(), aname.get_height(), aicon)
                appsurface.blit(aname, [app[0][0] + tcenter[0], app[0][2] + 49])
            npb = components().button(width - 50, height - 40, ' > ')
            ppb = components().button(30, height - 40, ' < ')
            indicator = font.render(str(page + 1) + ' of ' + str(len(clickables)), 1, (50, 50, 50))
            appsurface.blit(indicator, [centerobj(indicator.get_width(), indicator.get_height(), appsurface)[0], height - 40])
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        lps().end() 
                        return
                    if event.key == pygame.K_RIGHT:
                        if page < len(clickables) - 1:
                            page = page + 1
                    if event.key == pygame.K_LEFT:
                        if page > 0:
                            page = page - 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for abutton in clickables[page]:
                        if mousepos[0] >= abutton[0][0] and mousepos[0] <= abutton[0][1]:
                            if mousepos[1] >= abutton[0][2] and mousepos[1] <= abutton[0][3]:
                                if event.button == 1:
                                    try:
                                        commands.parsecmd([abutton[1]])
                                    except:
                                        error()
                                    lps().end()
                                    return
                                if event.button == 3:
                                    if gui().rcm_app(abutton[1]) == 'uninstall':
                                        lps().end()
                                        return
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            lps().end()
                            return
                    if mousepos[0] >= npb[0] and mousepos[0] <= npb[1]:
                        if mousepos[1] >= npb[2] and mousepos[1] <= npb[3]:
                            if page < len(clickables) - 1:
                                page = page + 1
                    if mousepos[0] >= ppb[0] and mousepos[0] <= ppb[1]:
                        if mousepos[1] >= ppb[2] and mousepos[1] <= ppb[3]:
                            if page > 0:
                                page = page - 1
    def rcm_app(self, execp):
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
        rcmbg = pygame.image.load('temp/rcmbg.jpg')
        mp = pygame.mouse.get_pos()
        while True:
            clock.tick(fps)
            appsurface.blit(rcmbg, [0, 0])
            appsurface.blit(rightclickmenuimg, mp)
            addtol = components().button(mp[0] + 2, mp[1] + 2, 'Add to Launcher')
            rem = components().button(mp[0] + 2, mp[1] + 25, 'Uninstall')
            run = components().button(mp[0] + 2, mp[1] + 48, 'Open')
            tlb = components().button(mp[0] + 2, mp[1] + 71, 'Add to Toolbar')
            hlp = components().button(mp[0] + 2, mp[1] + 94, 'Help')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            mousepos = pygame.mouse.get_pos()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] >= addtol[0] and mousepos[0] <= addtol[1]:
                        if mousepos[1] >= addtol[2] and mousepos[1] <= addtol[3]:
                            pvar.append('SIDEMENUITEMS', execp, 'SIDEMENU')
                            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            sidemenufinalitems = []
                            for i in sidemenuitems:
                                rf = c.readfile('res/' + i + '/req.txt', 'r')
                                icon = rf[3]
                                name = rf[4]
                                execp = rf[2]
                                sidemenufinalitems.append([name, icon, execp])
                            return 'done'
                    if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                        if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                            if components().ynbox('Do you really want to uninstall?', ['You are about to uninstall:', execp, 'Do you wish to proceed with uninstallation?']) == 'Yes':
                                commands.parsecmd(['uninstall', execp, '-n'])
                                name = execp
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                for a in sidemenuitems:
                                    if a == name:
                                        pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                        for ca in sidemenuitems:
                                            if ca != name and ca != 'appmenu':
                                                pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                sidemenufinalitems = []
                                for i in sidemenuitems:
                                    rf = c.readfile('res/' + i + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    sidemenufinalitems.append([name, icon, execp])
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                components().okbox('Application Uninstalled', ['The application has been uninstalled.'])
                                reload_data(True)
                                return 'uninstall'
                            return
                    if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                        if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                            commands.parsecmd([execp])
                            return 'done'
                    if mousepos[0] >= hlp[0] and mousepos[0] <= hlp[1]:
                        if mousepos[1] >= hlp[2] and mousepos[1] <= hlp[3]:
                            textedit().gui(True, 'res/' + execp + '/help.txt', 'Help')
                            return
                    if mousepos[0] >= tlb[0] and mousepos[0] <= tlb[1]:
                        if mousepos[1] >= tlb[2] and mousepos[1] <= tlb[3]:
                            toolbaradd(execp)
                            return 'done'
                    if mousepos[0] >= mp[0] + 152 and mousepos[0] <= width:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return 'done'
                    if mousepos[0] >= 2 and mousepos[0] <= mp[0] + 2:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return 'done'
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= 0 and mousepos[1] <= mp[1] - 20:
                            return 'done'
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                            return 'done'
    def rcm_sidemenu(self, execp):
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
        rcmbg = pygame.image.load('temp/rcmbg.jpg')
        mp = pygame.mouse.get_pos()
        while True:
            clock.tick(fps)
            appsurface.blit(rcmbg, [0, 0])
            appsurface.blit(rightclickmenuimg, mp)
            rln = components().button(mp[0] + 2, mp[1] + 2, 'Remove Link')
            rem = components().button(mp[0] + 2, mp[1] + 25, 'Uninstall')
            run = components().button(mp[0] + 2, mp[1] + 48, 'Open')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            mousepos = pygame.mouse.get_pos()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] >= rln[0] and mousepos[0] <= rln[1]:
                        if mousepos[1] >= rln[2] and mousepos[1] <= rln[3]:
                            insm = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            pvar.destroy('SIDEMENUITEMS', 'SIDEMENU')
                            pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                            for app in insm:
                                if app != execp and app != 'appmenu':
                                    pvar.append('SIDEMENUITEMS', app, 'SIDEMENU')
                            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            sidemenufinalitems = []
                            for i in sidemenuitems:
                                rf = c.readfile('res/' + i + '/req.txt', 'r')
                                icon = rf[3]
                                name = rf[4]
                                execp = rf[2]
                                sidemenufinalitems.append([name, icon, execp])
                            return
                    if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                        if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                            if components().ynbox('Do you really want to uninstall?', ['You are about to uninstall:', execp, 'Do you wish to proceed with uninstallation?']) == 'Yes':
                                commands.parsecmd(['uninstall', execp, '-n'])
                                name = execp
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                for a in sidemenuitems:
                                    if a == name:
                                        pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                        for ca in sidemenuitems:
                                            if ca != name and ca != 'appmenu':
                                                pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                sidemenufinalitems = []
                                for i in sidemenuitems:
                                    rf = c.readfile('res/' + i + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    sidemenufinalitems.append([name, icon, execp])
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                components().okbox('Application Uninstalled', ['The application has been uninstalled.'])
                                reload_data(True)
                            return
                    if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                        if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                            commands.parsecmd([execp])
                            return
                    if mousepos[0] >= mp[0] + 152 and mousepos[0] <= width:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= 2 and mousepos[0] <= mp[0] + 2:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= 0 and mousepos[1] <= mp[1] - 20:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                            return

def error():
    import traceback
    import sys
    exc_type, exc_value, exc_traceback = sys.exc_info()
    tb = traceback.format_exc()
    tbf = c.makefile('temp/traceback.txt')
    print >> tbf, tb
    tbf.close()
    c.logentry('ERROR', 'Python OS has experienced an error that stopped the normal flow of operations.')
    c.logentry('INFO', "A transcript of the error's traceback is at temp/traceback.txt.")
    rtb = c.readfile('temp/traceback.txt', 'r')
    screen.fill([50, 50, 50])
    screen.blit(font.render('The application ' + lps().listall()[len(lps().listall()) - 1] + ' has crashed. Press Space key to continue.', 1, (255, 100, 100)), [20, 20])
    h = 40
    for ln in rtb:
        screen.blit(font.render(ln, 1, (200, 200, 200)), [20, h])
        h = h + 16
    h = h + 40
    oh = h
    for app in lps().listall():
        screen.blit(font.render(app, 1, (200, 200, 200)), [20, h])
        h = h + 16
    pygame.display.update()
    done = False
    while done != True:
        clock.tick(5)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    done = True
    screen.blit(font.render('Python OS has experienced an error. Quit graphics? (Y/N)', 1, (200, 200, 200)), [20, h])
    pygame.display.update()
    done = False
    while done != True:
        clock.tick(5)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_y:
                    qg = True
                    done = True
                if event.key == pygame.K_n:
                    qg = False
                    done = True
    if qg == True:
        commands.parsecmd(['killg'])
    lps().end()
    return
class daemonman():
    def atstart(self):
        dstarts = pvar.read('DAEMONS', 'DMAN')
        for dstart in dstarts:
            daemonman().add(dstart)
    def add(self, name):
        global daemons
        dinf = c.readfile('res/' + name + '/daemon.txt')
        eval(dinf[1])
        c.logentry('DMAN', 'Added daemon ' + name + ' @ ' + dinf[0])
        daemons.append([name, dinf[0]])
    def delete(self, name):
        global daemons
        nd = []
        for daemon in daemons:
            if daemon[0] == name:
                continue
            nd.append(daemon)
        daemons = nd
    def run(self):
        for daemon in daemons:
            eval(daemon[1])
    def retall(self):
        return pvar.read('DAEMONS', 'DMAN')
def sysinfo():
    components().okbox('System Information', ['Python OS 4.1', 'Version: 4.1.0', 'Kernel: 1.1-pyos4', 'GServer: 1.01'])
    return
def pyexit():
    r = components().ynbox('Sure to Exit?', ['Are you sure you want to exit?', 'This will close all:', 'Active Files', 'Active Applications'])
    if r == 'Yes':
        try:
            pvar.read('NSR', 'NESTEDSESSION')
            commands.parsecmd(['exit'])
        except:
            commands.parsecmd(['killg'])
            return
def settings():
    lps().new('Settings')
    global fps
    global bg
    global screen
    global width
    global height
    global full
    global appsurface
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        b1 = components().button(20, 20, 'Change FPS of applications')
        b2 = components().button(20, b1[3] + 2, 'Change the Desktop background')
        b3 = components().button(20, b2[3] + 2, 'Graphics Auto-Start')
        b4 = components().button(20, b3[3] + 2, 'Toggle Fullscreen')
        b5 = components().button(20, b4[3] + 2, 'Change Copy/Move chunk size')
        b6 = components().button(20, b5[3] + 2, 'Toggle Debug Mode')
        b7 = components().button(20, b6[3] + 2, 'Applications Background Transparency')
        screen.blit(appsurface, [2, 20])
	components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.VIDEORESIZE:
                width = event.w
                height = event.h
                appsurface = pygame.Surface((width - 2, height - 20))
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        return
                if mousepos[0] >= b1[0] and mousepos[0] <= b1[1]:
                    if mousepos[1] >= b1[2] and mousepos[1] <= b1[3]:
                        pvar.make('FPS', components().inputbox('Set the FPS of applications', ['Enter the Flips Per Second of applications.', 'Default: 10']), 'SETTINGS')
                        fps = int(pvar.read('FPS', 'SETTINGS')[0])
                if mousepos[0] >= b2[0] and mousepos[0] <= b2[1]:
                    if mousepos[1] >= b2[2] and mousepos[1] <= b2[3]:
                        path = files('return')
                        if c.readfile(path, 'r') != None:
                            pvar.make('DESKTOPBG', path, 'SETTINGS')
                            bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
                if mousepos[0] >= b3[0] and mousepos[0] <= b3[1]:
                    if mousepos[1] >= b3[2] and mousepos[1] <= b3[3]:
                        answer = components().ynbox('Start Graphics at login?', ['Do you want to start the Desktop when you log in?'])
                        if answer == 'Yes':
                            pvar.make('RUNG', 'True', 'SETTINGS')
                        if answer == 'No':
                            pvar.make('RUNG', 'False', 'SETTINGS')
                if mousepos[0] >= b4[0] and mousepos[0] <= b4[1]:
                    if mousepos[1] >= b4[2] and mousepos[1] <= b4[3]:
                        full = c.toggle_full()
                        if full == True:
                            pvar.make('FULLSCREEN', 'yes', 'SETTINGS')
                        if full == False:
                            pvar.make('FULLSCREEN', 'no', 'SETTINGS')
                        width = screen.get_width()
                        height = screen.get_height()
                        appsurface = pygame.Surface((width - 2, height - 20))
                if mousepos[0] >= b5[0] and mousepos[0] <= b5[1]:
                    if mousepos[1] >= b5[2] and mousepos[1] <= b5[3]:
                        pvar.make('BUFFERSIZE', components().inputbox('Set the Copy/Move Buffer Size', ['Enter the buffer size.', 'Default: 1000']), 'COPY')
                if mousepos[0] >= b6[0] and mousepos[0] <= b6[1]:
                    if mousepos[1] >= b6[2] and mousepos[1] <= b6[3]:
                        ans = components().ynbox('Debug Mode', ['Do you want debug mode to be on?', 'For this to work, debugd needs to be installed.'])
                        if ans == 'Yes':
                            pvar.make('DEBUGMODE', 'True', 'KERNEL')
                            c.varman().make('debugmode', True)
                            daemonman().add('debugd')
                        if ans != 'Yes':
                            pvar.make('DEBUGMODE', 'False', 'KERNEL')
                            c.varman().make('debugmode', False)
                if mousepos[0] >= b7[0] and mousepos[0] <= b7[1]:
                    if mousepos[1] >= b7[2] and mousepos[1] <= b7[3]:
                        ans = components().ynbox('Transparent Background', ['Do you want the Apps background to be transparent?'])
                        if ans == 'Yes':
                            pvar.make('TRANSPARENT', 'True', 'APPS')
                        if ans != 'Yes':
                            pvar.make('TRANSPARENT', 'False', 'APPS')
class pman():
    def category(self, cat, opts):
        print 'Working...'
        import urllib
        mirrors = c.readfile('res/pman/mirrors.txt')
        cmir = 0
        for mirror in mirrors:
            try:
                code = urllib.urlopen(mirror + cat + '.txt').code
                if (code / 100 >= 4):
                   raise RuntimeError
                urllib.urlretrieve(mirror + cat + '.txt', 'temp/cat.txt')
            except:
                c.msg(cat + ' is not available on mirror #' + str(cmir + 1), 'PMAN')
                cmir = cmir + 1
                continue
            if opts == 'v':
                if shell.grunning == True:
                    components().okbox('Category downloaded.', ['Category downloaded to temp/cat.txt'])
                if shell.grunning == False:
                    print 'Apps in this category:'
                    for ln in c.readfile('temp/cat.txt'):
                        print ln
            if opts == 'ri':
                for app in c.readfile('temp/cat.txt'):
                    pman().getapp(app)
            if opts == 'u':
                for app in c.readfile('temp/cat.txt'):
                    c.uninstall(app, '-n')
            return
    def printver(self):
        print 'PMan: Application Manager for Python OS 4x.'
        print '---------       Version 1.1'
        print '| .zip  |       For Python OS 4.1+'
        print '| $app  |       By Adam Furman.'
        print '---------       Server http://adamfurman.privatepage.sk/PythonOS/Files/Applications/'
    def install(self, path, silent = False):
        commands.parsecmd(['install', path])
        if silent == False:
            if shell.grunning == True:
                components().okbox('Install finished', ['The application has been installed'])
            if shell.grunning == False:
                c.msg('Application Installed', 'PMAN')
        if shell.grunning == True:
            reload_data(True)
        if shell.grunning == False:
            commands.parsecmd('reload-pyos')
    def uninstall(self, name):
        commands.parsecmd(['uninstall', name])
        if shell.grunning == True:
            components().okbox('Uninstall finished', ['The application has been uninstalled'])
        if shell.grunning == False:
            c.msg('Application Uninstalled', 'PMAN')
        if shell.grunning == True:
            reload_data(True)
        if shell.grunning == False:
            commands.parsecmd('reload-pyos')
    def updateapps(self):
        if shell.grunning == True:
            components().hangscreen('Updating Apps', ['PMan is querying, downloading, and applying updates.'])
        c.msg('Querying Updates', 'PMAN')
        import shutil
        shutil.copy('var/GLOBAL-INSTALLEDAPPS.var', 'data/pman/apps.var')
        import urllib
        il = pvar.read('INSTALLEDAPPS', 'GLOBAL')
        av = []
        for app in il:
            apv = float(c.readfile('res/' + app + '/req.txt')[5])
            av.append([app, apv])
        mirrors = c.readfile('res/pman/mirrors.txt')
        cmir = 1
        for mirror in mirrors:
            print 'Mirror #' + str(cmir) + ': ' + mirror
            cmir = cmir + 1
        cmir = 1
        for tra in il:
            c.msg('Checking updates for ' + tra, 'PMAN')
            cmir = 1
            for mirror in mirrors:
                try:
                    code = urllib.urlopen(mirror + tra + '/req.txt').code
                    if (code / 100 >= 4):
                       raise RuntimeError
                    urllib.urlretrieve(mirror + tra + '/req.txt', 'temp/netreq.txt')
                    laver = float(c.readfile('res/' + tra + '/req.txt')[5])
                    sver = float(c.readfile('temp/netreq.txt')[5])
                    if sver > laver:
                        commands.parsecmd(['uninstall', tra, '-kd'])
                        c.msg('Downloading updates for ' + tra, 'PMAN')
                        pman().getapp(tra, True)
                        c.deletefile('data/pman/app.zip')
                        shutil.copy('data/pman/apps.var', 'var/GLOBAL-INSTALLEDAPPS.var')
                        c.msg('Updated ' + tra, 'PMAN')
                except:
                    c.msg(tra + ' is not available on mirror #' + str(cmir), 'PMAN')
                    cmir = cmir + 1
                    continue
        if shell.grunning == True:
            reload_data(True)
        if shell.grunning == False:
            commands.parsecmd('reload-pyos')
    def getapp(self, name, ows = False):
        if ows == False:
            if shell.grunning == True:
                components().hangscreen('Downloading App', ['PMan is downloading the requested app.'])
        import urllib
        c.msg('Downloading', 'PMAN')
        mirrors = c.readfile('res/pman/mirrors.txt')
        cmir = 1
        for mirror in mirrors:
            print 'Mirror #' + str(cmir) + ': ' + mirror
            cmir = cmir + 1
        cmir = 1
        for mirror in mirrors:
            try:
                print 'Trying mirror #' + str(cmir)
                code = urllib.urlopen(mirror + name + '/'+ name + '.zip').code
                if (code / 100 >= 4):
                   raise RuntimeError
                urllib.urlretrieve(mirror + name + '/' + name + '.zip', 'data/pman/app.zip')
                break
            except:
                cmir = cmir + 1
                pass
        c.msg('Installing', 'PMAN')
        pman().install('data/pman/app.zip', True)
    def pgui(self):
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        lps().new('Application Manager')
        installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
        inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
        pages = [[[]]]
        h = 70
        w = 22
        page = 0
        subpage = 0
        for item in installed:
            if h < height - 20:
                pages[page][subpage].append(item)
            elif h >= height - 20 and w < width - 300:
                h = 70
                w = w + 300
                subpage = subpage + 1
                pages[page].append([])
                pages[page][subpage].append(item)
            elif w >= width - 300:
                pages.append([[]])
                page = page + 1
                subpage = 0
                pages[page][subpage].append(item)
                h = 70
                w = 22
            h = h + 15
            h = 70
            w = 22
            page = 0
            subpage = 0
        while True:
            clock.tick(fps)
            appsurface.fill([50, 50, 50])
            subpage = 0
            w = 2
            h = 70
            for i in pages[page][subpage]:
                appsurface.blit(font.render(i, 1, (200, 200, 200)), [w, h])
                if h >= height - 50:
                    subpage = subpage + 1
                    w = w + 300
                    h = 70 - 15
                h = h + 15
            ub = components().button(2, 2, 'Uninstall')
            ib = components().button(ub[1] + 2, 2, 'Install')
            upb = components().button(ib[1] + 2, 2, 'Check for Updates')
            fb = components().button(upb[1] + 2, 2, 'Fetch App')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            mousepos = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return
                    if event.key == pygame.K_LEFT:
                        if page != 0:
                            page = page - 1
                    if event.key == pygame.K_RIGHT:
                        if len(pages) - 1 != page:
                            page = page + 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return
                        if mousepos[0] >= ub[0] and mousepos[0] <= ub[1]:
                            if mousepos[1] >= ub[2] and mousepos[1] <= ub[3]:
                                name = components().inputbox('Application Name', ['Enter the name of the application to uninstall.'])
                                if name != 'Cancel':
                                    ask = components().ynbox('Are you sure?', ['Are you sure you want to uninstall:', name, "This will delete all it's data"])
                                    if ask == 'Yes':
                                        pman().uninstall(name)
                                        installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                        sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                        for a in sidemenuitems:
                                            if a == name:
                                                pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                                for ca in sidemenuitems:
                                                    if ca != name and ca != 'appmenu':
                                                        pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                        sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                        sidemenufinalitems = []
                                        for i in sidemenuitems:
                                            rf = c.readfile('res/' + i + '/req.txt', 'r')
                                            icon = rf[3]
                                            name = rf[4]
                                            execp = rf[2]
                                            sidemenufinalitems.append([name, icon, execp])
                                        installedappsfinal = []
                                        for inst in installedapps:
                                            rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                            icon = rf[3]
                                            name = rf[4]
                                            execp = rf[2]
                                            if rf[3] != 'noicon':
                                                installedappsfinal.append([name, icon, execp])
                                        installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                        inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                        pages = [[[]]]
                                        h = 70
                                        w = 22
                                        page = 0
                                        subpage = 0
                                        for item in installed:
                                            if h < height - 20:
                                                pages[page][subpage].append(item)
                                            elif h >= height - 20 and w < width - 300:
                                                h = 70
                                                w = w + 300
                                                subpage = subpage + 1
                                                pages[page].append([])
                                                pages[page][subpage].append(item)
                                            elif w >= width - 300:
                                                pages.append([[]])
                                                page = page + 1
                                                subpage = 0
                                                pages[page][subpage].append(item)
                                                h = 70
                                                w = 22
                                            h = h + 15
                                            h = 70
                                            w = 22
                                            page = 0
                                            subpage = 0
                        if mousepos[0] >= ib[0] and mousepos[0] <= ib[1]:
                            if mousepos[1] >= ib[2] and mousepos[1] <= ib[3]:
                                components().okbox('Application Path', ['Enter the filepath of the application to install.', 'This should be a .zip file.'])
                                path = files('return')
                                pman().install(path)
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in installed:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                        if mousepos[0] >= upb[0] and mousepos[0] <= upb[1]:
                            if mousepos[1] >= upb[2] and mousepos[1] <= upb[3]:
                                pman().updateapps()
                        if mousepos[0] >= fb[0] and mousepos[0] <= fb[1]:
                            if mousepos[1] >= fb[2] and mousepos[1] <= fb[3]:
                                an = components().inputbox('App Name', ['Enter the name of the app you wish to get.'])
                                pman().getapp(an)
                                installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                inlauncher = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in installed:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
def files(mode = 'browse', retmode = 'file', startdir = '', history = ['']):
    if mode == 'browse':
        lps().new('Files')
    elif mode == 'return' and retmode == 'file':
        lps().new('Files: Select a File.')
    elif mode == 'return' and retmode == 'directory':
        lps().new('Files: Select a Folder.')
    if startdir == '':
        starterdirs = ['res', 'user', 'var', 'temp', 'PythonOS.py', 'constants.py', 'kernel.py', 'commands.py', 'shell.py']
        path = ''
    elif startdir != '':
        starterdirs = os.listdir(startdir)
        path = startdir
    contents = starterdirs
    pages = [[starterdirs]]
    h = 70
    w = 22
    page = 0
    subpage = 0
    if history == ['']:
        hpos = 0
    elif history != ['']:
        hpos = len(history) - 1
    while True:
        appsurface.fill([50, 50, 50])
        subpage = 0
        w = 2
        h = 70
        for i in pages[page][subpage]:
            appsurface.blit(font.render(i, 1, (200, 200, 200)), [w, h])
            if h >= height - 50:
                subpage = subpage + 1
                w = w + 300
                h = 70 - 15
            h = h + 15
        appsurface.blit(font.render('Page ' + str(page + 1) + ' of ' + str(len(pages)), 1, [200, 200, 200]), [2, 45])
        sbtn = components().button(2, 2, 'Select File/Folder')
        cbtn = components().button(sbtn[1] + 2, 2, 'Copy')
        mbtn = components().button(cbtn[1] + 2, 2, 'Move')
        dbtn = components().button(mbtn[1] + 2, 2, 'Delete')
        nbtn = components().button(dbtn[1] + 2, 2, 'New Folder')
        gbtn = components().button(nbtn[1] + 2, 2, 'Go To')
        bbtn = components().button(gbtn[1] + 2, 2, ' < Back')
        fbtn = components().button(bbtn[1] + 2, 2, 'Forward >')
        if retmode == 'directory':
            srbtn = components().button(fbtn[1] + 2, 2, 'Use This Folder')
        appsurface.blit(ifont.render(path, 1, (200, 200, 200)), [2, 25])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            lps().end()
                            return None
                    if mousepos[0] >= sbtn[0] and mousepos[0] <= sbtn[1]:
                        if mousepos[1] >= sbtn[2] and mousepos[1] <= sbtn[3]:
                            atp = components().getinput(2, 20, '')
                            if c.fileordir(path + atp) == 'File':
                                if mode == 'browse':
                                    c.openfile(path + atp)
                                if mode == 'return':
				    lps().end()
                                    return path + atp
                            if c.fileordir(path + atp) == 'Directory':
                                path = path + atp
                                if path.endswith('/') == False:
                                    path = path + '/'
                                contents = os.listdir(path)
                                history.append(path)
                                hpos = len(history) - 1
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                    if mousepos[0] >= cbtn[0] and mousepos[0] <= cbtn[1]:
                        if mousepos[1] >= cbtn[2] and mousepos[1] <= cbtn[3]:
                            components().okbox('Select file to copy', ['Select the file to copy from.'])
                            read = files('return', 'file', path, history)
                            components().okbox('Select destination directory', ['Please select the destination directory.'])
                            new = files('return', 'directory', path, history) + components().inputbox('Name new file.', ['Name the copy destination file.'])
                            bgpd().newtask('copy', ['copy-job', 'c.copyfile().copy("' + read + '", "' + new + '")', 'c.msg("Copying", "Files")', 'notifyd().notify("File Copied", "Files")'], 'c.copyfile().setup("'+ read + '", "' + new + '")')
                    if mousepos[0] >= mbtn[0] and mousepos[0] <= mbtn[1]:
                        if mousepos[1] >= mbtn[2] and mousepos[1] <= mbtn[3]:
                            components().okbox('Select file to move', ['Select the file to move.'])
                            read = files('return', 'file', path, history)
                            components().okbox('Select destination directory', ['Please select the destination directory.'])
                            new = files('return', 'directory', path, history) + components().inputbox('Name new file.', ['Name the move destination file.'])
                            bgpd().newtask('copy', ['copy-job', 'c.copyfile().copy("' + read + '", "' + new + '")', 'c.msg("Copying", "Files")', 'notifyd().notify("File Moved", "Files")\nos.remove("' + read + '")'], 'c.copyfile().setup("'+ read + '", "' + new + '")')
                    if mousepos[0] >= dbtn[0] and mousepos[0] <= dbtn[1]:
                        if mousepos[1] >= dbtn[2] and mousepos[1] <= dbtn[3]:
                            sel = components().inputbox('Select the file to delete.', ['Please select the file or folder you wish to delete.'])
                            if c.fileordir(path + sel) == 'File':
                                if components().ynbox('Sure to delete?', ['Are you sure you want to delete the file?', 'The operation cannot be undone.']) == 'Yes':
                                    c.deletefile(path + sel)
                                    components().okbox('File deleted.', ['The file has been deleted.'])
                            if c.fileordir(path + sel) == 'Directory':
                                if components().ynbox('Sure to delete?', ['Are you sure you want to delete the folder?', 'The operation cannot be undone.', 'All the files within this folder will be removed.']) == 'Yes':
                                    for f in os.listdir(path + sel):
                                        c.deletefile(path + sel + f)
                                    os.rmdir(path + sel)
                                    components().okbox('Folder deleted.', ['The folder has been deleted.'])
                            contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if retmode == 'directory':
                        if mousepos[0] >= srbtn[0] and mousepos[0] <= srbtn[1]:
                            if mousepos[1] >= srbtn[2] and mousepos[1] <= srbtn[3]:
                                if mode == 'return':
                                    lps().end()
                                    return path
                    if mousepos[0] >= nbtn[0] and mousepos[0] <= nbtn[1]:
                        if mousepos[1] >= nbtn[2] and mousepos[1] <= nbtn[3]:
                            name = components().inputbox('Name the new directory', ['Enter the name for the new directory.', 'Please end with "/"'])
                            os.mkdir(path + name)
                            contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if mousepos[0] >= gbtn[0] and mousepos[0] <= gbtn[1]:
                        if mousepos[1] >= gbtn[2] and mousepos[1] <= gbtn[3]:
                            path = components().inputbox('Where to?', ['Enter the full path of the destination.', 'Please end with "/"'])
                            if c.fileordir(path) == 'File':
                                c.openfile(path)
                            if c.fileordir(path) == 'Directory':
                                if path.endswith('/') == False:
                                    path = path + '/'
                                contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if mousepos[0] >= bbtn[0] and mousepos[0] <= bbtn[1]:
                        if mousepos[1] >= bbtn[2] and mousepos[1] <= bbtn[3]:
                            if hpos == 1:
                                hpos = hpos - 1
                                path = ''
                                contents = starterdirs
                                pages = [[starterdirs]]
                            if hpos > 1:
                                hpos = hpos - 1
                                path = history[hpos]
                                contents = os.listdir(path)
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                    if mousepos[0] >= fbtn[0] and mousepos[0] <= fbtn[1]:
                        if mousepos[1] >= fbtn[2] and mousepos[1] <= fbtn[3]:
                            if hpos + 1 <= len(history) - 1:
                                hpos = hpos + 1
                                path = history[hpos]
                                contents = os.listdir(path)
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return None
                if event.key == pygame.K_LEFT:
                    if page != 0:
                        page = page - 1
                if event.key == pygame.K_RIGHT:
                    if len(pages) - 1 != page:
                        page = page + 1
                if event.key == pygame.K_RETURN:
                    pygame.mouse.set_pos([20, 40])
                    pygame.event.post(pygame.event.Event(pygame.MOUSEBUTTONDOWN, button = 1))
def pictures(path):
    lps().new('Pictures')
    if path == '':
        path = files('return')
    op = pygame.image.load(path)
    if op.get_width() > width:
        if op.get_height > height - 20:
            op = pygame.transform.smoothscale(op, [width, height])
    iw = op.get_width()
    ih = op.get_height()
    bp = centerobj(iw, ih, appsurface)
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        appsurface.blit(op, bp)
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
		    lps().end()
                    return
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                   if mousepos[1] >= 0 and mousepos[1] <= 20:
		       lps().end()
                       return
def music(mode = 'play', fp = ''):
    global vol
    if fp == '':
        try:
            playing = pvar.read('PLAYING', 'MUSIC')[0]
            stopped = pvar.read('STOPPED', 'MUSIC')[0]
            fp = pvar.read('FILEPATH', 'MUSIC')[0]
        except:
            pvar.make('PLAYING', 'no', 'MUSIC')
            pvar.make('STOPPED', 'yes', 'MUSIC')
            playing = pvar.read('PLAYING', 'MUSIC')[0]
            stopped = pvar.read('STOPPED', 'MUSIC')[0]
        if mode == 'play':
            if playing == 'no' and stopped == 'yes':
                fp = files('return')
                pvar.make('FILEPATH',fp ,'MUSIC')
                fp = pvar.read('FILEPATH', 'MUSIC')[0]
                pygame.mixer.init()
                pygame.mixer.music.load(fp)
    elif fp != '':
        pvar.make('PLAYING', 'no', 'MUSIC')
        pvar.make('STOPPED', 'yes', 'MUSIC')
        playing = pvar.read('PLAYING', 'MUSIC')[0]
        stopped = pvar.read('STOPPED', 'MUSIC')[0]
        if mode == 'play':
            if playing == 'no' and stopped == 'yes':
                pvar.make('FILEPATH',fp ,'MUSIC')
                fp = pvar.read('FILEPATH', 'MUSIC')[0]
                pygame.mixer.init()
                pygame.mixer.music.load(fp)
    pausei = pygame.image.load('res/music/pause.png')
    playi = pygame.image.load('res/music/play.png')
    stopi = pygame.image.load('res/music/stop.png')
    bplayi = pygame.image.load('res/music/bgplay.png')
    lps().new('Music')
    center = centerobj(pausei.get_width(), pausei.get_height(), appsurface)
    while True:
        clock.tick(fps)
        appsurface.fill([255, 250, 205])
        appsurface.blit(ifont.render(fp, 1, (50, 50,50)), [2, 20])
        if playing == 'yes' and stopped == 'no':
            cb = components().imagebutton(center[0], center[1], pausei)
        if playing == 'no' or stopped == 'yes':
            cb = components().imagebutton(center[0], center[1], playi)
        sb = components().imagebutton(center[0] - 202, center[1], stopi)
        bb = components().imagebutton(center[0] + 228, center[1], bplayi)
        vdb = components().button(center[0] - 102, center[1] + 80, 'Volume -')
        vub = components().button(center[0] + 98, center[1] + 80, 'Volume +')
        vtxt = bfont.render('Volume: ' + str(vol * 10), 1, [50, 50, 50])
        appsurface.blit(vtxt, [center[0] - 5, center[1] - 50])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= cb[0] and mousepos[0] <= cb[1]:
                    if mousepos[1] >= cb[2] and mousepos[1] <= cb[3]:
                        if playing == 'yes' and stopped == 'no':
                            pygame.mixer.music.pause()
                            pvar.make('PLAYING', 'no', 'MUSIC')
                            pvar.make('STOPPED', 'no', 'MUSIC')
                            playing = pvar.read('PLAYING', 'MUSIC')[0]
                            stopped = pvar.read('STOPPED', 'MUSIC')[0]
                        elif playing == 'no' and stopped == 'no':
                            pygame.mixer.music.unpause()
                            pvar.make('PLAYING', 'yes', 'MUSIC')
                            pvar.make('STOPPED', 'no', 'MUSIC')
                            playing = pvar.read('PLAYING', 'MUSIC')[0]
                            stopped = pvar.read('STOPPED', 'MUSIC')[0]
                        elif playing == 'no' and stopped == 'yes':
                            pygame.mixer.music.play()
                            pvar.make('PLAYING', 'yes', 'MUSIC')
                            pvar.make('STOPPED', 'no', 'MUSIC')
                            playing = pvar.read('PLAYING', 'MUSIC')[0]
                            stopped = pvar.read('STOPPED', 'MUSIC')[0]
                if mousepos[0] >= sb[0] and mousepos[0] <= sb[1]:
                    if mousepos[1] >= sb[2] and mousepos[1] <= sb[3]:
                        pygame.mixer.music.stop()
                        pvar.make('PLAYING', 'no', 'MUSIC')
                        pvar.make('STOPPED', 'yes', 'MUSIC')
                        playing = pvar.read('PLAYING', 'MUSIC')[0]
                        stopped = pvar.read('STOPPED', 'MUSIC')[0]
                if mousepos[0] >= bb[0] and mousepos[0] <= bb[1]:
                    if mousepos[1] >= bb[2] and mousepos[1] <= bb[3]:
                        lps().end()
                        return
                if mousepos[0] >= vub[0] and mousepos[0] <= vub[1]:
                    if mousepos[1] >= vub[2] and mousepos[1] <= vub[3]:
                        vol = vol + 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= vdb[0] and mousepos[0] <= vdb[1]:
                    if mousepos[1] >= vdb[2] and mousepos[1] <= vdb[3]:
                        vol = vol - 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        pygame.mixer.music.stop()
                        pvar.make('PLAYING', 'no', 'MUSIC')
                        pvar.make('STOPPED', 'yes', 'MUSIC')
                        return

def cleanup():
    import shell
    c.msg('Looking for files to delete.', 'CLEANUP')
    todel = ['commands.pyc', 'constants.pyc', 'shell.pyc', 'PythonOS.pyc']
    index = c.readfile('data/system/index.txt')
    for cand in index:
        if c.fileordir(cand) == 'File':
            if cand.endswith('.pyc') == True:
                todel.append(cand)
            if cand.endswith('~') == True:
                todel.append(cand)
            if cand.startswith('temp/') == True:
                todel.append(cand)
    c.msg('Will delete ' + str(len(todel)) + ' files.', 'CLEANUP')
    for f in todel:
        try:
            os.remove(f)
            c.msg('Removed ' + f, 'CLEANUP')
        except:
            c.msg('Failed to remove ' + f, 'CLEANUP')
    if shell.grunning == True:
        components().okbox('Cleanup finished.', ['Cleanup has removed ' + str(len(todel)) + ' junk files.'])
    if shell.grunning != True:
        c.msg('Deleted ' + str(len(todel)) + ' junk files.', 'CLEANUP') 
def adduser():
    import shell
    if shell.grunning == False:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            name = raw_input ('Desired Username: ')
            rname = raw_input ('Your name: ')
            pwd = c.shiftchar(raw_input ('Your desired password (lowercase letters only): '))
            other = []
            print 'Enter any other desired information, then write "done"'
            uin = ''
            while uin != 'done':
                uin = raw_input ()
                other.append(uin)
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            print 'You must be root to add users.'
    if shell.grunning == True:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            ask = components().ynbox('Add a new user?', ['Do you want to add a new user?']) 
            if ask == 'Yes':
                name = components().inputbox('Username', ['Your desired username:'])
                rname = components().inputbox('Real Name', ['Your name:'])
                pwd = c.shiftchar(components().inputbox('Password', ['Your desired password (lowercase letters only):']))
                other = []
                components().okbox('Other Information', ['Enter any other desired information, then write "done"'])
                uin = ''
                h = 86
                while uin != 'done':
                    uin = components().inputbox('Other info', ['Any other information you wish to submit:'])
                    other.append(uin)
                    h = h + 28
            if ask != 'Yes':
                return
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            components().okbox('Must be root.', ['You must be root to add users.'])
    try:
        import shutil
        shutil.copytree('res/system/default-vars/', 'data/system/' + name + '/')
        os.mkdir('user/' + name + '/')
        up = 'user/' + name + '/'
        os.mkdir(up + 'apps/')
        os.mkdir(up + 'Downloads/')
        os.mkdir(up + 'Documents/')
        os.mkdir(up + 'Pictures/')
        os.mkdir(up + 'Music/')
        random_str = ['pythonos', 'password', '123456', 'l0g1n', 'pyth0nos', '123abc']
        import random
        fp1 = c.shiftchar(random_str[random.randint(0, 5)])
        fp2 = c.shiftchar(random_str[random.randint(0, 5)])
        pf = c.makefile(up + name + '.pwd')
        print >> pf, fp1
        print >> pf, pwd
        print >> pf, fp2
        pf.close()
        infof = c.makefile(up + 'user.txt')
        print >> infof, rname
        for ln in other:
	    if ln != 'done':
                print >> infof, ln
        infof.close()
        pvar.append('USERS', name, 'GLOBAL')
    except:
        c.msg('Error creating user.', 'ADDUSR')
def remuser():
    import shell
    import shutil
    if shell.grunning == False:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            name = raw_input ('Name of user to remove: ')
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            print 'You must be root to remove users.'
            return
    if shell.grunning == True:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            ask = components().ynbox('Sure to remove?', ['Are you sure you want to remove a user?'])
            if ask == 'Yes':
                name = components().inputbox('User Name', ['Name of user to remove:'])
            if ask != 'Yes':
                return
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            components().okbox('Must be root.', ['You must be root to remove users.'])
            return
    try:
        if name != 'root':
	    shutil.rmtree('user/' + name + '/')
	    shutil.rmtree('data/system/' + name + '/')
            users = pvar.read('USERS', 'GLOBAL')
            pvar.destroy('USERS', 'GLOBAL')
            pvar.make('USERS', 'root', 'GLOBAL')
            for user in users:
                if user != 'root' and user != name:
                    pvar.append('USERS', user, 'GLOBAL')
            return
    except:
        c.msg('Error removing user.', 'REMUSER')
def addtoindex():
    print "Blank placeholder. Use constants.addtoindexusr()"
def remfromindex():
    print "This is just a placeholder. Use constants.removefromindexusr()"
def nestedsession():
    import shell
    if shell.grunning == True:
	yes = False
        if components().ynbox('Launch a Nesdted Session?', ['Launching a nested session will freeze graphics.']) == 'Yes':
            yes = True
    if shell.grunning == False:
        yes = True
    if yes == True:
        import runpy
        import pvar
        pvar.make('NSR', 'yes', 'NESTEDSESSION')
        runpy.run_path('kernel.py')
        pvar.destroy('NSR', 'NESTEDSESSION')
def gunzip(fp = 'find'):
    import zipfile
    if fp == 'find':
        components().okbox('Select the .zip file.', ['Select the .zip file you wish to unpack.'])
        fp = files('return')
    zf = zipfile.ZipFile(fp, 'r')
    nlst = zf.namelist()
    components().okbox('Select Destination', ['Select the directory to unpack to.'])
    dest = files('return', 'directory')
    for item in nlst:
        c.msg('Extracting ' + item, 'ZEXTRACT')
        zf.extract(item, dest)
    zf.close()
    components().okbox('Extract Complete.', ['.zip extraction complete.'])
def dirlink():
    components().okbox('Select the Directory', ['Select the folder to create the link for.'])
    ddir = files('return', 'directory')
    name = components().inputbox('Name the Link', ['Enter a name for your link.', 'This will be the Applications icon name.'])
    cmd = components().inputbox('Name the Command', ['Enter a name for your command. No spaces are allowed.', 'This will be the executable command.'])
    os.mkdir('res/' + cmd + '/')
    os.mkdir('data/' + cmd + '/')
    rf = open('res/' + cmd + '/req.txt', 'w')
    print >> rf, 'req.txt'
    print >> rf, 'PythonOS.files("browse", "file", "' + ddir + '")'
    print >> rf, cmd
    print >> rf, 'res/dirlink/dl.png'
    print >> rf, name
    print >> rf, '1.0'
    c.addcmd(cmd, 'PythonOS.files("browse", "file", "' + ddir + '")')
    rf.close()
    scmd = cmd.split(' .,  -')
    ncmd = ''
    for part in scmd:
        scmd = part + '_'
        pvar.append('INSTALLEDAPPS', cmd, 'GLOBAL')
    components().okbox('Link added', ['Restart Python OS to use the link.', 'The link is installed as a command.', 'Use uninstall to remove it.'])
def sysshell():
    import shell
    import subprocess
    opsys = pvar.read('CURROS', 'KERNEL')[0]
    if opsys == 'win32':
        sym = '> '
    elif opsys == 'linux2':
        sym = '$ '
    elif opsys != 'win32' or opsys != 'linux2':
        sym = '$ '
    if shell.grunning == False:
        cmd = raw_input (sym)
    if shell.grunning == True:
        cmd = components().inputbox('Enter Command.', ['Enter the command you wish to execute.'])
        if cmd == 'Cancel':
            return
    os.system(cmd)
class quickboot():
    def down(self):
        import shell
        if shell.grunning == True:
            rb = components().ynbox('Exit in QuickBoot Mode?', ['Do you wish to exit Python OS in QuickBoot Mode?', 'You will be logged in automatically at the next boot.'])
        if shell.grunning == False:
            uin = raw_input ('Exit in QuickBoot Mode (Y/n)')
            rb = 'No'
            if uin == 'Y' or uin == 'y':
                rb = 'Yes'
        if rb == 'Yes':
            ksf = c.makefile('boot.kstr')
            print >> ksf, shell.user
            ksf.close()
            commands.parsecmd(['killg'])
            return
    def up(self):
        rksf = c.readfile('boot.kstr', 'rU')
        c.deletefile('boot.kstr')
        return rksf[0]
def runcmd(cmd = ''):
    import shell
    if cmd == '':
        if shell.grunning == True:
            cmd = components().inputbox('Enter the Command to Run.', ['Enter the command you wish to run.', 'Make your Python Shell visible for text output.'])
            if cmd == 'Cancel':
                return
        if shell.grunning == False:
            cmd = raw_input ('Enter the command you wish to run: ')
    commands.parsecmd(cmd.split())
def sleep():
    clr = [200, 200, 200]
    while clr[0] != 0:
        clock.tick(fps)
        screen.fill(clr)
        pygame.display.update()
        clr = [clr[0] - 5, clr[1] - 5, clr[2] - 5]
    while True:
        clock.tick(1)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                return
            if event.type == pygame.MOUSEMOTION:
                return
            if event.type == pygame.MOUSEBUTTONDOWN:
                return
    
def toolbarman(app):
    global toolbarplugstarters
    global toolbarplugs
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 150, mp[1]])
        rem = components().button(mp[0] - 148, mp[1] + 25, 'Unpin from Toolbar')
        run = components().button(mp[0] - 148, mp[1] + 48, 'Open App')
        new = components().button(mp[0] - 148, mp[1] + 71, 'New Link')
        screen.blit(appsurface, [2, 20])
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        components().menu()
        components().mouse()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                    if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                        toolbarplugs = []
                        tbapps = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
                        pvar.destroy('TOOLBARPLUGINS', 'GLOBAL')
                        pvar.make('TOOLBARPLUGINS', 'clock', 'GLOBAL')
                        for tbapp in tbapps:
                            if tbapp != app and tbapp != 'clock':
                                pvar.append('TOOLBARPLUGINS', tbapp, 'GLOBAL')
                        toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
                        for plg in toolbarplugstarters:
                            rf = c.readfile('res/' + plg + '/toolbar.txt')
                            icon = rf[0]
                            run = rf[1]
                            toolbarplugs.append([icon, run])
                        return
                if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                    if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                        commands.parsecmd([app])
                        return
                if mousepos[0] >= new[0] and mousepos[0] <= new[1]:
                    if mousepos[1] >= new[2] and mousepos[1] <= new[3]:
                        toolbaradd()
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 150 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 150 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 150 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
def toolbargui():
    while True:
        clock.tick(fps)
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        bw = width - 35
        clickables = []
        for plg in toolbarplugs:
            ico = pygame.image.load(plg[0])
            bw = bw - ico.get_width()
            bw = bw - 2
            screen.blit(ico, [bw, 0])
            clickables.append([[bw, bw + ico.get_width(), 0, 20], plg[1]])
        mousepos = pygame.mouse.get_pos()
        components().mouse()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                for abutton in clickables:
                    if mousepos[0] >= abutton[0][0] and mousepos[0] <= abutton[0][1]:
                        if mousepos[1] >= abutton[0][2] and mousepos[1] <= abutton[0][3]:
                            if event.button == 1:
                                commands.parsecmd([abutton[1]])
                                return
                            if event.button == 3:
                                toolbarman(abutton[1])
        if mousepos[1] >= 20:
            return
def search():
    pygame.image.save(appsurface, 'temp/search.jpg')
    sbg = pygame.image.load('temp/search.jpg')
    lps().new('Search')
    pygame.draw.rect(appsurface, [150, 150, 150], [width - 700, 0, 700, height - 20])
    pygame.display.update()
    q = components().getinput(width - 698, 2, '')
    pygame.draw.rect(appsurface, [150, 150, 150], [width - 700, 0, 700, height - 20])
    pygame.display.update()
    appsurface.blit(font.render(q, 1, (50, 50, 50)), [width - 680, 22])
    pygame.display.update()
    import re
    index = c.readfile('data/system/index.txt')
    ff = []
    ffi = []
    for item in index:
        if c.fileordir(item) == 'Directory':
            if item.endswith(q) == True:
                ff.append(item)
            elif item.endswith(q) == False:
                words = re.split('\W+', item)
                for word in words:
                    if word == q:
                        ff.append(item)
        if c.fileordir(item) == 'File':
            if item.endswith(q) == True:
                ffi.append(item)
            elif item.endswith(q) == False:
                words = re.split('\W+', item)
                for word in words:
                    if word == q:
                        ffi.append(item)
    displaylist = []
    displaylist.append('Folders:')
    if ff == []:
        displaylist.append('No Folders Found.')
    for item in ff:
        displaylist.append(item)
    displaylist.append('')
    displaylist.append('Files:')
    if ffi == []:
        displaylist.append('No Files Found.')
    for item in ffi:
        displaylist.append(item)
    rs = pygame.Surface((700, height - 20))
    rs.fill([150, 150, 150])
    appsurface.blit(rs, [width - 700, 0])
    pygame.display.update()
    pages = [[]]
    h = 22
    page = 0
    for item in displaylist:
        if h <= height - 40:
            pages[page].append(item)
        if h >= height - 40:
            pages.append([])
            page = page + 1
            pages[page].append(item)
            h = 22
        h = h + 15
    page = 0
    while True:
        clock.tick(fps)
        h = 22
        appsurface.blit(sbg, [0, 0])
        rs.fill([150, 150, 150])
        for i in pages[page]:
            rs.blit(font.render(i, 1, (50, 50, 50)), [20, h])
            h = h + 15
        rs.blit(font.render('Page ' + str(page + 1) + ' of ' + str(len(pages)), 1, [50, 50, 50]), [20, 2])
        appsurface.blit(rs, [width - 700, 0])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    mousepos = pygame.mouse.get_pos()
                    mwidth = mousepos[0]
                    mheight = mousepos[1]
                    if mwidth >= 0 and mwidth <= 20:
                        if mheight >= 0 and mheight <= 20:
                            lps().end()
                            return
                    if mwidth >= 0 and mwidth <= width - 700:
                        if mheight >= 20 and mheight <= height:
                            lps().end()
                            return
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return
                if event.key == pygame.K_LEFT:
                    if page != 0:
                        page = page - 1
                if event.key == pygame.K_RIGHT:
                    if len(pages) - 1 != page:
                        page = page + 1
def reload_data(silent = False):
    global sidemenufinalitems
    global sidemenuitems
    global installedapps
    global installedappsfinal
    global toolbarplugstarters
    global toolbarplugs
    global width
    global height
    global appsurface
    global commands
    if silent == False:
        screen.fill([200, 200, 200])
        screen.blit(font.render('Indexing', 1, (50, 50, 50)), [2, 2])
        pygame.display.update()
    c.index()
    if silent == False:
        screen.blit(font.render('Reloading Apps', 1, (50, 50, 50)), [2, 22])
        pygame.display.update()
    installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    installedappsfinal = []
    for inst in installedapps:
        rf = c.readfile('res/' + inst + '/req.txt', 'rU')
        icon = rf[3]
        name = rf[4]
        execp = rf[2]
        if rf[3].startswith('noicon') == False:
            installedappsfinal.append([name, icon, execp])
    if silent == False:
        screen.blit(font.render('Reloading Menu Shortcuts', 1, (50, 50, 50)), [2, 42])
        pygame.display.update()
    sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
    sidemenufinalitems = []
    for i in sidemenuitems:
        rf = c.readfile('res/' + i + '/req.txt', 'rU')
        icon = rf[3]
        name = rf[4]
        execp = rf[2]
        sidemenufinalitems.append([name, icon, execp])
    if silent == False:
        screen.blit(font.render('Reloading Toolbar Plugins', 1, (50, 50, 50)), [2, 62])
        pygame.display.update()
    toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
    toolbarplugs = []
    for plg in toolbarplugstarters:
        rf = c.readfile('res/' + plg + '/toolbar.txt')
        icon = rf[0]
        run = rf[1]
        toolbarplugs.append([icon, run])
    if silent == False:
        screen.blit(font.render('Refreshing Screen', 1, (50, 50, 50)), [2, 82])
        pygame.display.update()
    width = pygame.display.Info().current_w
    height = pygame.display.Info().current_h
    appsurface = pygame.Surface((width - 2, height - 20))
    c.deletefile('commands.pyc')
    reload(commands)
    commands.parsecmd(['reload-pyos'])
    if silent == False:
        components().okbox('Finished.', ['Your Python OS system has been refreshed.', 'This cannot replace a restart.', 'To clean up junk files, use the cleanup app.'])
    return
def clockapp():
    lps().new('Clock')
    import datetime
    while True:
        clock.tick(10)
        dt = str(datetime.datetime.now()).split()
        rttb = ''
        for char in dt[1]:
            if char != '.':
                rttb = rttb + char
            if char == '.':
                break
        rt = largebfont.render(rttb, 1, (50, 50, 50))
        rd = largeifont.render(dt[0], 1, (50, 50, 50))
        rdc = centerobj(rd.get_width(), rd.get_height(), screen)
        appsurface.blit(bg, [0, 0])
        appsurface.blit(rt, centerobj(rt.get_width(), rt.get_height(), screen))
        appsurface.blit(rd, [rdc[0], height - 100])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                mousepos = pygame.mouse.get_pos()
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        return
class notifyd():
    def setup(self):
        c.logentry('NOTIFYD', 'Notification Daemon Set Up.')
        qfile = c.makefile('data/notifyd/queue.txt')
        print >> qfile, 'Welcome, ' + c.readfile('user/' + shell.user + '/user.txt')[0]
        print >> qfile, 'Python OS'
        qfile.close()
        global notifyd_var_ticks
        global notifyd_var_trem
        global notifyd_var_count
        notifyd_var_ticks = 0
        notifyd_var_trem = fps * 10
        notifyd_var_count = 0
        c.logentry('NOTIFYD', 'Queue: data/notifyd/queue.txt')
        c.logentry('NOTIFYD', 'Trem: ' + str(notifyd_var_trem))
        c.logentry('NOTIFYD', 'Count: ' + str(notifyd_var_count))
        c.logentry('NOTIFYD', 'Ticks: ' + str(notifyd_var_ticks))
    def notify(self, text, app = 'Notification'):
        qfile = open('data/notifyd/queue.txt', 'a+')
        print >> qfile, text
        print >> qfile, app
        qfile.close()
    def run(self):
        global notifyd_var_ticks
        global notifyd_var_trem
        global notifyd_var_count
        if notifyd_var_ticks == 100 or notifyd_var_count > 0:
            notifyd_var_ticks = 0
            queue = c.readfile('data/notifyd/queue.txt')
            if len(queue) - 1 >= 0:
                if notifyd_var_count <= notifyd_var_trem:
                    cpos = 0
                    w = width - 400
                    h = 25
                    while cpos <= len(queue) - 2:
                        pygame.draw.rect(screen, [75, 75, 75], [w, h, 390, 60])
                        screen.blit(font.render(queue[cpos], 1, (200, 200, 200)), [w + 4, h + 5])
                        screen.blit(bfont.render(queue[cpos + 1], 1, (200, 200, 200)), [w + 4, h + 35])
                        pygame.display.update()
                        cpos = cpos + 2
                        h = h + 62
                    notifyd_var_count = notifyd_var_count + 1
                elif notifyd_var_count > notifyd_var_trem:
                    queue = c.makefile('data/notifyd/queue.txt')
                    queue.close()
                    notifyd_var_count = 0
                    notifyd_var_ticks = 0
        elif notifyd_var_ticks < 100:
            notifyd_var_ticks = notifyd_var_ticks + 1
                
def sandbox():
    print 'Empty. Use constants.securetools().sandbox()'
def calculator():
    lps().new('Calculator')
    pygame.image.save(appsurface, 'temp/cbg.jpg')
    cbg = pygame.image.load('temp/cbg.jpg')
    inf = pvar.read('INFO', 'CALCULATOR')
    cpos = [20, 20]
    if len(inf) - 1 > 0:
        if inf[1] != '0.0' and inf[1] != '':
            try:
                showing = inf[1]
                snum = int(inf[1])
                n2 = int(inf[2])
                op = inf[3]
                stage = int(inf[4])
            except:
                showing = inf[1]
                snum = float(inf[1])
                n2 = float(inf[2])
                op = inf[3]
                stage = int(inf[4])
        elif inf[1] == '0.0' or inf[1] == '':
            inf = ['calc-info']
    if len(inf) - 1 == 0:
        showing = ''
        snum = 0.0
    n2 = 0.0
    op = '+'
    stage = 0
    res = 0
    dragging = False
    while True:
        if fps <= 30 and fps >= 15:
            clock.tick(fps - 5)
        elif fps <= 10:
            clock.tick(fps)
        appsurface.blit(cbg, [0, 0])
        pygame.draw.rect(appsurface, [75, 75, 75], [cpos[0], cpos[1], 110, 240])
        appsurface.blit(font.render('Calculator', 1, (200, 200, 200)), [cpos[0] + 2, cpos[1] + 2])
        bar = components().inputbutton(cpos[0] + 2, cpos[1] + 20, showing)
        b1 = components().button(cpos[0] + 2, bar[3] + 2, ' 1 ')
        b2 = components().button(b1[1] + 2, bar[3] + 2, ' 2 ')
        b3 = components().button(b2[1] + 2, bar[3] + 2, ' 3 ')
        b4 = components().button(cpos[0] + 2, b1[3] + 2, ' 4 ')
        b5 = components().button(b4[1] + 2, b1[3] + 2, ' 5 ')
        b6 = components().button(b5[1] + 2, b1[3] + 2, ' 6 ')
        b7 = components().button(cpos[0] + 2, b4[3] + 2, ' 7 ')
        b8 = components().button(b7[1] + 2, b4[3] + 2, ' 8 ')
        b9 = components().button(b8[1] + 2, b4[3] + 2, ' 9 ')
        b0 = components().button(cpos[0] + 2, b7[3] + 2, ' 0 ')
        bp = components().button(b3[1] + 2, bar[3] + 2, ' + ')
        bm = components().button(b6[1] + 2, bp[3] + 2, '  - ')
        bmu = components().button(b9[1] + 2, bm[3] + 2, ' * ')
        bc = components().button(b0[1] + 2, b7[3] + 2, ' C ')
        be = components().button(bc[1] + 2, b7[3] + 2, ' = ')
        bd = components().button(be[1] + 2, bmu[3] + 2, ' / ')
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        if dragging == True:
            cpos = [mousepos[0] - 2, mousepos[1] - 20]
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP:
                if dragging == True:
                    dragging = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        return
                if mousepos[0] >= bar[0] and mousepos[0] <= bar[1]:
                    if mousepos[1] >= bar[2] and mousepos[1] <= bar[3]:
                        if stage == 0:
                            snum = components().getinput(bar[0], bar[2] - 20, str(snum))
                            if snum.endswith('.0'):
                                snum = int(float(snum))
                            elif snum.endswith('.0') == False:
                                snum = float(snum)
                            showing = str(snum)
                        if stage == 2:
                            n2 = components().getinput(bar[0], bar[2] - 20, str(n2))
                            if n2.endswith('.0'):
                                n2 = int(float(n2))
                            elif n2.endswith('.0') == False:
                                n2 = float(n2)
                            showing = str(n2)
                if mousepos[0] < cpos[0] + 2 and mousepos[0] >= 2:
                    if mousepos[1] >= 20 and mousepos[1] <= height:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] > cpos[0] + 127 and mousepos[0] <= width:
                    if mousepos[1] >= 20 and mousepos[1] <= height:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] >= 0 and mousepos[0] <= width:
                    if mousepos[1] >= 20 and mousepos[1] < cpos[1] + 20:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] >= 0 and mousepos[0] <= width:
                    if mousepos[1] > cpos[1] + 260 and mousepos[1] <= height:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] >= cpos[0] + 2 and mousepos[0] <= cpos[0] + 112:
                    if mousepos[1] >= cpos[1] + 20 and mousepos[1] <= cpos[1] + 40:
                        dragging = True
                if mousepos[0] >= bp[0] and mousepos[0] <= bp[1]:
                    if mousepos[1] >= bp[2] and mousepos[1] <= bp[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '+'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bm[0] and mousepos[0] <= bm[1]:
                    if mousepos[1] >= bm[2] and mousepos[1] <= bm[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '-'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bmu[0] and mousepos[0] <= bmu[1]:
                    if mousepos[1] >= bmu[2] and mousepos[1] <= bmu[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '*'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bd[0] and mousepos[0] <= bd[1]:
                    if mousepos[1] >= bd[2] and mousepos[1] <= bd[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '/'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bc[0] and mousepos[0] <= bc[1]:
                    if mousepos[1] >= bc[2] and mousepos[1] <= bc[3]:
                        stage = 0
                        snum = 0.0
                        showing = ''
                        n2 = 0.0
                        op = '+'
                if mousepos[0] >= be[0] and mousepos[0] <= be[1]:
                    if mousepos[1] >= be[2] and mousepos[1] <= be[3]:
                        if stage == 2:
                            if op == '+':
                                res = snum + n2
                            if op == '-':
                                res = snum - n2
                            if op == '*':
                                res = snum * n2
                            if op == '/':
                                res = snum / n2
                            showing = str(res)
                            snum = res
                            stage = 0
                if mousepos[0] >= b1[0] and mousepos[0] <= b1[1]:
                    if mousepos[1] >= b1[2] and mousepos[1] <= b1[3]:
                        if stage == 0:
                            showing = showing + '1'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '1'
                            n2 = float(showing)
                if mousepos[0] >= b2[0] and mousepos[0] <= b2[1]:
                    if mousepos[1] >= b2[2] and mousepos[1] <= b2[3]:
                        if stage == 0:
                            showing = showing + '2'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '2'
                            n2 = float(showing)
                if mousepos[0] >= b3[0] and mousepos[0] <= b3[1]:
                    if mousepos[1] >= b3[2] and mousepos[1] <= b3[3]:
                        if stage == 0:
                            showing = showing + '3'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '3'
                            n2 = float(showing)
                if mousepos[0] >= b4[0] and mousepos[0] <= b4[1]:
                    if mousepos[1] >= b4[2] and mousepos[1] <= b4[3]:
                        if stage == 0:
                            showing = showing + '4'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '4'
                            n2 = float(showing)
                if mousepos[0] >= b5[0] and mousepos[0] <= b5[1]:
                    if mousepos[1] >= b5[2] and mousepos[1] <= b5[3]:
                        if stage == 0:
                            showing = showing + '5'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '5'
                            n2 = float(showing)
                if mousepos[0] >= b6[0] and mousepos[0] <= b6[1]:
                    if mousepos[1] >= b6[2] and mousepos[1] <= b6[3]:
                        if stage == 0:
                            showing = showing + '6'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '6'
                            n2 = float(showing)
                if mousepos[0] >= b7[0] and mousepos[0] <= b7[1]:
                    if mousepos[1] >= b7[2] and mousepos[1] <= b7[3]:
                        if stage == 0:
                            showing = showing + '7'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '7'
                            n2 = float(showing)
                if mousepos[0] >= b8[0] and mousepos[0] <= b8[1]:
                    if mousepos[1] >= b8[2] and mousepos[1] <= b8[3]:
                        if stage == 0:
                            showing = showing + '8'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '8'
                            n2 = float(showing)
                if mousepos[0] >= b9[0] and mousepos[0] <= b9[1]:
                    if mousepos[1] >= b9[2] and mousepos[1] <= b9[3]:
                        if stage == 0:
                            showing = showing + '9'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '9'
                            n2 = float(showing)
                if mousepos[0] >= b0[0] and mousepos[0] <= b0[1]:
                    if mousepos[1] >= b0[2] and mousepos[1] <= b0[3]:
                        if stage == 0:
                            showing = showing + '0'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '0'
                            n2 = float(showing)
                            
def root_home_dir_link():
    print "This is an empty file."
def volumeplugin():
    global vol
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    mp = [mp[0] - 50, mp[1]]
    pygame.mixer.init()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 50, mp[1]])
        p = components().button(mp[0] - 48, mp[1] + 2, ' + ')
        m = components().button(mp[0] - 20, mp[1] + 2, ' - ')
        appsurface.blit(largefont.render(str(vol * 10), 1, (50, 50, 50)), [mp[0] - 46, mp[1] + 50])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= p[0] and mousepos[0] <= p[1]:
                    if mousepos[1] >= p[2] and mousepos[1] <= p[3]:
                        vol = vol + 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= m[0] and mousepos[0] <= m[1]:
                    if mousepos[1] >= m[2] and mousepos[1] <= m[3]:
                        vol = vol - 0.1
                        pygame.mixer.music.set_volume(vol)  
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 50 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
def hola_greeter():
    global toolbarplugs
    global toolbarplugstarters
    gserver()
    gui().boot(True)
    toolbarplugs = []
    toolbarplugstarters = []
    hbgimg = pygame.transform.smoothscale(pygame.image.load('res/hola-greeter/hola-bg.png'), (width, height))
    enter = pygame.image.load('res/hola-greeter/enter.png')
    entercenter = centerobj(40, 40, screen)
    unb = components().inputbutton(50, 50, 'User Name')
    unbcenter = centerobj(unb[1] - unb[0], unb[3] - unb[2], screen)
    pwb = components().inputbutton(50, 50, 'Password')
    pwbcenter = centerobj(pwb[1] - pwb[0], pwb[3] - pwb[2], screen)
    uname = ''
    pwd = ''
    lps().new('Hola! Greeter')
    while True:
        clock.tick(fps)
        appsurface.blit(hbgimg, [0, 0])
        if uname == '':
            unb = components().inputbutton(unbcenter[0] - 200, unbcenter[1], 'User Name')
        elif uname != '':
            unb = components().inputbutton(unbcenter[0] - 200, unbcenter[1], uname)
        pwb = components().inputbutton(pwbcenter[0] + 200, pwbcenter[1], 'Password')
        enb = components().imagebutton(entercenter[0], entercenter[1], enter)
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                commands.parsecmd(['killg'])
                return
            if event.type == pygame.MOUSEBUTTONDOWN:
                mousepos = pygame.mouse.get_pos()
                if mousepos[0] >= unb[0] and mousepos[0] <= unb[1]:
                    if mousepos[1] >= unb[2] and mousepos[1] <= unb[3]:
                        uname = components().getinput(unbcenter[0] - 200, unbcenter[1], uname)
                if mousepos[0] >= pwb[0] and mousepos[0] <= pwb[1]:
                    if mousepos[1] >= pwb[2] and mousepos[1] <= pwb[3]:
                        pwd = components().getinput(pwbcenter[0] + 200, pwbcenter[1], pwd)
                if mousepos[0] >= enb[0] and mousepos[0] <= enb[1]:
                    if mousepos[1] >= enb[2] and mousepos[1] <= enb[3]:
                        lps().end()
                        lps().end()
                        return uname, pwd

def volumeplugin():
    global vol
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    mp = [mp[0] - 50, mp[1]]
    pygame.mixer.init()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 50, mp[1]])
        p = components().button(mp[0] - 48, mp[1] + 2, ' + ')
        m = components().button(mp[0] - 20, mp[1] + 2, ' - ')
        appsurface.blit(largefont.render(str(vol * 10), 1, (50, 50, 50)), [mp[0] - 46, mp[1] + 50])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= p[0] and mousepos[0] <= p[1]:
                    if mousepos[1] >= p[2] and mousepos[1] <= p[3]:
                        vol = vol + 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= m[0] and mousepos[0] <= m[1]:
                    if mousepos[1] >= m[2] and mousepos[1] <= m[3]:
                        vol = vol - 0.1
                        pygame.mixer.music.set_volume(vol)  
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 50 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
def CatcherGame():
    global width
    global height
    lps().new('Catcher Game')
    appsurface.fill([50, 50, 50])
    CatcherGameBackground = pygame.image.load('res/catchergame/background.png')
    CatcherGameUFO = pygame.image.load('res/catchergame/UFO.png')
    CatcherGameUFOWidth = 500
    CatcherGameUFOHeight = 500
    CatcherGamePlayer = pygame.image.load('res/catchergame/Player.png')
    CatcherGamePlayerWidth = 0
    CatcherGamePlayerHeight = 0
    CatcherGameItem = pygame.image.load('res/catchergame/Item.png')
    CatcherGameItemWidth = random.randint(100, 1366)
    CatcherGameItemHeight = random.randint(5, 768)
    CatcherGameGotItem = False
    cd = 3
    pygame.key.set_repeat(1, 1)
    while cd != 0:
        clock.tick(1)
        appsurface.blit(CatcherGameBackground, [0, 0])
        appsurface.blit(largebfont.render(str(cd), 1, (50, 50, 50)), centerobj(40, 40, appsurface))
        screen.blit(appsurface, [2, 20])
        pygame.display.update()
        cd = cd - 1
    while True:
        clock.tick(20)
        appsurface.blit(CatcherGameBackground, [0, 0])
        appsurface.blit(CatcherGamePlayer, [CatcherGamePlayerWidth, CatcherGamePlayerHeight])
        appsurface.blit(CatcherGameUFO, [CatcherGameUFOWidth, CatcherGameUFOHeight])
        appsurface.blit(CatcherGameItem, [CatcherGameItemWidth, CatcherGameItemHeight])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return
                if event.key == pygame.K_F4:
                    recovery(appsurface)
                if event.key == pygame.K_F3:
                    main.launcher(1, 1, [1, 1])
                if event.key == pygame.K_SPACE:
                    done = False
                    text = largefont.render('Paused.', 1, (100, 100, 100))
                    while done != True:
                        clock.tick(fps)
                        appsurface.blit(CatcherGameBackground, [0, 0])
                        appsurface.blit(CatcherGamePlayer, [CatcherGamePlayerWidth, CatcherGamePlayerHeight])
                        appsurface.blit(CatcherGameUFO, [CatcherGameUFOWidth, CatcherGameUFOHeight])
                        appsurface.blit(CatcherGameItem, [CatcherGameItemWidth, CatcherGameItemHeight])
                        appsurface.blit(text, [100, 20])
                        components().toolbar()
                        components().menu()
                        screen.blit(appsurface, [2, 20])
                        pygame.display.update()
                        for event in pygame.event.get():
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_ESCAPE:
                                    lps().end()
                                    return
                                if event.key == pygame.K_SPACE:
                                    done = True
                if event.key == pygame.K_RIGHT:
                    CatcherGamePlayerWidth = CatcherGamePlayerWidth + 6
                if event.key == pygame.K_LEFT:
                    CatcherGamePlayerWidth = CatcherGamePlayerWidth - 6
                if event.key == pygame.K_DOWN:
                    CatcherGamePlayerHeight = CatcherGamePlayerHeight + 6
                if event.key == pygame.K_UP:
                    CatcherGamePlayerHeight = CatcherGamePlayerHeight - 6
        CatcherGameUFORangeWidth = CatcherGameUFOWidth + 40
        CatcherGameUFORangeHeight = CatcherGameUFOHeight + 40
        CatcherGamePlayerRangeWidth = CatcherGamePlayerWidth + 13
        CatcherGamePlayerRangeHeight = CatcherGamePlayerHeight + 10
        if CatcherGamePlayerWidth >= CatcherGameUFOWidth:
            CatcherGameUFOWidth = CatcherGameUFOWidth + 4
        if CatcherGamePlayerWidth <= CatcherGameUFOWidth:
            CatcherGameUFOWidth = CatcherGameUFOWidth - 4
        if CatcherGamePlayerHeight >= CatcherGameUFOHeight:
            CatcherGameUFOHeight = CatcherGameUFOHeight + 4
        if CatcherGamePlayerHeight <= CatcherGameUFOHeight:
            CatcherGameUFOHeight = CatcherGameUFOHeight - 4
        if CatcherGameUFOWidth <= 60:
            CatcherGameUFOWidth = CatcherGameUFOWidth + 5
        if CatcherGamePlayerWidth <= CatcherGameUFORangeWidth and CatcherGamePlayerWidth >= CatcherGameUFOWidth:
            if CatcherGamePlayerHeight <= CatcherGameUFORangeHeight and CatcherGamePlayerHeight >= CatcherGameUFOHeight:
                components().okbox('Game Over!', ['You got caught!'])
                lps().end()
                res = components().ynbox('Play Again?', [''])
                if res == 'Yes':
                    commands.parsecmd(['catchergame'])
                if res == 'No' or res == 'Exit':
                    return
                pygame.key.set_repeat(50, 50)
                return
        if CatcherGameItemWidth <= CatcherGamePlayerRangeWidth and CatcherGameItemWidth >= CatcherGamePlayerWidth:
            if CatcherGameItemHeight <= CatcherGamePlayerRangeHeight and CatcherGameItemHeight >= CatcherGamePlayerHeight:
                CatcherGameItemWidth = CatcherGamePlayerWidth
                CatcherGameItemHeight = CatcherGamePlayerHeight
                appsurface.blit(CatcherGameItem, [CatcherGamePlayerWidth, CatcherGamePlayerHeight])
        if CatcherGamePlayerWidth <= 0:
            CatcherGamePlayerWidth = CatcherGamePlayerWidth + 4
        if CatcherGamePlayerWidth >= width:
            CatcherGamePlayerWidth = CatcherGamePlayerWidth - 13
        if CatcherGamePlayerHeight <= 0:
            CatcherGamePlayerHeight = CatcherGamePlayerHeight + 4
        if CatcherGamePlayerHeight >= height:
            CatcherGamePlayerHeight = CatcherGamePlayerHeight - 10
        if CatcherGameItemWidth <= 60:
            components().okbox('You Won!', ['You evaded the Catcher!'])
            lps().end()
            res = components().ynbox('Play Again?', [''])
            if res == 'Yes':
                commands.parsecmd(['catchergame'])
            if res == 'No' or res == 'Exit':
                return
            pygame.key.set_repeat(50, 50)
            return
def toolbaradd(name = None):
    global toolbarplugstarters
    global toolbarplugs
    if name == None:
        if shell.grunning == True:
            name = components().inputbox('Plugin to Add.', ['Enter the executable path of the toolbar plugin.'])
        if shell.grunning == False:
            name = raw_input ('Plugin executable command: ')
    try:
        f = open('res/' + name + '/toolbar.txt', 'r')
        f.close()
    except:
        c.msg('Error. This app does not have a plugin.', 'ERROR')
        return
    pvar.append('TOOLBARPLUGINS', name, 'GLOBAL')
    toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
    toolbarplugs = []
    for plg in toolbarplugstarters:
        rf = c.readfile('res/' + plg + '/toolbar.txt')
        icon = rf[0]
        run = rf[1]
        toolbarplugs.append([icon, run])
class bgpd():
    def run(self):
        tasks = os.listdir('data/bgpd/tasks/')
        if len(tasks) > 0:
            for task in tasks:
                tinfo = c.readfile('data/bgpd/tasks/' + task + '/info.txt')
                jinfo = c.readfile('data/bgpd/tasks/' + task + '/' + tinfo[1] + '.txt')
                data = eval(jinfo[1])
                if data == True:
                    import shutil
                    shutil.rmtree('data/bgpd/tasks/' + task + '/')
                    try:
                        eval(jinfo[3])
                    except:
                        pass
    def newtask(self, name, job, setup = None):
        os.mkdir('data/bgpd/tasks/' + name + '/')
        dfile = c.makefile('data/bgpd/tasks/' + name + '/info.txt')
        print >> dfile, name
        print >> dfile, job[0]
        print >> dfile, setup
        print >> dfile, str(datetime.datetime.now())
        dfile.close()
        jobf = c.makefile('data/bgpd/tasks/' + name + '/' + job[0] + '.txt')
        #job is in format [name, run, setup, ondone]
        print >> jobf, job[0]
        print >> jobf, job[1]
        print >> jobf, job[2]
        print >> jobf, job[3]
        jobf.close()
        if setup != None:
            eval(setup)
        eval(job[2])
    def setup(self):
        try:
            os.mkdir('data/bgpd/tasks/')
        except:
            pass
class textedit():
    def gui(self, vom = False, f = None, title = 'Text Editor'):
        lps().new(title)
        if f == None and vom == False:
            fo = files('return', 'directory')
            f = fo + components().inputbox('Name', ['Enter the file name and extension.'])
        try:
            open(f, 'rU').close()
            b = c.readfile(f)
            b.append('')
        except:
            b = ['Delete and start typing.']
        page = 0
        typing = False
        oh = 25
        c.varman().rem('input-exitmode')
        while True:
            clock.tick(fps)
            appsurface.fill([200, 200, 200])
            pygame.draw.rect(appsurface, [50, 50, 50], [0, 0, width, 23])
            tod = textedit().displaygen(b)
            if vom == False:
                sb = components().button(2, 1, 'Save')
            nb = components().button(200, 1, 'Next Page')
            pb = components().button(280, 1, 'Previous Page')
            db = components().button(380, 1, 'Delete a Line')
            cb = []
            h = 25
            for itm in tod[page]:
                if vom == False:
                    cb.append([components().button(2, h, itm), itm])
                    h = h + 25
                    if typing == False:
                        oh = (len(cb) - 1) * 25
                if vom == True:
                    appsurface.blit(font.render(itm, 1, (50, 50, 50)), [2, h])
                    h = h + 25
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    mousepos = pygame.mouse.get_pos()
                    if vom == False:
                        if mousepos[0] >= sb[0] and mousepos[0] <= sb[1]:
                            if mousepos[1] >= sb[2] and mousepos[1] <= sb[3]:
                                if vom == False:
                                    import shutil
                                    try:
                                        shutil.copy(f, 'temp/txteditf.bak')
                                    except:
                                        pass
                                    nf = c.makefile(f)
                                    for ln in b:
                                        print >> nf, ln
                                    nf.close()
                    if mousepos[0] >= nb[0] and mousepos[0] <= nb[1]:
                        if mousepos[1] >= nb[2] and mousepos[1] <= nb[3]:
                            if page + 1 <= len(tod) - 1:
                                page = page + 1
                    if mousepos[0] >= pb[0] and mousepos[0] <= pb[1]:
                        if mousepos[1] >= pb[2] and mousepos[1] <= pb[3]:
                            if page - 1 >= 0:
                                page = page - 1
                    if mousepos[0] >= db[0] and mousepos[0] <= db[1]:
                        if mousepos[1] >= db[2] and mousepos[1] <= db[3]:
                            if vom == False:
                                l = int(components().inputbox('Line to Delete', ['Number of the line to delete.'])) - 1
                                nb = []
                                p = 0
                                for item in b:
                                    if p != l:
                                        nb.append(item)
                                    p = p + 1
                                c.copylist(nb)
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            if vom == False:
                                import shutil
                                try:
                                    shutil.copy(f, 'temp/txteditf.bak')
                                except:
                                    pass
                                nf = c.makefile(f)
                                for ln in b:
                                    print >> nf, ln
                                nf.close()
                            lps().end()
                            return
                    for btn in cb:
                        if mousepos[0] >= btn[0][0] and mousepos[0] <= width:
                            if mousepos[1] >= btn[0][2] and mousepos[1] <= btn[0][3]:
                                oh = btn[0][2]
                                txt = components().getinput(0, oh - 20, btn[1])
                                if btn[1] == '':
                                    pos = c.findinlist(cb, btn)
                                if btn[1] != '':
                                    pos = c.findinlist(b, btn[1])
                                b[pos] = txt
                                c.varman().make('typing-cpos', pos)
                                oh = oh + 25
            if c.varman().get('input-exitmode') == 'enter':
                typing = True
            if c.varman().get('input-exitmode') == 'click':
                typing = False
            if typing == True:
                pos = c.varman().get('typing-cpos')
                while typing == True:
                    clock.tick(fps)
                    appsurface.fill([200, 200, 200])
                    pygame.draw.rect(appsurface, [50, 50, 50], [0, 0, width, 23])
                    sb = components().button(2, 1, 'Save')
                    nb = components().button(200, 1, 'Next Page')
                    pb = components().button(280, 1, 'Previous Page')
                    db = components().button(380, 1, 'Delete a Line')
                    ci = 0
                    bh = 25
                    for itm in b:
                        if ci != pos + 1:
                            appsurface.blit(font.render(itm, 1, (50, 50, 50)), [2, bh])
                            bh = bh + 25
                        if ci == pos + 1:
                            appsurface.blit(font.render(itm, 1, (50, 50, 50)), [2, bh + 25])
                            bh = bh + 50
                        ci = ci + 1
                    txt = components().getinput(0, oh - 20, '')
                    oh = oh + 25
                    if pos < len(b) - 1:
                        nb = []
                        p = 0
                        for item in b:
                            nb.append(item)
                            if p == pos:
                                nb.append(txt)
                            p = p + 1
                        b = c.copylist(nb)
                        pos = pos + 1
                    elif pos == len(b) - 1:
                        b.append(txt)
                        pos = pos + 1
                    if c.varman().get('input-exitmode') == 'enter':
                        typing = True
                    if c.varman().get('input-exitmode') == 'click':
                        typing = False
    def displaygen(self, b):
        pages = [[]]
        page = 0
        h = 25
        for itm in b:
            if h + 50 <= height:
                pages[page].append(itm)
                h = h + 25
            elif h + 50 > height:
                page = page + 1
                pages.append([])
                pages[page].append(itm)
                h = 25
        return pages
class debugd():
    def setup(self):
        c.logentry('DEBUGD', 'Debugging Daemon Started')
    def run(self):
        if c.varman().get('debugmode') == False:
            daemonman().delete('debugd')
        if c.varman().get('debugmode') == True:
            pygame.draw.rect(screen, [156, 184, 232], [width - 600, 20, 600, 700])
            td = []
            out = c.varman().get('debug-out')
            if len(out) - 1 <= 42:
                pos = 0
            if len(out) - 1 > 42:
                pos = len(out) - 43
            done = 0
            while done <= 42:
                if pos <= len(out) - 1:
                    td.append(out[pos])
                pos = pos + 1
                done = done + 1
            h = 20
            for ln in td:
                screen.blit(font.render(ln, 1, [50, 50, 50]), [width - 598, h])
                h = h + 16
