variables = []

def msg(msg, mtype = 'INFO'):
    if varman().get('debugmode') == True:
        nout = varman().get('debug-out')
        nout.append('[' + mtype + ']  ' + msg)
        varman().make('debug-out', nout)
    print '[' + mtype + ']  ' + msg

def install(filepath, name = None):
    import zipfile
    import os
    import pvar
    import shutil
    curros = pvar.read('CURROS', 'KERNEL')[0]
    msg('Installing application from ' + filepath, 'INSTALL')
    #Make Backup
    shutil.copy('PythonOS.py', 'res/BACKUP-PythonOS.py')
    #Make new
    msg('Installing...', 'INSTALL')
    #open zip
    sourcezip = zipfile.ZipFile(filepath, 'r')
    sourcezip.extract('req.txt', 'temp/')
    reqfile = readfile('temp/req.txt', 'rU')
    sysver = readfile('res/system/req.txt')[5]
    if len(reqfile) - 1 >= 8:
        if float(sysver) < float(reqfile[8]):
            msg('Unsupported Version: Your Python OS system is too old to run this app.', 'INSTALL')
            new.close()
            sourcezip.close()
            return
    if name == None:
        name = reqfile[2]
    #Check install status
    msg('Installing as ' + name, 'INSTALL')
    installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    for app in installed:
        if app == name:
            msg('Already Installed - checking if update', 'INSTALL')
            uver = float(reqfile[5])
            iver = float(readfile('res/' + name + '/req.txt')[5])
            if uver > iver:
                msg('This is an update', 'INSTALL')
            if uver <= iver:
                msg('Newer or equal version of ' + name + ' installed', 'INSTALL')
                import shell
                if shell.grunning == True:
                    import PythonOS
                    a = PythonOS.components().ynbox('Reinstall or Upgrade?', ['The app you are trying to install is already installed.', 'Downgrade/Reinstall? Data will be kept.'])
                if shell.grunning == False:
                    a = raw_input ('Reinstall/Downgrade? (y/N) ')
                    if a == 'Y' or a == 'y':
                        a = 'Yes'
                if a == 'Yes':
                    uninstall(name, '-kd')
                    continue
                if a != 'Yes':
                    msg('Install Aborted!', 'INSTALL')
                    sourcezip.close()
                    return
    new = open('PythonOS.py', 'a')
    users = pvar.read('CURRENTUSER', 'GLOBAL')
    user = users[len(users) - 1]
    #Make application directories
    try:
        os.mkdir('res/' + name + '/')
    except:
        msg('Some application directories already exist!', 'INSTALL')
    try:
        os.mkdir('data/' +  name + '/')
    except:
        msg('Some application directories already exist!', 'INSTALL')
    #Get namelist of zip
    rlist = sourcezip.namelist()
    #Loop through namelist
    for r in rlist:
        member = sourcezip.open(r, 'rU')
        sourcezip.extract(r, 'res/' + name + '/')
        if r == 'req.txt':
            cmd = readfile('res/' + name + '/req.txt', 'rU')[1]
        if r == 'toolbar.txt':
            pvar.append('TOOLBARPLUGINS', name, 'GLOBAL')
    sourcezip.close()
    compat = readfile('res/' + name + '/source.py', 'rU', True)
    if compat[0] != 'Cannot read the file':    
        src = readfile('res/' + name + '/source.py', 'rU', True)
        for ln in src:
            print >> new, ln
    new.close()
    pvar.append('INSTALLEDAPPS', name, 'GLOBAL')
    rfl = len(readfile('res/' + name + '/req.txt', 'rU'))
    if rfl - 1 > 5:
        if readfile('res/' + name + '/req.txt', 'rU')[6] == 'install':
            msg('Running install script', 'INSTALL')
            shutil.copy2('res/' + name + '/install.py', 'installscript.py')
            import installscript
            deletefile('installscript.py')
    try:
        df = open('res/' + name + '/daemon.txt', 'r')
        df.close()
        pvar.append('DAEMONS', name, 'DMAN')
    except:
        pass
    msg('Finished.', 'INSTALL')
    msg('Cleaning up...', 'INSTALL')
    deletefile('res/BACKUP-PythonOS.py')
    msg('Creating command...', 'INSTALL')
    ccmd = readfile('res/' + name + '/cmd.txt', 'rU', True)
    if ccmd[0] != 'Cannot read the file':
        cmds = open('commands.py', 'a')
        for ln in ccmd:
            print >> cmds, ln
        cmds.close()
    if ccmd[0] == 'Cannot read the file':
        addcmd(name, cmd)
    msg('Resolving Dependancies...', 'INSTALL')
    try:
        open('res/' + name + '/depends.txt', 'r').close()
        deps = readfile('res/' + name + '/depends.txt')
        print name + ' depends on:'
        for d in deps:
            print d
        installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
        toinstall = []
        for dep in deps:
            found = False
            for a in installed:
                if a == dep:
                    found = True
            if found == False:
                toinstall.append(dep)
        import commands
        for app in toinstall:
            print 'Trying to download ' + app + ' via PMan. If this fails, download it manually and install it.'
            commands.parsecmd(['pman', '-ri', app])
    except:
        pass
def uninstall(name, opts):
    import pvar
    import os
    import shell
    import shutil
    msg('Uninstalling ' + name, 'UNINSTALL')
    rreq = readfile('res/' + name + '/req.txt', 'rU')
    if len(rreq) - 1 > 6:
        if rreq[7] == 'uninstall':
            msg('Running uninstall script', 'UNINSTALL')
            shutil.copy2('res/' + name + '/uninstall.py', 'uninstallscript.py')
            import uninstallscript
            deletefile('uninstallscript.py')
    installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    pvar.destroy('INSTALLEDAPPS', 'GLOBAL')
    npvf = open('var/GLOBAL-INSTALLEDAPPS.var', 'w')
    print 'Re-Indexing applications'
    for app in installed:
        if app != name:
            print >> npvf, app
    isb = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
    pvar.destroy('SIDEMENUITEMS', 'SIDEMENU')
    npvfs = open('var/SIDEMENU-SIDEMENUITEMS.var', 'w')
    for sbapp in isb:
        if sbapp != name:
            print >> npvfs, sbapp
    npvfs.close()
    try:
        f = open('res/' + name + '/toolbar.txt', 'rU')
        f.close()
        itba = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
        ntba = makefile('var/GLOBAL-TOOLBARPLUGINS.var')
        for app in itba:
            if app != name:
                print >> ntba, app
        ntba.close()
    except:
        pass
    try:
        f = open('res/' + name + '/daemon.txt', 'rU')
        f.close()
        ida = pvar.read('DAEMONS', 'DMAN')
        nda = makefile('var/DMAN-DAEMONS.var')
        for app in ida:
            if app != name:
                print >> nda, app
        ntba.close()
    except:
        pass
    print 'Rebuilding commands'
    basecmds = readfile('res/system/basecommands.py', 'rU')
    deletefile('commands.py')
    newcmds = makefile('commands.py')
    for bcln in basecmds:
        print >> newcmds, bcln
    newcmds.close()
    for rtg in installed:
        if rtg != name:
            rtxt = readfile('res/' + rtg + '/req.txt', 'rU')
            ccmd = readfile('res/' + rtg + '/cmd.txt', 'rU', True)
            if ccmd[0] != 'Cannot read the file':
                cmds = open('commands.py', 'a')
                for ln in ccmd:
                    print >> cmds, ln
                cmds.close()
            if ccmd[0] == 'Cannot read the file':
                addcmd(rtg, rtxt[1])
    print 'Rebuilding PythonOS'
    deletefile('PythonOS.py')
    newpyos = makefile('PythonOS.py')
    for aps in installed:
        if aps != name:
            src = readfile('res/' + aps + '/source.py', 'rU', True)
            if src[0] != 'Cannot read the file':
                for ln in src:
                    print >> newpyos, ln
    newpyos.close()
    if opts != '-kd':
        print 'Deleting directories'
        try:
            shutil.rmtree('data/' + name + '/')
        except:
            os.rmdir('data/' + name + '/')
    try:
        shutil.rmtree('res/' + name + '/')
    except:
        os.rmdir('res/' + name + '/')
    print 'Done.'
        
def readfile(filepath, mode = 'rU', silent = False):
    try:
        f = open(filepath, mode)
    except:
        if silent == False:
            msg('Cannot open file ' + filepath + ' in mode ' + mode, 'FILEMAN')
        return ['Cannot read the file']
    lines0 = f.readlines()
    lines = []
    for line in lines0:
        cchar = 0
        fline = ''
        while cchar <= len(line) - 2:
            char = line[cchar]
            fline = fline + char
            cchar = cchar + 1
        lines.append(fline)
    f.close()
    return lines

def makefile(filepath):
    try:
        f = open(filepath, 'w')
    except:
        msg('Cannot open ' + filepath + ' in mode w', 'FILEMAN')
        return None
    return f

def deletefile(filepath):
    import os
    try:
        os.remove(filepath)
        return True
    except:
        msg('Cannot delete ' + filepath, 'FILEMAN')
        return None

def addcmd(name, command):
    commandfile = readfile('commands.py', 'rU')
    commandfile.append('    if scmd[0] == "' + name + '":')
    commandfile.append('        ' + command)
    ncf = makefile('commands.py')
    for line in commandfile:
        print >> ncf, line
    ncf.close()

def compiledir(cdir):
    msg('Compiling all .py files in ' + cdir, 'COMPILE')
    import compileall
    compileall.compile_dir(cdir)

def index():
    import os
    import pvar
    indf = makefile('data/system/index.txt')
    toindex = pvar.read('TOINDEX', 'INDEX')
    index = ['PythonOS.py', 'kernel.py', 'commands.py', 'constants.py', 'shell.py', 'pvar.py']
    for item in toindex:
        if fileordir(item) == 'Directory':
            contents = os.listdir(item)
            for candidate in contents:
                if fileordir(item + candidate) == 'File':
                    index.append(item + candidate)
                if fileordir(item + candidate + '/') == 'Directory':
                    index.append(item + candidate + '/')
                    toindex.append(item + candidate + '/')
        elif fileordir(item) == 'File':
            index.append(item)
    for obj in index:
        print >> indf, obj
    indf.close()

def addtoindexusr():
    import shell
    import pvar
    import PythonOS
    if shell.grunning == False:
        path = raw_input('Add Directory to Index: ')
    if shell.grunning == True:
        path = PythonOS.components().inputbox('Add Directory to Index', ['Enter the directory to add:'])
    pvar.append('TOINDEX', path, 'INDEX')
    uaif = open('data/system/user-added-index.txt', 'a')
    print >> uaif, path
    uaif.close()
    return

def removefromindexusr():
    import shell
    import pvar
    import PythonOS
    if shell.grunning == False:
        path = raw_input('Remove Directory to Index: ')
    if shell.grunning == True:
        path = PythonOS.components().inputbox('Remove Directory to Index', ['Enter the directory to remove:'])
    ind = pvar.read('TOINDEX', 'INDEX')
    pvar.make('TOINDEX', 'res/', 'INDEX')
    for i in ind:
        if i != path and i != 'res/':
            pvar.append('TOINDEX', i, 'INDEX')
    uaif = readfile('data/system/user-added-index.txt')
    nuaif = makefile('data/system/user-added-index.txt')
    for ln in uaif:
        if ln != path:
            print >> nuaif, ln
    nuaif.close()
    return

def fileordir(obj):
    import os
    try:
        tf = open(obj, 'r')
        tf.close()
        return 'File'
    except:
        try:
            os.listdir(obj)
            return 'Directory'
        except:
            return None

def appreqg(boolean):
    import PythonOS
    import shell
    if boolean == 'True':
        msg('Starting Pygame Graphics...', 'GSERVER')
        PythonOS.gserver()
        shell.grunning = True
        return
    if boolean == 'False':
        msg('The application does not rely on graphics.', 'GSERVER')
        return
    else:
        msg('This is not a valid argument: use appreqg True or appreqg False.', 'GSERVER')
        return

def fst():
    import shell
    import PythonOS
    if shell.grunning == True:
        PythonOS.pygame.display.toggle_fullscreen()

def changescreen(w, h):
    import shell
    import PythonOS
    if shell.grunning == True:
        PythonOS.screen = PythonOS.pygame.display.set_mode([int(w), int(h)], pygame.RESIZABLE)

def textfman(path):
    import os
    cmd = ''
    atp = ''
    msg('Test-based File Manager', 'TXTFMAN')
    while atp != 'exit':
        try:
            items = os.listdir(path)
            for item in items:
                if fileordir(path + item + '/') == 'Directory':
                    item = item + '/'
            msg(path, 'PATH')
            for item in items:
                if fileordir(path + item) == 'Directory':
                    msg(item, 'DIR')
                if fileordir(path + item) == 'File':
                    msg(item, 'OBJ')
            atp = raw_input ('Open: ')
            try:
                if atp.endswith('/') == False:
                    raise IOError
                path = path + atp
            except:
                try:
                    os.listdir(path + atp + '/')
                    path = path + atp + '/'
                except:
                    try:
                        if fileordir(path + atp) == 'File':
                            print path + atp
                            openfile(path + atp)
                    except:
                            path = atp
        except:
            msg('Error', 'TXTFMAN')
            atp = 'exit'

def openfile(path):
    import shell
    import PythonOS
    textfiles = ['.txt', '.rtf', '.pos']
    audiofiles = ['.wav', '.mp3', '.ogg']
    picfiles = ['.bmp', '.jpg', '.jpeg', '.JPG', '.png']
    treatliketext = ['.odf', '.doc', '.docx', '.htm', '.html', '.var', '.p']
    archives = ['.zip']
    try:
        f = open(path, 'r')
        f.close()
        for item in textfiles:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    txtedit(path)
                    return
                if shell.grunning == True:
                    PythonOS.textedit().gui(False, path)
                    return
        for item in audiofiles:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    if item == '.wav':
                        import wave
                        sound = wave.open(path)
                        sound.close()
                    return
                if shell.grunning == True:
                    PythonOS.music('play', path)
                    return
        for item in picfiles:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    msg('Opening a ' + item + ' file requires graphics', 'OPENFILE')
                    return
                if shell.grunning == True:
                    PythonOS.pictures(path)
                    return
        for item in treatliketext:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    txtedit(path)
                    return
                if shell.grunning == True:
                    PythonOS.textedit().gui(False, path)
                    return
        for item in archives:
            if path.endswith(item) == True:
                PythonOS.gunzip(path)
        msg('This file cannot be opened.\nIf you have an app that supports this file type, try opening it from the app.', 'OPENFILE')
    except:
        msg('Error opening.', 'OPENFILE')
        return

def txtedit(path):
    msg('File ' + path, 'TXTEDIT')
    print ''
    try:
        tf = open(path, 'r')
        tf.close()
        for ln in readfile(path, 'rU'):
            print ln
        f = open(path, 'a')
    except:
        f = makefile(path)
    atf = raw_input ()
    while atf != 'exit':
        print >> f, atf
        atf = raw_input ()
    f.close()
    return

def execpy(path):
    import runpy
    msg('Executing .py file ' + path, 'RUNPY')
    runpy.run_path(path)

def execmod(name):
    import runpy
    msg('Executing Python Module ' + name, 'RUNPY')
    runpy.run_module(name)

def pyoscredits():
    for ln in readfile('res/system/credits.txt', 'rU'):
        print ln

def misc():
    pycfiles = ['commands.pyc', 'constants.pyc', 'PythonOS.pyc', 'shell.pyc', 'pvar.pyc']
    directories = ['var/', 'res/', 'user/', 'temp/', 'data/']

def shiftchar(text, sft=1):
    import string
    return string.joinfields(map(eval("lambda ch:chr((ord(ch)-ord('a')+%d)%%26+ord('a'))"%(sft,)),text),'')

def logentry(app, inf):
    import datetime
    lf = open('data/system/syslog.txt', 'a')
    print >> lf, str(datetime.datetime.now()) + ':  ' + app + ': ' + inf
    lf.close()
    if varman().get('debugmode') == True:
        nout = varman().get('debug-out')
        nout.append(':  ' + app + ': ' + inf)
        varman().make('debug-out', nout)
class securetools():
    def scanall(self):
        msg('Scanning entire index: This may take a while...', 'SCANNER')
        indf = readfile('data/system/index.txt')
        rfound = []
        for obj in indf:
            if fileordir(obj) == 'File':
                if obj.endswith('.txt') == True or obj.endswith('.py') == True or obj.endswith('.var') == True:
                    fio = securetools().scanner(obj, True)
                    rfound.append([obj, fio])
        msg('Done.', 'SCANNER')
        for f in rfound:
            if len(f[1]) == 0:
                continue
            print 'File Scanned: ' + f[0]
            print 'Warnings Found: ' + str(len(f[1]))
            print ''
    def scanner_update(self):
        import pvar
        tss = int(pvar.read('TIMES_FROM_UPDATE', 'SCANNER')[0])
        if tss == 3:
            msg('Updating library...', 'SCANNER')
            import commands
            commands.parsecmd('wget http://adamfurman.privatepage.sk/PythonOS/Files/scanner-lib.txt res/system/scanner-lib.txt'.split())
            msg('Done.', 'SCANNER')
            tss = -1
        pvar.make('TIMES_FROM_UPDATE', str(tss + 1), 'SCANNER')
    def scanner(self, f = None, silent = False):
        if silent == False:
            securetools().scanner_update()
        import shell
        logentry('SCANNER', 'file-scanner started.')
        if f == None:
            if shell.grunning == True:
                PythonOS.components().okbox('File to Scan', ['Select the file to scan.'])
                f = PythonOS.files('return')
            elif shell.grunning == False:
                f = shell.cdir + raw_input ('Enter the file to scan: ')
        lib = readfile('res/system/scanner-lib.txt')
        cpos = 0
        found = []
        read = readfile(f)
        if silent == False:
            print 'File Information:'
            print 'Number of Lines: ' + str(len(read))
            print 'Path: ' + f
        for ln in read:
            cpos = 0
            while cpos <= len(lib) - 1:
                ll = ln.find(lib[cpos])
                if ll != -1:
                    fif = False
                    for obj in found:
                        if obj[0] == lib[cpos]:
                            i = obj[2] + 1
                            obj[2] = i
                            fif = True
                    if fif == False:
                        found.append([lib[cpos], lib[cpos + 1], 1])
                cpos = cpos + 2
        if silent == False:
            for item in found:
                print 'Found Warning: ' + item[0]
                print 'What this means: ' + item[1]
                print 'Times found: ' + str(item[2])
                print ''
        elif silent == True:
            return found
    def sandbox(self, f = None, g = False):
        import shell
        import PythonOS
        import os
        import shutil
        if f == None:
            if shell.grunning == True:
                if PythonOS.full == True:
                    PythonOS.components().okbox('Go to Windowed mode.', ['It is reccommended you switch to windowed mode.', 'Do this to see command-line output.'])
                poa = PythonOS.components().ynbox('Python OS App?', ['Do you wish to sandbox a Python OS app?'])
                if poa != 'Yes':
                    PythonOS.components().okbox('File to run Sandboxed.', ['Select the file to run sandboxed.'])
                    f = PythonOS.files('return')
                yn = PythonOS.components().ynbox('Graphics Required?', ['Does the selected file need Pygame to run?'])
                if poa == 'Yes':
                    name = PythonOS.components().inputbox('App Name', ['Type the command of the app (e.g. "files", "music").'])
                if yn == 'Yes':
                    g = True
                    f = 'res/' + name + '/source.py'
            if shell.grunning == False:
                poa = raw_input ('Sandbox a Python OS app? (Y/n) ')
                if poa == 'N' or poa == 'n':
                    f = shell.cdir + raw_input ('File to run Sandboxed: ')
                yn = raw_input ('Need Pygame (Y/N) ')
                if yn == 'Y' or yn == 'y':
                    g = True
                if poa == 'Y' or poa == 'y':
                    name = raw_input ("Enter the app's command: ")
                    f = 'res/' + name + '/source.py'
            try:
                tf = open('data/system/sandbox-info.txt', 'r')
                tf.close()
                sbinf = readfile('data/system/sandbox-info.txt')
                loc = sbinf[0]
                pyloc = sbinf[1]
            except:
                nf = makefile('data/system/sandbox-info.txt')
                if shell.grunning == True:
                    loc = PythonOS.components().inputbox('Python OS location.', ['Type the folder where Python OS is installed.'])
                    pyloc = PythonOS.components().inputbox('Path to Python', ['Type the path to Python (e.g. C:/Python27/python.exe)', 'Or, type the command to launch Python.'])
                elif shell.grunning == False:
                    loc = raw_input ('Where is your Python OS installed?: ')
                    pyloc = raw_input ("Enter the path to python.exe or Python's command: ")
                print >> nf, loc
                print >> nf, pyloc
        print 'Scanning with file-scanner.'
        securetools().scanner(f)
        sbp = 'user/' + shell.user + '/'
        try:
            os.rmdir(sbp + 'sandbox/')
        except:
            pass
        os.mkdir(sbp + 'sandbox/')
        sbp = sbp + 'sandbox/'
        shutil.copytree('res/system/sandbox/data/', sbp + 'data/')
        shutil.copytree('res/system/sandbox/res/', sbp + 'res/')
        shutil.copytree('res/system/sandbox/temp/', sbp + 'temp/')
        shutil.copytree('res/system/sandbox/user/', sbp + 'user/')
        shutil.copy('res/system/sandbox/kernel.py', sbp + 'kernel.py')
        shutil.copy('res/system/sandbox/constants.py', sbp + 'constants.py')
        shutil.copy('res/system/sandbox/commands.py', sbp + 'commands.py')
        shutil.copy('res/system/sandbox/shell.py', sbp + 'shell.py')
        shutil.copy('res/system/sandbox/pvar.py', sbp + 'pvar.py')
        shutil.copy('res/system/sandbox/PythonOS.py', sbp + 'PythonOS.py')
        shutil.copy('res/system/sandbox/boot.kstr', sbp + 'boot.kstr')
        shutil.copytree('res/system/sandbox/var/', sbp + 'var/')
        if poa == 'n' or poa == 'N' or poa == 'No' or poa == 'Cancel':
            shutil.copy(f, sbp + 'file.py')
            f = sbp + 'file.py'
            print 'Please move any requirements of the app to ' + loc + ' and press Enter/Return.'
            raw_input ()
        if poa == 'y' or poa == 'Y' or poa == 'Yes':
            shutil.copytree('res/' + name + '/', sbp + 'res/' + name + '/')
            shutil.copytree('data/' + name + '/', sbp + 'data/' + name + '/')
            f = sbp + 'kernel.py'
            posf = open(sbp + 'PythonOS.py', 'a+')
            src = readfile('res/' + name + '/source.py')
            for ln in src:
                print >> posf, ln
            posf.close()
            ral = open(sbp + 'var/GLOBAL-INSTALLEDAPPS.var', 'a+')
            print >> ral, name
            ral.close()
            if g == True:
                var = makefile(sbp + 'var/SETTINGS-RUNG.var')
                print >> var, 'yes'
            if g == False:
                var = makefile(sbp + 'var/SETTINGS-RUNG.var')
                print >> var, 'no'
            var.close()
            cmd = readfile('res/' + name + '/req.txt')[1]
            cmdf = open(sbp + 'commands.py', 'a+')
            if g == False:
                print >> cmdf, '    if scmd[0] == "' + name + '":'
                print >> cmdf, '        ' + cmd
            if g == True:
                print >> cmdf, '    if scmd[0] == "' + name + '":'
                print >> cmdf, '        parsecmd(["appreqg", "True"])'
                print >> cmdf, '        ' + cmd
                print >> cmdf, '        parsecmd(["killg"])'
            cmdf.close()
        shutil.copy('res/system/sandbox/var/GLOBAL-USERS.var', sbp + 'var/GLOBAL-USERS.var')
        logentry('SANDBOX', 'Running ' + f + ' in ' + sbp)
        import sys
        lfs = f.rfind('/')
        if lfs != -1:
            nf = ''
            cpos = lfs + 1
            while cpos != len(f):
                nf = nf + f[cpos]
                cpos = cpos + 1
            f = nf
        p = sys.platform
        if p == 'win32':
            bf = makefile('runsand.bat')
            print >> bf, 'cd ' + loc + 'user/' + shell.user + '/sandbox/'
            print >> bf, pyloc + ' ' + f
            bf.close()
            os.system(loc + 'runsand.bat')
        if p.startswith('linux') == True or p == 'darwin':
            bf = makefile('runsand.sh')
            print >> bf, 'cd ' + loc + 'user/' + shell.user + '/sandbox/'
            print >> bf, pyloc + ' ' + f
            bf.close()
            os.system(loc + 'runsand.sh')
        logentry('SANDBOX', 'Running')
        shutil.rmtree(sbp)
        if p == 'win32':
            deletefile('runsand.bat')
        if p.startswith('linux') == True or p == 'darwin':
            deletefile('runsand.sh')
def makepoint():
    msg('Creating Restore Point.', 'SYSTEM')
    import zipfile
    import datetime
    import shell
    import pvar
    import os
    lnum = int(pvar.read('LASTBACKUP', 'SYSTEM')[0]) + 1
    logentry('AUTO-BACKUP', 'Creating automatic backup.')
    point = zipfile.ZipFile('data/system/restores/' + str(lnum) + '.zip', 'w')
    index()
    indf = readfile('data/system/index.txt')
    for o in indf:
        if o.startswith('data/system/restores/') == False:
            try:
                fiuaif = False
                uaif = readfile('data/system/user-added-index.txt')
                for ln in uaif:
                    if o.startswith(ln):
                        fiuaif = True
                if fiuaif == False:
                    point.write(o)
            except:
                print 'Error'
    info = makefile('temp/backupinfo.txt')
    print >> info, 'Restore Point Info.'
    print >> info, 'Restore Point #' + str(lnum)
    print >> info, 'At: ' + str(datetime.datetime.now())
    print >> info, 'By: ' + shell.user
    info.close()
    point.write('temp/backupinfo.txt')
    point.write('data/system/restores/')
    deletefile('temp/backupinfo.txt')
    point.close()
    pvar.make('LASTBACKUP', str(lnum), 'SYSTEM')
    for p in os.listdir('data/system/restores/'):
        if int(p.split('.')[0]) < lnum - 3:
            deletefile('data/system/restores/' + p)
    msg('Done', 'SYSTEM')
def restore(place = None):
    import shell
    import pvar
    if shell.grunning == True:
        import PythonOS
        resp = PythonOS.components().ynbox('Restore?', ['Really restore?', 'This will not overwrite new files.', 'This WILL overwrite existing files with older ones.'])
        if resp == 'No' or resp == 'Cancel':
            return
    elif shell.grunning == False:
        resp = raw_input ('Really restore? (Y/N) ')
        if resp == 'N'or resp == 'n':
            return
    msg('Restoring from restore point.', 'SYSTEM')
    if place == None:
        lnum = int(pvar.read('LASTBACKUP', 'SYSTEM')[0])
    if place != None:
        lnum = place
    import zipfile
    point = zipfile.ZipFile('data/system/restores/' + str(lnum) + '.zip', 'r')
    point.extractall()
    point.close()
    msg('Done.', 'SYSTEM')
def checkapp(name):
    import pvar
    installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    found = False
    for app in installed:
        if app == name:
            found = True
    return found
def compatibility_bridge(name, f = 'main.py'):
    logentry('COMPAT-BRIDGE', 'Running ' + name + ' in compatibility mode.')
    import os
    import sys
    if sys.platform == 'win32':
        ef = makefile('run_compat.bat')
    if sys.platform != 'win32':
        ef = makefile('run_compat.sh')
    print >> ef, 'cd res/' + name + '/'
    print >> ef, f
    ef.close()
    if sys.platform == 'win32':
        os.system('run_compat.bat')
    if sys.platform != 'win32':
        os.system('sh run_compat.sh')
    if sys.platform == 'win32':
        deletefile('run_compat.bat')
    if sys.platform != 'win32':
        deletefile('run_compat.sh')
        
class varman():
    def make(self, name, info):
        global variables
        if varman().get(name) != None:
            varman().rem(name)
        variables.append([name, info])
    def get(self, name):
        global variables
        for v in variables:
            if v[0] == name:
                return v[1]
        return None
    def rem(self, name):
        global variables
        nvars = []
        for var in variables:
            if var[0] != name:
                nvars.append(var)
        variables = []
        for v in nvars:
            variables.append(v)
    def dumpvars(self):
        logentry('VARIABLES', str(variables))

class copyfile():
    def copy(self, name, dest):
        global variables
        import PythonOS
        if varman().get('copy-clp') == None:
            copyfile().setup(name, dest)
        clp = varman().get('copy-clp')
        f = varman().get('copy-dest')[1]
        done = 0
        todo = varman().get('buffersize')
        while done <= todo and varman().get('copy-done') == False:
            f.write(varman().get('srccache')[clp])
            if clp >= len(varman().get('srccache')) - 1:
                f.close()
                varman().rem('srccache')
                varman().rem('copy-dest')
                varman().rem('copy-clp')
                varman().rem('copy-done')
                return True
            clp = clp + 1
            done = done + 1
        varman().rem('copy-clp')
        varman().make('copy-clp', clp)
    def setup(self, name, dest):
        import pvar
        s = open(name, 'rb')
        stuff = []
        for line in s:
            stuff.append(line)
        s.close()
        varman().make('copy-src', name)
        varman().make('srccache', stuff)
        varman().make('copy-dest', [dest, open(dest, 'wb')])
        varman().make('copy-clp', 0)
        varman().make('copy-done', False)
        varman().make('buffersize', int(pvar.read('BUFFERSIZE', 'COPY')[0]))
def pparse(path):
    contents = readfile(path)
    import commands
    import shell
    for ln in contents:
        line = ln.split(':')
        if len(line) == 0:
            commands.parsecmd(line[0].split())
            continue
        if line[0] == 'exec':
            exec(line[1])
        if line[0] == 'print':
            print line[1]
        if line[0] == 'notify':
            if shell.grunning == True:
                import PythonOS
                PythonOS.notifyd().notify(line[1])
            if shell.grunning == False:
                msg(line[1], 'INFO')
        if line[0] == 'mkfile':
            makefile(line[1]).close()
        if line[0] == 'writetofile':
            f = open(line[1], line[2])
            f.write(line[3])
            f.close()
def reload_all():
    import os
    import pvar
    installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    pvar.destroy('INSTALLEDAPPS', 'GLOBAL')
    npvf = open('var/GLOBAL-INSTALLEDAPPS.var', 'w')
    print 'Re-Indexing applications'
    for app in installed:
        print >> npvf, app
    isb = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
    pvar.destroy('SIDEMENUITEMS', 'SIDEMENU')
    npvfs = open('var/SIDEMENU-SIDEMENUITEMS.var', 'w')
    for sbapp in isb:
        print >> npvfs, sbapp
    npvfs.close()
    try:
        itba = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
        ntba = makefile('var/GLOBAL-TOOLBARPLUGINS.var')
        for app in itba:
            print >> ntba, app
        ntba.close()
    except:
        pass
    try:
        ida = pvar.read('DAEMONS', 'DMAN')
        nda = makefile('var/DMAN-DAEMONS.var')
        for app in ida:
            print >> nda, app
        ntba.close()
    except:
        pass
    basecmds = readfile('res/system/basecommands.py', 'rU')
    deletefile('commands.py')
    newcmds = makefile('commands.py')
    for bcln in basecmds:
        print >> newcmds, bcln
    newcmds.close()
    for rtg in installed:
        rtxt = readfile('res/' + rtg + '/req.txt', 'rU')
        ccmd = readfile('res/' + rtg + '/cmd.txt', 'rU', True)
        if ccmd[0] != 'Cannot read the file':
            cmds = open('commands.py', 'a')
            for ln in ccmd:
                print >> cmds, ln
            cmds.close()
        if ccmd[0] == 'Cannot read the file':
            addcmd(rtg, rtxt[1])
    print 'Rebuilding PythonOS'
    deletefile('PythonOS.py')
    newpyos = makefile('PythonOS.py')
    for aps in installed:
        src = readfile('res/' + aps + '/source.py', 'rU', True)
        if src[0] != 'Cannot read the file':
            for ln in src:
                print >> newpyos, ln
    newpyos.close()
    import commands
    commands.parsecmd(['reload-pyos'])
    reload(commands)
def findinlist(l, term):
    cpos = 0
    for itm in l:
        if l[cpos] == term:
            return cpos
        cpos = cpos + 1
    return -1
def rfindinlist(l, term):
    cpos = len(l) - 1
    while cpos >= 0:
        if l[cpos] == term:
            logentry('LIST', 'Found ' + term + ' at position ' + str(cpos))
            return cpos
        cpos = cpos - 1
    return -1
def copylist(l):
    nl = []
    for itm in l:
        nl.append(itm)
    return nl
def toggle_full():
    import PythonOS
    flags = PythonOS.screen.get_flags()
    if flags == PythonOS.pygame.FULLSCREEN:
        PythonOS.screen = PythonOS.pygame.display.set_mode([0, 0], PythonOS.pygame.RESIZABLE)
        return False
    if flags != PythonOS.pygame.FULLSCREEN:
        PythonOS.screen = PythonOS.pygame.display.set_mode([0, 0], PythonOS.pygame.FULLSCREEN | PythonOS.pygame.HWSURFACE | PythonOS.pygame.RESIZABLE)
        return True
