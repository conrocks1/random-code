def msg(msg, mtype):
    print '[' + mtype + ']  ' + msg

def install(filepath, name = None):
    import zipfile
    import os
    import pvar
    import shutil
    curros = pvar.read('CURROS', 'KERNEL')[0]
    msg('Installing application from ' + filepath, 'INSTALL')
    #Make Backup
    shutil.copy2('PythonOS.py', 'res/BACKUP-PythonOS.py')
    deletefile('PythonOS.py')
    #Make new
    msg('Installing...', 'INSTALL')
    #Print backup into new
    shutil.copyfile('res/BACKUP-PythonOS.py', 'PythonOS.py')
    new = open('PythonOS.py', 'a')
    #open zip
    sourcezip = zipfile.ZipFile(filepath, 'r')
    sourcezip.extract('req.txt', 'temp/')
    reqfile = readfile('temp/req.txt', 'rU')
    sysver = readfile('res/system/req.txt')[5]
    if len(reqfile) - 1 >= 8:
        if float(sysver) < float(reqfile[5]):
            msg('Unsupported Version: Your Python OS system is too old to run this app.', 'INSTALL')
            new.close()
            sourcezip.close()
            return
    if name == None:
        name = reqfile[2]
    users = pvar.read('CURRENTUSER', 'GLOBAL')
    user = users[len(users) - 1]
    #Make application directories
    try:
        os.mkdir('res/' + name + '/')
    except:
        msg('Some application directories already exist!', 'INSTALL')
    try:
        os.mkdir('data/' +  name + '/')
    except:
        msg('Some application directories already exist!', 'INSTALL')
    #Get namelist of zip
    rlist = sourcezip.namelist()
    #Loop through namelist
    for r in rlist:
        member = sourcezip.open(r, 'rU')
        sourcezip.extract(r, 'res/' + name + '/')
        if r == 'req.txt':
            cmd = readfile('res/' + name + '/req.txt', 'rU')[1]
        if r == 'toolbar.txt':
            pvar.append('TOOLBARPLUGINS', name, 'GLOBAL')
    sourcezip.close()
    src = readfile('res/' + name + '/source.py', 'rU')
    for ln in src:
        print >> new, ln
    new.close()
    pvar.append('INSTALLEDAPPS', name, 'GLOBAL')
    rfl = len(readfile('res/' + name + '/req.txt', 'rU'))
    if rfl - 1 > 5:
        if readfile('res/' + name + '/req.txt', 'rU')[6] == 'install':
            msg('Running install script', 'INSTALL')
            shutil.copy2('res/' + name + '/install.py', 'installscript.py')
            import installscript
            deletefile('installscript.py')
    msg('Finished.', 'INSTALL')
    msg('Cleaning up...', 'INSTALL')
    deletefile('res/BACKUP-PythonOS.py')
    msg('Creating command...', 'INSTALL')
    addcmd(name, cmd)
def uninstall(name, opts):
    import pvar
    import os
    import shell
    import shutil
    msg('Uninstalling ' + name, 'UNINSTALL')
    rreq = readfile('res/' + name + '/req.txt', 'rU')
    if len(rreq) - 1 > 6:
        if rreq[7] == 'uninstall':
            msg('Running uninstall script', 'UNINSTALL')
            shutil.copy2('res/' + name + '/uninstall.py', 'uninstallscript.py')
            import uninstallscript
            deletefile('uninstallscript.py')
    installed = pvar.read('INSTALLEDAPPS', 'GLOBAL')
    pvar.destroy('INSTALLEDAPPS', 'GLOBAL')
    npvf = open('var/GLOBAL-INSTALLEDAPPS.var', 'w')
    print 'Re-Indexing applications'
    for app in installed:
        if app != name:
            print >> npvf, app
    try:
        f = open('res/' + name + '/toolbar.txt', 'rU')
        f.close()
        itba = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
        ntba = makefile('var/GLOBAL-TOOLBARPLUGINS.var')
        for app in itba:
            if app != name:
                print >> ntba, app
        ntba.close()
    except:
        msg('Skipping Toolbar Config', 'UNINSTALL')
    print 'Rebuilding commands'
    basecmds = readfile('res/system/basecommands.py', 'rU')
    deletefile('commands.py')
    newcmds = makefile('commands.py')
    for bcln in basecmds:
        print >> newcmds, bcln
    newcmds.close()
    for rtg in installed:
        if rtg != name:
            rtxt = readfile('res/' + rtg + '/req.txt', 'rU')
            addcmd(rtg, rtxt[1])
    print 'Rebuilding PythonOS'
    deletefile('PythonOS.py')
    newpyos = makefile('PythonOS.py')
    for aps in installed:
        if aps != name:
            src = readfile('res/' + aps + '/source.py', 'rU')
            for ln in src:
                print >> newpyos, ln
    newpyos.close()
    if opts != '-kd':
        print 'Deleting directories'
        try:
            shutil.rmtree('data/' + name + '/')
        except:
            os.rmdir('data/' + name + '/')
    try:
        shutil.rmtree('res/' + name + '/')
    except:
        os.rmdir('res/' + name + '/')
    print 'Done.'
        
def readfile(filepath, mode = 'rU'):
    try:
        f = open(filepath, mode)
    except:
        msg('Cannot open file ' + filepath + ' in mode ' + mode, 'FILEMAN')
        return ['Cannot read the file']
    lines0 = f.readlines()
    lines = []
    for line in lines0:
        cchar = 0
        fline = ''
        while cchar <= len(line) - 2:
            char = line[cchar]
            fline = fline + char
            cchar = cchar + 1
        lines.append(fline)
    f.close()
    return lines

def makefile(filepath):
    try:
        f = open(filepath, 'w')
    except:
        msg('Cannot open ' + filepath + ' in mode w', 'FILEMAN')
        return None
    return f

def deletefile(filepath):
    import os
    try:
        os.remove(filepath)
        return True
    except:
        msg('Cannot delete ' + filepath, 'FILEMAN')
        return None

def addcmd(name, command):
    commandfile = readfile('commands.py', 'rU')
    commandfile.append('    if scmd[0] == "' + name + '":')
    commandfile.append('        ' + command)
    ncf = makefile('commands.py')
    for line in commandfile:
        print >> ncf, line
    ncf.close()

def compiledir(cdir):
    msg('Compiling all .py files in ' + cdir, 'COMPILE')
    import compileall
    compileall.compile_dir(cdir)

def index():
    import os
    import pvar
    indf = makefile('data/system/index.txt')
    toindex = pvar.read('TOINDEX', 'INDEX')
    index = ['PythonOS.py', 'kernel.py', 'commands.py', 'constants.py', 'shell.py']
    for item in toindex:
        if fileordir(item) == 'Directory':
            contents = os.listdir(item)
            for candidate in contents:
                if fileordir(item + candidate) == 'File':
                    index.append(item + candidate)
                if fileordir(item + candidate + '/') == 'Directory':
                    index.append(item + candidate + '/')
                    toindex.append(item + candidate + '/')
        elif fileordir(item) == 'File':
            index.append(item)
    for obj in index:
        print >> indf, obj
    indf.close()

def addtoindexusr():
    import shell
    import pvar
    import PythonOS
    if shell.grunning == False:
        path = raw_input('Add Directory to Index: ')
    if shell.grunning == True:
        path = PythonOS.components().inputbox('Add Directory to Index', ['Enter the directory to add:'])
    pvar.append('TOINDEX', path, 'INDEX')
    return

def removefromindexusr():
    import shell
    import pvar
    import PythonOS
    if shell.grunning == False:
        path = raw_input('Remove Directory to Index: ')
    if shell.grunning == True:
        path = PythonOS.components().inputbox('Remove Directory to Index', ['Enter the directory to remove:'])
    ind = pvar.read('TOINDEX', 'INDEX')
    pvar.make('TOINDEX', 'res/', 'INDEX')
    for i in ind:
        if i != path and i != 'res/':
            pvar.append('TOINDEX', i, 'INDEX')
    return

def fileordir(obj):
    import os
    try:
        tf = open(obj, 'r')
        tf.close()
        return 'File'
    except:
        try:
            os.listdir(obj)
            return 'Directory'
        except:
            return None

def appreqg(boolean):
    import PythonOS
    import shell
    if boolean == 'True':
        msg('Starting Pygame Graphics...', 'GSERVER')
        PythonOS.gserver()
        shell.grunning = True
        return
    if boolean == 'False':
        msg('The application does not rely on graphics.', 'GSERVER')
        return
    else:
        msg('This is not a valid argument: use appreqg True or appreqg False.', 'GSERVER')
        return

def fst():
    import shell
    import PythonOS
    if shell.grunning == True:
        PythonOS.pygame.display.toggle_fullscreen()

def changescreen(w, h):
    import shell
    import PythonOS
    if shell.grunning == True:
        PythonOS.screen = PythonOS.pygame.display.set_mode([int(w), int(h)], pygame.RESIZABLE)

def textfman(path):
    import os
    cmd = ''
    atp = ''
    msg('Test-based File Manager', 'TXTFMAN')
    while atp != 'exit':
        try:
            items = os.listdir(path)
            for item in items:
                if fileordir(path + item + '/') == 'Directory':
                    item = item + '/'
            msg(path, 'PATH')
            for item in items:
                if fileordir(path + item) == 'Directory':
                    msg(item, 'DIR')
                if fileordir(path + item) == 'File':
                    msg(item, 'OBJ')
            atp = raw_input ('Open: ')
            try:
                if atp.endswith('/') == False:
                    raise IOError
                path = path + atp
            except:
                try:
                    os.listdir(path + atp + '/')
                    path = path + atp + '/'
                except:
                    try:
                        if fileordir(path + atp) == 'File':
                            print path + atp
                            openfile(path + atp)
                    except:
                            path = atp
        except:
            msg('Error', 'TXTFMAN')
            atp = 'exit'

def openfile(path):
    import shell
    import PythonOS
    textfiles = ['.txt', '.rtf', '.pos']
    audiofiles = ['.wav', '.mp3', '.ogg']
    picfiles = ['.bmp', '.jpg', '.jpeg', '.JPG', '.png']
    treatliketext = ['.odf', '.doc', '.docx', '.htm', '.html', '.var']
    archives = ['.zip']
    try:
        f = open(path, 'r')
        f.close()
        for item in textfiles:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    txtedit(path)
                    return
                if shell.grunning == True:
                    PythonOS.components().okbox('Make the Python Shell Visible.', ['Please make your Python Shell visible.'])
                    txtedit(path)
                    return
        for item in audiofiles:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    if item == '.wav':
                        import wave
                        sound = wave.open(path)
                        sound.close()
                    return
                if shell.grunning == True:
                    PythonOS.music('play', path)
                    return
        for item in picfiles:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    msg('Opening a ' + item + ' file requires graphics', 'OPENFILE')
                    return
                if shell.grunning == True:
                    PythonOS.pictures(path)
                    return
        for item in treatliketext:
            if path.endswith(item) == True:
                if shell.grunning == False:
                    txtedit(path)
                    return
                if shell.grunning == True:
                    PythonOS.notes(path)
                    return
        for item in archives:
            if path.endswith(item) == True:
                PythonOS.gunzip(path)
        msg('This file cannot be opened.\nIf you have an app that supports this file type, try opening it from the app.', 'OPENFILE')
    except:
        msg('Error opening.', 'OPENFILE')
        return

def txtedit(path):
    msg('File ' + path, 'TXTEDIT')
    print ''
    try:
        tf = open(path, 'r')
        tf.close()
        for ln in readfile(path, 'rU'):
            print ln
        f = open(path, 'a')
    except:
        f = makefile(path)
    atf = raw_input ()
    while atf != 'exit':
        print >> f, atf
        atf = raw_input ()
    f.close()
    return

def execpy(path):
    import runpy
    msg('Executing .py file ' + path, 'RUNPY')
    runpy.run_path(path)

def execmod(name):
    import runpy
    msg('Executing Python Module ' + name, 'RUNPY')
    runpy.run_module(name)

def pyoscredits():
    for ln in readfile('res/system/credits.txt', 'rU'):
        print ln

def misc():
    pycfiles = ['commands.pyc', 'constants.pyc', 'PythonOS.pyc', 'shell.pyc']
    directories = ['var/', 'res/', 'user/']

def shiftchar(text, sft=1):
    import string
    return string.joinfields(map(eval("lambda ch:chr((ord(ch)-ord('a')+%d)%%26+ord('a'))"%(sft,)),text),'')

def logentry(app, inf):
    import datetime
    lf = open('data/system/syslog.txt', 'a')
    print >> lf, str(datetime.datetime.now()) + ':  ' + app + ': ' + inf
    lf.close()
                                        
