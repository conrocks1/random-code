import pvar
import PythonOS
import constants
import commands
import sys
import traceback
import shutil

grunning = False
cdir = ''
user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]

class interact():
    def sh(self, cmd = ''):
        shutil.copy('data/system/' + user + '/SIDEMENU-SIDEMENUITEMS.var', 'var/SIDEMENU-SIDEMENUITEMS.var')
        shutil.copy('data/system/' + user + '/SETTINGS-FULLSCREEN.var', 'var/SETTINGS-FULLSCREEN.var')
        shutil.copy('data/system/' + user + '/SETTINGS-DESKTOPBG.var', 'var/SETTINGS-DESKTOPBG.var')
        shutil.copy('data/system/' + user + '/SETTINGS-FPS.var', 'var/SETTINGS-FPS.var')
        shutil.copy('data/system/' + user + '/SETTINGS-RUNG.var', 'var/SETTINGS-RUNG.var')
        shutil.copy('PythonOS.py', 'data/system/PythonOS.py')
        shutil.copy('commands.py', 'data/system/commands.py')
        shutil.copy('kernel.py', 'data/system/kernel.py')
        shutil.copy('constants.py', 'data/system/constants.py')
        shutil.copy('shell.py', 'data/system/shell.py')
        shutil.rmtree('data/system/varbackup/', True)
        shutil.copytree('var/', 'data/system/varbackup/')
        l = 0
        rg = pvar.read('RUNG', 'SETTINGS')
        while cmd != 'exit':
            if rg[0] == 'True' and l == 0:
                cmd = 'startg'
                l = 1
            elif l == 0:
                cmd = raw_input (user + '@pythonos ' + cdir + '$ ')
            scmd = cmd.split()
            try:
                commands.parsecmd(scmd)
                if len(cmd.split()) > 0:
                    constants.logentry('SHELL', 'The command "' + scmd[0] + '" ran without errors.')
            except:
                if cmd != 'startg':
                    constants.msg('The command ' + scmd[0] + ' has crashed. Error Traceback:', 'SHELL')
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    traceback.print_exception(exc_type, exc_value, exc_traceback)
                    nf = constants.makefile('temp/traceback.txt')
                    tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
                    for ln in tb:
                        print >> nf, ln
                    nf.close()
                    constants.msg('This traceback is saved to temp/traceback.txt.', 'SHELL')
                    constants.logentry('SHELL', 'The command "' + cmd.split()[0] + '" encountered an error. The error is logged at temp/traceback.txt')
                if cmd == 'startg':
                    constants.msg('Graphics session ended.', 'SHELL')
                    constants.logentry('SHELL', 'GServer ended.')
                    global grunning
                    grunning = False
        shutil.copy('var/SIDEMENU-SIDEMENUITEMS.var', 'data/system/' + user + '/SIDEMENU-SIDEMENUITEMS.var')
        shutil.copy('var/SETTINGS-FULLSCREEN.var', 'data/system/' + user + '/SETTINGS-FULLSCREEN.var')
        shutil.copy('var/SETTINGS-DESKTOPBG.var', 'data/system/' + user + '/SETTINGS-DESKTOPBG.var')
        shutil.copy('var/SETTINGS-FPS.var', 'data/system/' + user + '/SETTINGS-FPS.var')
        shutil.copy('var/SETTINGS-RUNG.var', 'data/system/' + user + '/SETTINGS-RUNG.var')
        
    def signon(self):
        global user
        print ''
        print 'Python OS Login'
        try:
            bf = open('boot.kstr', 'rU')
            bf.close()
            bfound = True
        except IOError:
            bfound = False
        if bfound == False:
            uname = raw_input ('User Name : ')
            pwd = raw_input ('Password  : ')
            users = pvar.read('USERS', 'GLOBAL')
            found = False
            for user2 in users:
                if user2 == uname:
                    found = True
            if found == True:
                rpass = constants.shiftchar(constants.readfile('user/' + uname + '/' + uname + '.pwd', 'rU')[1], -1)  
                if rpass == pwd:
                    constants.msg('Now signed in as ' + uname, 'SHELL')
                    pvar.append('CURRENTUSER', uname, 'GLOBAL')
                    user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]
                    print ''
                    return uname
                if rpass != pwd:
                    constants.msg('Incorrect Password', 'SHELL')
                    interact().signon()
                    return uname
            if found == False:
                constants.msg('No such user: ' + uname, 'SHELL')
                return interact().signon()
        if bfound == True:
            uname = PythonOS.quickboot().up()
            constants.msg('Now signed in as ' + uname, 'SHELL')
            pvar.append('CURRENTUSER', uname, 'GLOBAL')
            user = pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1]
            print ''
            return uname
                
        
