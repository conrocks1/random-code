import constants
import PythonOS
import shell

variables = [['shver', '1.1']]

def parsecmd(scmd):
    if len(scmd) - 1 == -1:
        return
    if scmd[0] == 'install':
        try:
            f = open(shell.cdir + scmd[1], 'r')
            f.close()
            constants.install(shell.cdir + scmd[1])
        except:
            constants.install(scmd[1])
    if scmd[0] == "index":
        constants.msg('Indexing File System.', 'INDEXSERVICE')
        constants.index()
    if scmd[0] == 'fod':
        print constants.fileordir(shell.cdir + scmd[1])
    if scmd[0] == 'compileall':
        for cdir in constants.misc().directories:
            constants.compiledir(cdir)
    if scmd[0] == 'appreqg':
        constants.appreqg(scmd[1])
    if scmd[0] == 'killg':
        PythonOS.pygame.quit()
        shell.grunning = False
    if scmd[0] == 'changescreen':
        constants.changescreen(scmd[1], scmd[2])
    if scmd[0] == 'fullscreen-toggle':
        constants.fst()
    if scmd[0] == 'print':
        toprint = ''
        cpil = 1
        while cpil <= len(scmd) - 1:
            toprint = toprint + scmd[cpil] + ' '
            cpil = cpil + 1
        print toprint
    if scmd[0] == 'pvar-make':
        import pvar
        towrite = ''
        cpil = 2
        while cpil <= len(scmd) - 1:
            towrite = towrite + ' ' + scmd[cpil]
            cpil = cpil + 1
        pvar.make(scmd[1], towrite, 'USERSHELL')
    if scmd[0] == 'pvar-read':
        import pvar
        string = ''
        for ln in pvar.read(scmd[1], 'USERSHELL'):
            string = string + ln
        print string
    if scmd[0] == 'pvar-append':
        import pvar
        pvar.append(scmd[1], scmd[2], 'USERSHELL')
    if scmd[0] == 'pvar-delete':
        import pvar
        pvar.destroy(scmd[1], 'USERSHELL')
    if scmd[0] == 'txtfman':
        try:
            constants.textfman(shell.cdir + scmd[1])
        except:
            constants.textfman(shell.cdir)
    if scmd[0] == 'txtedit':
        constants.txtedit(shell.cdir + scmd[1])
    if scmd[0] == 'cd':
        if scmd[1] != '~':
            if scmd[1].endswith('/') == False:
                scmd[1] = scmd[1] + '/'
            shell.cdir = shell.cdir + scmd[1]
        if scmd[1] == '~':
            shell.cdir = ''
    if scmd[0] == 'rm':
        constants.deletefile(shell.cdir + scmd[1])
    if scmd[0] == 'ls':
        if shell.cdir != '':
            import os
            for itm in os.listdir(shell.cdir):
                print itm
    if scmd[0] == 'wget':
        constants.msg('Downloading ' + scmd[1] + ' to ' + scmd[2], 'WGET')
        import urllib
        urllib.urlretrieve(scmd[1], scmd[2])
    if scmd[0] == 'credits':
        constants.pyoscredits()
    if scmd[0] == 'uninstall':
        if len(scmd) - 1 == 2:
            constants.uninstall(scmd[1], scmd[2])
        elif len(scmd) - 1 != 2:
            constants.uninstall(scmd[1], '-n')
    if scmd[0] == 'startg':
        PythonOS.gserver()
        PythonOS.gui().boot()
    if scmd[0] == 'apps':
        PythonOS.gui().apps()
    if scmd[0] == 'cipher':
        tocipher = raw_input ('String to cipher: ')
        i = int(raw_input ('Integer to cipher by: '))
        fs = ''
        for char in tocipher:
            if char != ' ' and char != '!' and char != '.' and char != ',' and char != '?' and char != "'" and char != ':' and char != ';':
                fs = fs + constants.shiftchar(char, i)
            if char == ' ':
                fs = fs + ' '
            if char == '!':
                fs = fs + '!'
            if char == '.':
                fs = fs + '.'
            if char == ',':
                fs = fs + ','
            if char == '?':
                fs = fs + '?'
            if char == "'":
                fs = fs + "'"
            if char == ':':
                fs = fs + ':'
            if char == ';':
                fs = fs + ';'
        constants.msg(fs, 'RESULT')
    if scmd[0] == 'cp':
        constants.msg('Copying file ...', 'COPY')
        import shutil
        shutil.copy2(shell.cdir + scmd[1], scmd[2])
        constants.msg('Done.', 'COPY')
    if scmd[0] == 'mv':
        constants.msg('Moving file ...', 'MOVE')
        import shutil
        shutil.copy2(shell.cdir + scmd[1], scmd[2])
        constants.deletefile(shell.cdir + scmd[1])
        constants.msg('Done.', 'MOVE')
    if scmd[0] == 'pman':
        try:
            if scmd[1] == '-i':
                PythonOS.pman().install(scmd[2])
            if scmd[1] == '-cup':
                PythonOS.pman().updateapps()
            if scmd[1] == '-u':
                PythonOS.pman().uninstall(scmd[2])
            if scmd[1] == '-ri':
                PythonOS.pman().getapp(scmd[2])
        except:
            PythonOS.pman().pgui()
    if scmd[0] == 'var':
        import pvar
        if scmd[1] == '-m':
            ail = False
            for v in variables:
                if v[0] == scmd[2]:
                    v[1] = scmd[3]
                    ail = True
            if ail == False:
                s = scmd[3]
                cpos = 4
                while cpos <= len(scmd) - 1:
                    s = s + ' ' + scmd[cpos]
                    cpos = cpos + 1
                variables.append([scmd[2], s])
        if scmd[1] == '-p':
            for v in variables:
                if v[0] == scmd[2]:
                    print v[1]
        if scmd[1] == '-r':
            s = ''
            for v in variables:
                if v[0] == scmd[2]:
                    s = v[1]
            pvar.make('VAR', s, 'TRANSFER')
        if scmd[1] == '-rd':
            pvar.destroy('VAR', 'TRANSFER')
        if scmd[1] == '-d':
            cpos = 0
            while cpos <= len(variables) - 1:
                if variables[cpos][0] == scmd[2]:
                    variables.pop(cpos)
                    continue
                cpos = cpos + 1
    if scmd[0] == "system":
        PythonOS.sysinfo()
    if scmd[0] == "pyexit":
        PythonOS.pyexit()
    if scmd[0] == "settings":
        PythonOS.settings()
    if scmd[0] == "files":
        PythonOS.files()
    if scmd[0] == "pictures":
        PythonOS.pictures('')
    if scmd[0] == "music":
        PythonOS.music()
    if scmd[0] == "clock":
        PythonOS.clockapp()
    if scmd[0] == "toolbargui":
        PythonOS.toolbargui()
    if scmd[0] == "toolbarman":
        PythonOS.toolbarman(None)
    if scmd[0] == "toolbaradd":
        PythonOS.toolbaradd()
    if scmd[0] == "volumeplugin":
        PythonOS.volumeplugin()
    if scmd[0] == "sleep":
        PythonOS.sleep()
    if scmd[0] == "search":
        PythonOS.search()
