import constants as c
import pvar
import commands
try:
    import pygame
except:
    c.msg('No Pygame.', 'ERROR')
import datetime
import os
import random
import time
import shell
appsurface = None
screen = None
toolbarplugs = []

def gserver():
    c.msg('Gserver v1.0', 'GSERVER')
    import shell
    global width
    global height
    global font
    global largefont
    global smallfont
    global bfont
    global largebfont
    global smallbfont
    global ifont
    global largeifont
    global smallifont
    global clock
    global screen
    global appsurface
    global full
    global ctheme
    ctheme = None
    global processes
    processes = ['Python OS System']
    global daemons
    daemons = []
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    pygame.mixer.init()
    pygame.display.set_caption('Python OS')
    pygame.display.set_icon(pygame.image.load('res/system/graphics/icons/windowicon.png'))
    try:
        width = pygame.display.Info().current_w
        height = pygame.display.Info().current_h
    except:
        width = 1366
        height = 768
    pfull = pvar.read('FULLSCREEN', 'SETTINGS')[0]
    if pfull == 'no':
        screen = pygame.display.set_mode([width, height], pygame.RESIZABLE)
        full = False
    if pfull ==  'yes':
        screen = pygame.display.set_mode([width, height], pygame.RESIZABLE | pygame.HWSURFACE | pygame.FULLSCREEN)
        full = True
    appsurface = pygame.Surface([width, height - 20])
    font = pygame.font.Font('res/system/Ubuntu-R.ttf', 14)
    largefont = pygame.font.Font('res/system/Ubuntu-R.ttf', 40)
    smallfont = pygame.font.Font('res/system/Ubuntu-R.ttf', 8)
    bfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 14)
    largebfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 40)
    smallbfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 8)
    ifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 14)
    largeifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 40)
    smallifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 8)
    clock = pygame.time.Clock()
    shell.grunning = True

def centerobj(ow, oh, surface):
    s1 = ow / 2
    s2 = oh / 2
    rbw = (surface.get_width() / 2) - s1
    rbh = (surface.get_height() / 2) - s2
    return [rbw, rbh]

class components():
    def button(self, x, y, text):
        ftxt = font.render(text, 1, [25, 25, 25])
        button = pygame.transform.smoothscale(buttonimg, (ftxt.get_width() + 8, ftxt.get_height() + 4))
        appsurface.blit(button, [x, y])
        center = centerobj(button.get_width(), button.get_height(), button)
        appsurface.blit(ftxt, [x + center[0], y + center[1]])
        tret = [x + 2, x + 2 + button.get_width(), y + 20, y + 20 + button.get_height()]
        return tret
    def toolbar(self):
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        bw = width - 35
        for plug in toolbarplugs:
            ico = pygame.image.load(plug[0])
            bw = (bw - 2) - ico.get_width()
            screen.blit(ico, [bw, 0])
        mp = pygame.mouse.get_pos()
        if mp[1] <= 20 and mp[0] >= bw - 10:
            toolbargui()
        daemonman().run()
        components().mouse()
    def mouse(self):
        if ctheme == 'darktheme':
            color = [100, 100, 100]
        if ctheme == 'lighttheme':
            color = [50, 50, 50]
        if ctheme == 'blacktheme':
            color = [200, 200, 200]
        if ctheme == 'whitetheme':
            color = [50, 50, 50]
        pygame.draw.circle(screen, color, pygame.mouse.get_pos(), 3)
    def okbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/okbox-bg.jpg')
        obg = pygame.image.load('temp/okbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        dp = centerobj(400, 300, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'OK')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        lps().end()
                        return 'OK'
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return 'OK'
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def inputbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/inputbox-bg.jpg')
        obg = pygame.image.load('temp/inputbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        dp = centerobj(400, 300, screen)
        reply = ''
        txt = 'Enter text here.'
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            if reply != '':
                ib = components().inputbutton(dp[0] + centerobj(300, 250, dialogimg)[0], dp[1] + 200, reply)
            if reply == '':
                ib = components().inputbutton(dp[0] + centerobj(300, 250, dialogimg)[0], dp[1] + 200, txt)
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'OK')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        lps().end()
                        return reply
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                    if event.key == pygame.K_DELETE:
                        reply = ''
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return reply
                        if mousepos[0] >= ib[0] and mousepos[0] <= ib[1]:
                            if mousepos[1] >= ib[2] and mousepos[1] <= ib[3]:
                                reply = reply + components().getinput(ib[0] - 2, ib[2] - 20, reply)
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def ynbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/ynbox-bg.jpg')
        obg = pygame.image.load('temp/ynbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        selected = 1
        dp = centerobj(400, 300, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'Yes')
            bp1 = components().button(dp[0] + 60, dp[1] + 255, 'No')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        if selected == 0:
                            lps().end()
                            return 'No'
                        if selected == 1:
                            lps().end()
                            return 'Yes'
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                    if event.key == pygame.K_RIGHT:
                        if selected == 1:
                            selected = 0
                    if event.key == pygame.K_LEFT:
                        if selected == 0:
                            selected = 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return 'Yes'
                        if mousepos[0] >= bp1[0] and mousepos[0] <= bp1[1]:
                            if mousepos[1] >= bp1[2] and mousepos[1] <= bp1[3]:
                                lps().end()
                                return 'No'
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def menu(self):
        if pygame.mouse.get_pos()[0] <= 2 and pygame.mouse.get_pos()[1] > 21:
            mb = pygame.image.save(appsurface, 'temp/sidemenu-bg.jpg')
            mb = pygame.image.load('temp/sidemenu-bg.jpg')
            clickables = []
            cp = 21
            for mitem in sidemenufinalitems:
                clickables.append([[1, 49, cp, cp + 49], mitem[2]])
                cp = cp + 49
            while True:
                clock.tick(fps)
                screen.blit(pygame.transform.smoothscale(sidemenuimg, [50, height - 20]), [0, 20])
                screen.blit(mb, [50, 20])
                cp = 21
                for mitem in sidemenufinalitems:
                    screen.blit(pygame.image.load(mitem[1]), [1, cp])
                    cp = cp + 49
                components().toolbar()
                mousepos = pygame.mouse.get_pos()
                if mousepos[0] >= 80:
                    pygame.draw.rect(screen, [50, 50, 50], [0, 20, 2, height - 20])
                    pygame.display.update()
                    return
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        cc = 0
                        while cc <= len(clickables) - 1:
                            obj = clickables[cc]
                            if mousepos[0] >= obj[0][0] and mousepos[0] <= obj[0][1]:
                                if mousepos[1] >= obj[0][2] and mousepos[1] <= obj[0][3]:
                                    if event.button == 1:
                                        screen.fill([50, 50, 50])
                                        pygame.display.update()
                                        commands.parsecmd([obj[1]])
                                        screen.fill([50, 50, 50])
                                        pygame.display.update()
                                        return
                                    if event.button == 3:
                                        gui().rcm_sidemenu(obj[1])
                            cc = cc + 1
                        if mousepos[0] >= 50 and mousepos[0] <= width:
                            if mousepos[1] >= 20 and mousepos[1] <= height:
                                if event.button == 1:
                                    screen.fill([50, 50, 50])
                                    pygame.display.update()
                                    return
    def inputbutton(self, x, y, text):
        ftxt = font.render(text, 1, [25, 25, 25])
        button = pygame.transform.smoothscale(inputboximg, (ftxt.get_width() + 26, ftxt.get_height() + 2))
        appsurface.blit(button, [x, y])
        center = centerobj(button.get_width(), button.get_height(), button)
        appsurface.blit(ftxt, [x + center[0], y + center[1]])
        tret = [x + 2, x + 2 + button.get_width(), y + 20, y + 20 + button.get_height()]
        return tret
    def imagebutton(self, x, y, image, targetsurf=appsurface):
        global appsurface
        if targetsurf == None:
            appsurface.blit(image, [x, y])
            tret = [x + 2, x + 2 + image.get_width(), y + 20, y + 20 + image.get_height()]
        elif targetsurf != None:
            targetsurf.blit(image, [x, y])
            tret = [x, x + image.get_width(), y, y + image.get_height()]
        return tret
    def getinput(self, wpix, hpix, suin):
        uinput = []
        uin = suin
        shift = False
        done = False
        pygame.image.save(appsurface, 'temp/getinput.jpg')
        gbg = pygame.image.load('temp/getinput.jpg')
        color = [50, 50, 50]
        ibi = inputboximg
        while done != True:
            clock.tick(10)
            appsurface.blit(gbg, [0, 0])
            text = font.render(str(uin), 1, color)
            if text.get_width() >= ibi.get_width():
                ibi = pygame.transform.smoothscale(ibi, [text.get_width() + 2, ibi.get_height()])
            appsurface.blit(ibi, [wpix, hpix])
            appsurface.blit(text, [wpix + 2, hpix + 2])
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            components().toolbar()
            pygame.display.update()
            mousepos = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.VIDEORESIZE:
                    width = event.w
                    height = event.h
                    gbg = pygame.transform.smoothscale(gbg, (width, height))
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] < wpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        return uin
                    elif mousepos[0] > text.get_width() + 2:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        return uin
                    elif mousepos[0] < hpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        return uin
                    elif mousepos[0] > hpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        return uin
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        done = True
                    if event.key == pygame.K_RETURN:
                        done = True
                    if event.key == pygame.K_a:
                        if shift == False:
                            uinput.append('a')
                        if shift == True:
                            uinput.append('A')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_b:
                        if shift == False:
                            uinput.append('b')
                        if shift == True:
                            uinput.append('B')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_c:
                        if shift == False:
                            uinput.append('c')
                        if shift == True:
                            uinput.append('C')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_d:
                        if shift == False:
                            uinput.append('d')
                        if shift == True:
                            uinput.append('D')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_e:
                        if shift == False:
                            uinput.append('e')
                        if shift == True:
                            uinput.append('E')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_f:
                        if shift == False:
                            uinput.append('f')
                        if shift == True:
                            uinput.append('F')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_g:
                        if shift == False:
                            uinput.append('g')
                        if shift == True:
                            uinput.append('G')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_h:
                        if shift == False:
                            uinput.append('h')
                        if shift == True:
                            uinput.append('H')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_i:
                        if shift == False:
                            uinput.append('i')
                        if shift == True:
                            uinput.append('I')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_j:
                        if shift == False:
                            uinput.append('j')
                        if shift == True:
                            uinput.append('J')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_k:
                        if shift == False:
                            uinput.append('k')
                        if shift == True:
                            uinput.append('K')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_l:
                        if shift == False:
                            uinput.append('l')
                        if shift == True:
                            uinput.append('L')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_m:
                        if shift == False:
                            uinput.append('m')
                        if shift == True:
                            uinput.append('M')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_n:
                        if shift == False:
                            uinput.append('n')
                        if shift == True:
                            uinput.append('N')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_o:
                        if shift == False:
                            uinput.append('o')
                        if shift == True:
                            uinput.append('O')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_p:
                        if shift == False:
                            uinput.append('p')
                        if shift == True:
                            uinput.append('P')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_q:
                        if shift == False:
                            uinput.append('q')
                        if shift == True:
                            uinput.append('Q')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_r:
                        if shift == False:
                            uinput.append('r')
                        if shift == True:
                            uinput.append('R')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_s:
                        if shift == False:
                            uinput.append('s')
                        if shift == True:
                            uinput.append('S')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_t:
                        if shift == False:
                            uinput.append('t')
                        if shift == True:
                            uinput.append('T')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_u:
                        if shift == False:
                            uinput.append('u')
                        if shift == True:
                            uinput.append('U')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_v:
                        if shift == False:
                            uinput.append('v')
                        if shift == True:
                            uinput.append('V')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_w:
                        if shift == False:
                            uinput.append('w')
                        if shift == True:
                            uinput.append('W')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_x:
                        if shift == False:
                            uinput.append('x')
                        if shift == True:
                            uinput.append('X')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_y:
                        if shift == False:
                            uinput.append('y')
                        if shift == True:
                            uinput.append('Y')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_z:
                        if shift == False:
                            uinput.append('z')
                        if shift == True:
                            uinput.append('Z')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_LSHIFT or event.key == pygame.K_RSHIFT:
                        shift = True
                    if event.key == pygame.K_INSERT:
                        if shift == False:
                            if copied == '':
                                copied = uin
                            elif copied != '':
                                uinput.append(copied)
                                uin = ''
                                for i in uinput:
                                    uin = uin + i
                        if shift == True:
                            copied = ''
                    if event.key == pygame.K_SPACE:
                        uinput.append(' ')
                    if event.key == pygame.K_DELETE:
                        uinput = []
                        uin = ''
                    if event.key == pygame.K_TAB:
                        uinput.append(' ')
                    if event.key == pygame.K_1:
                        if shift == True:
                            uinput.append('!')
                            shift = False
                        elif shift == False:
                            uinput.append('1')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_2:
                        if shift == True:
                            uinput.append('@')
                            shift = False
                        elif shift == False:
                            uinput.append('2')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_3:
                        if shift == True:
                            uinput.append('#')
                            shift = False
                        elif shift == False:
                            uinput.append('3')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_4:
                        if shift == True:
                            uinput.append('$')
                            shift = False
                        elif shift == False:
                            uinput.append('4')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_5:
                        if shift == True:
                            uinput.append('%')
                            shift = False
                        elif shift == False:
                            uinput.append('5')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_6:
                        if shift == True:
                            uinput.append('^')
                            shift = False
                        elif shift == False:
                            uinput.append('6')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_7:
                        if shift == True:
                            uinput.append('&')
                            shift = False
                        elif shift == False:
                            uinput.append('7')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_8:
                        if shift == True:
                            uinput.append('*')
                            shift = False
                        elif shift == False:
                            uinput.append('8')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_9:
                        if shift == True:
                            uinput.append('(')
                            shift = False
                        elif shift == False:
                            uinput.append('9')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_0:
                        if shift == True:
                            uinput.append(')')
                            shift = False
                        elif shift == False:
                            uinput.append('0')
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_PERIOD:
                        if shift == False:
                            uinput.append('.')
                        elif shift == True:
                            uinput.append('>')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_COMMA:
                        if shift == False:
                            uinput.append(',')
                        elif shift == True:
                            uinput.append('<')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_SLASH:
                        if shift == False:
                            uinput.append('/')
                        elif shift == True:
                            uinput.append('?')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_MINUS:
                        if shift == False:
                            uinput.append('-')
                        elif shift == True:
                            uinput.append('_')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_SEMICOLON:
                        if shift == False:
                            uinput.append(';')
                        elif shift == True:
                            uinput.append(':')
                            shift = False
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                    if event.key == pygame.K_BACKSPACE:
                        if len(uinput) >= 1:
                            uinput.pop()
                        uin = ''
                        for i in uinput:
                            uin = uin + i
            uin = ''
            for i in uinput:
                uin = uin + i
        uin = ''
        for i in uinput:
            uin = uin + i
        return uin
                                

class lps():
    def new(self, name):
        processes.append(name)
        c.logentry('lps', 'Application ' + name + ' officially started')
        return name
    def end(self):
        c.logentry('lps', 'Application ' + processes[len(processes) - 1] + ' officially ended')
        return processes.pop()
    def listall(self):
        return processes
        
class gui():
    def boot(self):
        c.msg('Booting Python OS GUI', 'POSGUI')
        global ctheme
        ctheme = None
        global processes
        processes = ['Python OS System']
        global mousepos
        global vol
        global full
        global copied
        copied = ''
        full = False
        vol = 0.5
        mousepos = centerobj(4, 4, screen)
        pygame.mouse.set_pos(mousepos)
        pygame.mouse.set_visible(False)
        pygame.key.set_repeat(50, 50)
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
        sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
        sidemenufinalitems = []
        for i in sidemenuitems:
            rf = c.readfile('res/' + i + '/req.txt', 'rU')
            icon = rf[3]
            name = rf[4]
            execp = rf[2]
            sidemenufinalitems.append([name, icon, execp])
        installedappsfinal = []
        for inst in installedapps:
            rf = c.readfile('res/' + inst + '/req.txt', 'rU')
            icon = rf[3]
            name = rf[4]
            execp = rf[2]
            if rf[3].startswith('noicon') == False:
                installedappsfinal.append([name, icon, execp])
        global toolbarplugstarters
        toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
        for plg in toolbarplugstarters:
            rf = c.readfile('res/' + plg + '/toolbar.txt')
            icon = rf[0]
            run = rf[1]
            toolbarplugs.append([icon, run])
        global pythonpoweredl
        global pythonpowereds
        global bg
        global buttonimg
        global toolbarimg
        global dialogimg
        global sidemenuimg
        global closeicon
        global rightclickmenuimg
        global inputboximg
        inputboximg = pygame.image.load('res/system/graphics/input.png')
        rightclickmenuimg = pygame.image.load('res/system/graphics/right-click-menu.png')
        closeicon = pygame.image.load('res/system/graphics/icons/close.png')
        sidemenuimg = pygame.image.load('res/system/graphics/menu.png')
        pythonpoweredl = pygame.image.load('res/system/python-powered-l.png')
        pythonpowereds = pygame.image.load('res/system/python-powered-s.png')
        bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
        buttonimg = pygame.image.load('res/system/graphics/button.png')
        toolbarimg = pygame.image.load('res/system/graphics/toolbar.png')
        dialogimg = pygame.image.load('res/system/graphics/dialog.png')
        screen.fill([50, 50, 50])
        screen.blit(pygame.transform.smoothscale(bg, [width, height]), [0, 0])
        screen.blit(pythonpoweredl, centerobj(200, 80, screen))
        pygame.display.update()
        global red
        global green
        global blue
        global black
        global white
        global gray
        global lightgray
        global darkwhite
        red = [255, 0, 0]
        green = [0, 255, 0]
        blue = [0, 0, 255]
        black = [0, 0, 0]
        white = [255, 255, 255]
        gray = [50, 50, 50]
        lightgray = [100, 100, 100]
        darkwhite = [200, 200, 200]
        global darktheme
        global lighttheme
        global blacktheme
        global whitetheme
        darktheme = [gray, lightgray, darkwhite]
        lighttheme = [darkwhite, lightgray, gray]
        blacktheme = [black, gray, lightgray]
        whitetheme = [white, black, darkwhite]
        daemonman().atstart()
        gui().desktop()
        return
    def desktop(self):
        global ctheme
        global bg
        global width
        global height
        global fps
        lps().new('Home')
        ctheme = 'darktheme'
        bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
        apps = pvar.read('DESKTOPAPPS', 'SETTINGS')
        fps = int(pvar.read('FPS', 'SETTINGS')[0])
        bg = pygame.transform.smoothscale(bg, [width - 2, height - 20])
        appicon = pygame.image.load('res/system/graphics/icons/grayapps.png')
        icntr = centerobj(49, 49, screen)
        count = 0
        minx2 = fps * 60 * 2
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(bg, [0, 0])
            abtn = components().imagebutton(icntr[0], height - 149, appicon, appsurface)
            screen.blit(appsurface, [2, 20])
            try:
                components().toolbar()
                components().menu()
            except:
                error()
            pygame.display.update()
            if count == minx2:
                commands.parsecmd(['sleep'])
                count = 0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    commands.parsecmd(['killg'])
                    lps().end()
                    return
                if event.type == pygame.VIDEORESIZE:
                    width = event.w
                    height = event.h
                    icntr = centerobj(49, 49, screen)
                    bg = pygame.transform.smoothscale(bg, [width - 2, height - 20])
                if event.type == pygame.KEYDOWN:
                    count = 0
                    if event.key == pygame.K_ESCAPE:
                        if lps().listall()[len(lps().listall()) - 2] != 'Python OS System':
                            lps().end()
                            return
                        if lps().listall()[len(lps().listall()) - 2] == 'Python OS System':
                            pyexit()
                    if event.key == pygame.K_MENU:
                        mousepos = [1, 200]
                        pygame.mouse.set_pos(mousepos)
                    if event.key == pygame.K_RSUPER or event.key == pygame.K_LSUPER:
                        gui().apps()
                    if event.key == pygame.K_F1:
                        lps().end()
                        lps().end()
                        screen.fill([0, 0, 0])
                        pygame.display.update()
                        return
                    if event.key == pygame.K_F2:
                        gotfps = clock.get_fps()
                        c.logentry('INFO', 'FPS of Home is: ' + str(gotfps))
                        components().okbox('FPS Evaluation', ['Your framerate is: ' + str(gotfps) + '.', 'Python OS is set to reach: ' + str(fps), 'Change this in Settings >> Change FPS of Applications.'])
                if event.type == pygame.MOUSEBUTTONDOWN:
                    count = 0
                    if event.button == 1:
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                if lps().listall()[len(lps().listall()) - 2] != 'Python OS System':
                                    lps().end()
                                    return
                                if lps().listall()[len(lps().listall()) - 2] == 'Python OS System':
                                    pyexit()
                        if mousepos[0] >= abtn[0] and mousepos[0] <= abtn[1]:
                            if mousepos[1] >= abtn[2] and mousepos[1] <= abtn[3]:
                                gui().apps()
            count = count + 1
                    
    def apps(self):
        global ctheme
        global width
        global height
        global bg
        lps().new('Applications')
        ctheme = 'darktheme'
        bg = pygame.transform.smoothscale(pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0]), [width, height - 20])
        clickables = []
        w = 20
        h = 10
        for a in installedappsfinal:
            clickables.append([[w, w + 50, h, h + 60], a[2]])
            if w < width - 100:
                w = w + 100
            if w >= width - 100:
                w = 20
                h = h + 70
        while True:
            clock.tick(fps)
            appsurface.blit(bg, [0, 0])
            mousepos = pygame.mouse.get_pos()
            w = 20
            h = 10
            for app in installedappsfinal:
                aicon = pygame.image.load(app[1])
                appsurface.blit(aicon, [w, h])
                aname = font.render(app[0], 1, (25, 25, 25))
                appsurface.blit(aname, [w, h + 49])
                if w < width - 100:
                    w = w + 100
                if w >= width - 100:
                    w = 20
                    h = h + 70
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        lps().end() 
                        return
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for abutton in clickables:
                        if mousepos[0] >= abutton[0][0] and mousepos[0] <= abutton[0][1]:
                            if mousepos[1] >= abutton[0][2] and mousepos[1] <= abutton[0][3]:
                                if event.button == 1:
                                    try:
                                        commands.parsecmd([abutton[1]])
                                    except:
                                        error()
                                    lps().end()
                                    return
                                if event.button == 3:
                                    gui().rcm_app(abutton[1])
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            lps().end()
                            return
    def rcm_app(self, execp):
        global sidemenufinalitems
        global sidemenuitems
        rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
        rcmbg = pygame.image.load('temp/rcmbg.jpg')
        mp = pygame.mouse.get_pos()
        while True:
            clock.tick(fps)
            appsurface.blit(rcmbg, [0, 0])
            appsurface.blit(rightclickmenuimg, mp)
            addtol = components().button(mp[0] + 2, mp[1] + 2, 'Add to Launcher')
            rem = components().button(mp[0] + 2, mp[1] + 25, 'Uninstall')
            run = components().button(mp[0] + 2, mp[1] + 48, 'Open')
            tlb = components().button(mp[0] + 2, mp[1] + 71, 'Add to Toolbar')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            mousepos = pygame.mouse.get_pos()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] >= addtol[0] and mousepos[0] <= addtol[1]:
                        if mousepos[1] >= addtol[2] and mousepos[1] <= addtol[3]:
                            pvar.append('SIDEMENUITEMS', execp, 'SIDEMENU')
                            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            sidemenufinalitems = []
                            for i in sidemenuitems:
                                rf = c.readfile('res/' + i + '/req.txt', 'r')
                                icon = rf[3]
                                name = rf[4]
                                execp = rf[2]
                                sidemenufinalitems.append([name, icon, execp])
                            return
                    if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                        if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                            if components().ynbox('Do you really want to uninstall?', ['You are about to uninstall:', execp, 'Do you wish to proceed with uninstallation?']) == 'Yes':
                                commands.parsecmd(['uninstall', execp, '-n'])
                                name = execp
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                for a in sidemenuitems:
                                    if a == name:
                                        pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                        for ca in sidemenuitems:
                                            if ca != name and ca != 'appmenu':
                                                pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                sidemenufinalitems = []
                                for i in sidemenuitems:
                                    rf = c.readfile('res/' + i + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    sidemenufinalitems.append([name, icon, execp])
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                components().okbox('Application Uninstalled', ['The application ' + execp + ' has been uninstalled.'])
                            return
                    if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                        if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                            commands.parsecmd([execp])
                            return
                    if mousepos[0] >= tlb[0] and mousepos[0] <= tlb[1]:
                        if mousepos[1] >= tlb[2] and mousepos[1] <= tlb[3]:
                            toolbaradd(execp)
                            return
                    if mousepos[0] >= mp[0] + 152 and mousepos[0] <= width:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= 2 and mousepos[0] <= mp[0] + 2:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= 0 and mousepos[1] <= mp[1] - 20:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                            return
    def rcm_sidemenu(self, execp):
        global sidemenufinalitems
        global sidemenuitems
        rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
        rcmbg = pygame.image.load('temp/rcmbg.jpg')
        mp = pygame.mouse.get_pos()
        while True:
            clock.tick(fps)
            appsurface.blit(rcmbg, [0, 0])
            appsurface.blit(rightclickmenuimg, mp)
            rln = components().button(mp[0] + 2, mp[1] + 2, 'Remove Link')
            rem = components().button(mp[0] + 2, mp[1] + 25, 'Uninstall')
            run = components().button(mp[0] + 2, mp[1] + 48, 'Open')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            mousepos = pygame.mouse.get_pos()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] >= rln[0] and mousepos[0] <= rln[1]:
                        if mousepos[1] >= rln[2] and mousepos[1] <= rln[3]:
                            insm = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            pvar.destroy('SIDEMENUITEMS', 'SIDEMENU')
                            pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                            for app in insm:
                                if app != execp and app != 'appmenu':
                                    pvar.append('SIDEMENUITEMS', app, 'SIDEMENU')
                            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            sidemenufinalitems = []
                            for i in sidemenuitems:
                                rf = c.readfile('res/' + i + '/req.txt', 'r')
                                icon = rf[3]
                                name = rf[4]
                                execp = rf[2]
                                sidemenufinalitems.append([name, icon, execp])
                            return
                    if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                        if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                            if components().ynbox('Do you really want to uninstall?', ['You are about to uninstall:', execp, 'Do you wish to proceed with uninstallation?']) == 'Yes':
                                commands.parsecmd(['uninstall', execp, '-n'])
                                name = execp
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                for a in sidemenuitems:
                                    if a == name:
                                        pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                        for ca in sidemenuitems:
                                            if ca != name and ca != 'appmenu':
                                                pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                sidemenufinalitems = []
                                for i in sidemenuitems:
                                    rf = c.readfile('res/' + i + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    sidemenufinalitems.append([name, icon, execp])
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                components().okbox('Application Uninstalled', ['The application ' + execp + ' has been uninstalled.'])
                            return
                    if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                        if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                            commands.parsecmd([execp])
                            return
                    if mousepos[0] >= mp[0] + 152 and mousepos[0] <= width:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= 2 and mousepos[0] <= mp[0] + 2:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= 0 and mousepos[1] <= mp[1] - 20:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                            return

def error():
    import traceback
    import sys
    exc_type, exc_value, exc_traceback = sys.exc_info()
    tb = traceback.format_exc()
    tbf = c.makefile('temp/traceback.txt')
    print >> tbf, tb
    tbf.close()
    c.logentry('ERROR', 'Python OS has experienced an error that stopped the normal flow of operations.')
    c.logentry('INFO', "A transcript of the error's traceback is at temp/traceback.txt.")
    rtb = c.readfile('temp/traceback.txt', 'r')
    screen.fill([50, 50, 50])
    screen.blit(font.render('The application ' + lps().listall()[len(lps().listall()) - 1] + ' has crashed. Press Space key to continue.', 1, (255, 100, 100)), [20, 20])
    h = 40
    for ln in rtb:
        screen.blit(font.render(ln, 1, (200, 200, 200)), [20, h])
        h = h + 16
    h = h + 40
    oh = h
    for app in lps().listall():
        screen.blit(font.render(app, 1, (200, 200, 200)), [20, h])
        h = h + 16
    pygame.display.update()
    done = False
    while done != True:
        clock.tick(5)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    done = True
    screen.blit(font.render('Python OS has experienced an error. Quit graphics? (Y/N)', 1, (200, 200, 200)), [20, h])
    pygame.display.update()
    done = False
    while done != True:
        clock.tick(5)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_y:
                    qg = True
                    done = True
                if event.key == pygame.K_n:
                    qg = False
                    done = True
    if qg == True:
        commands.parsecmd(['killg'])
    lps().end()
    return
class daemonman():
    def atstart(self):
        dstarts = pvar.read('DAEMONS', 'DMAN')
        for dstart in dstarts:
            daemonman().add(dstart)
    def add(self, name):
        global daemons
        dinf = c.readfile('res/' + name + '/daemon.txt')
        eval(dinf[1])
        daemons.append([name, dinf[0]])
    def delete(self, name):
        global daemons
        nd = []
        for daemon in daemons:
            if daemon[0] == name:
                continue
            nd.append(daemon)
        daemons = nd
    def run(self):
        for daemon in daemons:
            eval(daemon[1])
def sysinfo():
    components().okbox('System Information', ['Python OS 4.1', 'Version: 4.1.0', 'Kernel: 1.1-pyos4', 'GServer: 1.01'])
    return

def pyexit():
    r = components().ynbox('Sure to Exit?', ['Are you sure you want to exit?', 'This will close all:', 'Active Files', 'Active Applications'])
    if r == 'Yes':
        try:
            pvar.read('NSR', 'NESTEDSESSION')
            commands.parsecmd(['exit'])
        except:
            commands.parsecmd(['killg'])
            return
def settings():
    lps().new('Settings')
    global fps
    global bg
    global screen
    global width
    global height
    global full
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        b1 = components().button(20, 20, 'Change FPS of applications')
        b2 = components().button(20, b1[3] + 2, 'Change the Desktop background')
        b3 = components().button(20, b2[3] + 2, 'Change boot desktop at login')
        b4 = components().button(20, b3[3] + 2, 'Toggle Fullscreen')
        screen.blit(appsurface, [2, 20])
	components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.VIDEORESIZE:
                width = event.w
                height = event.h
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        return
                if mousepos[0] >= b1[0] and mousepos[0] <= b1[1]:
                    if mousepos[1] >= b1[2] and mousepos[1] <= b1[3]:
                        pvar.make('FPS', components().inputbox('Set the FPS of applications', ['Enter the Flips Per Second of applications.', 'Default: 10']), 'SETTINGS')
                        fps = int(pvar.read('FPS', 'SETTINGS')[0])
                if mousepos[0] >= b2[0] and mousepos[0] <= b2[1]:
                    if mousepos[1] >= b2[2] and mousepos[1] <= b2[3]:
                        path = files('return')
                        if c.readfile(path, 'r') != None:
                            pvar.make('DESKTOPBG', path, 'SETTINGS')
                            bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
                if mousepos[0] >= b3[0] and mousepos[0] <= b3[1]:
                    if mousepos[1] >= b3[2] and mousepos[1] <= b3[3]:
                        answer = components().ynbox('Start Graphics at login?', ['Do you want to start the Desktop when you log in?'])
                        if answer == 'Yes':
                            pvar.make('RUNG', 'True', 'SETTINGS')
                        if answer == 'No':
                            pvar.make('RUNG', 'False', 'SETTINGS')
                if mousepos[0] >= b4[0] and mousepos[0] <= b4[1]:
                    if mousepos[1] >= b4[2] and mousepos[1] <= b4[3]:
                        if full == False:
                            screen = pygame.display.set_mode([0, 0], pygame.FULLSCREEN | pygame.RESIZABLE | pygame.HWSURFACE)
                            full = True
                            pvar.make('FULLSCREEN', 'yes', 'SETTINGS')
                        elif full == True:
                            screen = pygame.display.set_mode([0, 0], pygame.RESIZABLE)
                            full = False
                            pvar.make('FULLSCREEN', 'no', 'SETTINGS')
                        width = screen.get_width()
                        height = screen.get_height()
def files(mode = 'browse', retmode = 'file', startdir = ''):
    if mode == 'browse':
        lps().new('Files')
    elif mode == 'return' and retmode == 'file':
        lps().new('Files: Select a File.')
    elif mode == 'return' and retmode == 'directory':
        lps().new('Files: Select a Folder.')
    if startdir == '':
        starterdirs = ['res', 'user', 'var', 'temp', 'PythonOS.py', 'constants.py', 'kernel.py', 'commands.py', 'shell.py']
        path = ''
    elif startdir != '':
        starterdirs = os.listdir(startdir)
        path = startdir
    contents = starterdirs
    pages = [[starterdirs]]
    h = 70
    w = 22
    page = 0
    subpage = 0
    history = ['']
    hpos = 0
    while True:
        appsurface.fill([50, 50, 50])
        subpage = 0
        w = 2
        h = 70
        for i in pages[page][subpage]:
            appsurface.blit(font.render(i, 1, (200, 200, 200)), [w, h])
            if h >= height - 50:
                subpage = subpage + 1
                w = w + 300
                h = 70 - 15
            h = h + 15
        appsurface.blit(font.render('Page ' + str(page + 1) + ' of ' + str(len(pages)), 1, [200, 200, 200]), [2, 45])
        sbtn = components().button(2, 2, 'Select File/Folder')
        cbtn = components().button(sbtn[1] + 2, 2, 'Copy')
        mbtn = components().button(cbtn[1] + 2, 2, 'Move')
        dbtn = components().button(mbtn[1] + 2, 2, 'Delete')
        nbtn = components().button(dbtn[1] + 2, 2, 'New Folder')
        gbtn = components().button(nbtn[1] + 2, 2, 'Go To')
        bbtn = components().button(gbtn[1] + 2, 2, ' < Back')
        fbtn = components().button(bbtn[1] + 2, 2, 'Forward >')
        if retmode == 'directory':
            srbtn = components().button(fbtn[1] + 2, 2, 'Use This Folder')
        appsurface.blit(ifont.render(path, 1, (200, 200, 200)), [2, 25])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            lps().end()
                            return None
                    if mousepos[0] >= sbtn[0] and mousepos[0] <= sbtn[1]:
                        if mousepos[1] >= sbtn[2] and mousepos[1] <= sbtn[3]:
                            atp = components().getinput(2, 20, '')
                            if c.fileordir(path + atp) == 'File':
                                if mode == 'browse':
                                    c.openfile(path + atp)
                                if mode == 'return':
				    lps().end()
                                    return path + atp
                            if c.fileordir(path + atp) == 'Directory':
                                path = path + atp
                                if path.endswith('/') == False:
                                    path = path + '/'
                                contents = os.listdir(path)
                                history.append(path)
                                hpos = len(history) - 1
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                    if mousepos[0] >= cbtn[0] and mousepos[0] <= cbtn[1]:
                        if mousepos[1] >= cbtn[2] and mousepos[1] <= cbtn[3]:
                            components().okbox('Select file to copy', ['Select the file to copy from.'])
                            read = files('return', 'file', path)
                            components().okbox('Select destination directory', ['Please select the destination directory.'])
                            new = files('return', 'directory', path) + components().inputbox('Name new file.', ['Name the copy destination file.'])
                            commands.parsecmd(['cp', read, new])
                            components().okbox('File Copied', ['The file has finished copying.'])
                    if mousepos[0] >= mbtn[0] and mousepos[0] <= mbtn[1]:
                        if mousepos[1] >= mbtn[2] and mousepos[1] <= mbtn[3]:
                            components().okbox('Select file to move', ['Select the file to move.'])
                            read = files('return', 'file', path)
                            components().okbox('Select destination directory', ['Please select the destination directory.'])
                            new = files('return', 'directory', path) + components().inputbox('Name new file.', ['Name the move destination file.'])
                            commands.parsecmd(['mv', read, new])
                            components().okbox('File Moved', ['The file has finished moving.'])
                    if mousepos[0] >= dbtn[0] and mousepos[0] <= dbtn[1]:
                        if mousepos[1] >= dbtn[2] and mousepos[1] <= dbtn[3]:
                            sel = components().inputbox('Select the file to delete.', ['Please select the file or folder you wish to delete.'])
                            if c.fileordir(path + sel) == 'File':
                                if components().ynbox('Sure to delete?', ['Are you sure you want to delete the file?', 'The operation cannot be undone.']) == 'Yes':
                                    c.deletefile(path + sel)
                                    components().okbox('File deleted.', ['The file has been deleted.'])
                            if c.fileordir(path + sel) == 'Directory':
                                if components().ynbox('Sure to delete?', ['Are you sure you want to delete the folder?', 'The operation cannot be undone.', 'All the files within this folder will be removed.']) == 'Yes':
                                    for f in os.listdir(path + sel):
                                        c.deletefile(path + sel + f)
                                    os.rmdir(path + sel)
                                    components().okbox('Folder deleted.', ['The folder has been deleted.'])
                            contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if retmode == 'directory':
                        if mousepos[0] >= srbtn[0] and mousepos[0] <= srbtn[1]:
                            if mousepos[1] >= srbtn[2] and mousepos[1] <= srbtn[3]:
                                if mode == 'return':
                                    lps().end()
                                    return path
                    if mousepos[0] >= nbtn[0] and mousepos[0] <= nbtn[1]:
                        if mousepos[1] >= nbtn[2] and mousepos[1] <= nbtn[3]:
                            name = components().inputbox('Name the new directory', ['Enter the name for the new directory.', 'Please end with "/"'])
                            os.mkdir(path + name)
                            contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if mousepos[0] >= gbtn[0] and mousepos[0] <= gbtn[1]:
                        if mousepos[1] >= gbtn[2] and mousepos[1] <= gbtn[3]:
                            path = components().inputbox('Where to?', ['Enter the full path of the destination.', 'Please end with "/"'])
                            if c.fileordir(path) == 'File':
                                c.openfile(path)
                            if c.fileordir(path) == 'Directory':
                                if path.endswith('/') == False:
                                    path = path + '/'
                                contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if mousepos[0] >= bbtn[0] and mousepos[0] <= bbtn[1]:
                        if mousepos[1] >= bbtn[2] and mousepos[1] <= bbtn[3]:
                            if hpos == 1:
                                hpos = hpos - 1
                                path = ''
                                contents = starterdirs
                                pages = [[starterdirs]]
                            if hpos > 1:
                                hpos = hpos - 1
                                path = history[hpos]
                                contents = os.listdir(path)
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                    if mousepos[0] >= fbtn[0] and mousepos[0] <= fbtn[1]:
                        if mousepos[1] >= fbtn[2] and mousepos[1] <= fbtn[3]:
                            if hpos + 1 <= len(history) - 1:
                                hpos = hpos + 1
                                path = history[hpos]
                                contents = os.listdir(path)
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return None
                if event.key == pygame.K_LEFT:
                    if page != 0:
                        page = page - 1
                if event.key == pygame.K_RIGHT:
                    if len(pages) - 1 != page:
                        page = page + 1
                if event.key == pygame.K_RETURN:
                    pygame.mouse.set_pos([20, 40])
                    pygame.event.post(pygame.event.Event(pygame.MOUSEBUTTONDOWN, button = 1))
def pictures(path):
    lps().new('Pictures')
    if path == '':
        path = files('return')
    op = pygame.image.load(path)
    if op.get_width() > width:
        if op.get_height > height - 20:
            op = pygame.transform.smoothscale(op, [width, height])
    iw = op.get_width()
    ih = op.get_height()
    bp = centerobj(iw, ih, appsurface)
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        appsurface.blit(op, bp)
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
		    lps().end()
                    return
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                   if mousepos[1] >= 0 and mousepos[1] <= 20:
		       lps().end()
                       return
def music(mode = 'play', fp = ''):
    global vol
    if fp == '':
        try:
            playing = pvar.read('PLAYING', 'MUSIC')[0]
            stopped = pvar.read('STOPPED', 'MUSIC')[0]
            fp = pvar.read('FILEPATH', 'MUSIC')[0]
        except:
            pvar.make('PLAYING', 'no', 'MUSIC')
            pvar.make('STOPPED', 'yes', 'MUSIC')
            playing = pvar.read('PLAYING', 'MUSIC')[0]
            stopped = pvar.read('STOPPED', 'MUSIC')[0]
        if mode == 'play':
            if playing == 'no' and stopped == 'yes':
                fp = files('return')
                pvar.make('FILEPATH',fp ,'MUSIC')
                fp = pvar.read('FILEPATH', 'MUSIC')[0]
                pygame.mixer.init()
                pygame.mixer.music.load(fp)
    elif fp != '':
        pvar.make('PLAYING', 'no', 'MUSIC')
        pvar.make('STOPPED', 'yes', 'MUSIC')
        playing = pvar.read('PLAYING', 'MUSIC')[0]
        stopped = pvar.read('STOPPED', 'MUSIC')[0]
        if mode == 'play':
            if playing == 'no' and stopped == 'yes':
                pvar.make('FILEPATH',fp ,'MUSIC')
                fp = pvar.read('FILEPATH', 'MUSIC')[0]
                pygame.mixer.init()
                pygame.mixer.music.load(fp)
    pausei = pygame.image.load('res/music/pause.png')
    playi = pygame.image.load('res/music/play.png')
    stopi = pygame.image.load('res/music/stop.png')
    bplayi = pygame.image.load('res/music/bgplay.png')
    lps().new('Music')
    center = centerobj(pausei.get_width(), pausei.get_height(), appsurface)
    while True:
        clock.tick(fps)
        appsurface.fill([255, 250, 205])
        appsurface.blit(ifont.render(fp, 1, (50, 50,50)), [2, 20])
        if playing == 'yes' and stopped == 'no':
            cb = components().imagebutton(center[0], center[1], pausei)
        if playing == 'no' or stopped == 'yes':
            cb = components().imagebutton(center[0], center[1], playi)
        sb = components().imagebutton(center[0] - 202, center[1], stopi)
        bb = components().imagebutton(center[0] + 228, center[1], bplayi)
        vdb = components().button(center[0] - 102, center[1] + 80, 'Volume -')
        vub = components().button(center[0] + 98, center[1] + 80, 'Volume +')
        vtxt = bfont.render('Volume: ' + str(vol * 10), 1, [50, 50, 50])
        appsurface.blit(vtxt, [center[0] - 5, center[1] - 50])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= cb[0] and mousepos[0] <= cb[1]:
                    if mousepos[1] >= cb[2] and mousepos[1] <= cb[3]:
                        if playing == 'yes' and stopped == 'no':
                            pygame.mixer.music.pause()
                            pvar.make('PLAYING', 'no', 'MUSIC')
                            pvar.make('STOPPED', 'no', 'MUSIC')
                            playing = pvar.read('PLAYING', 'MUSIC')[0]
                            stopped = pvar.read('STOPPED', 'MUSIC')[0]
                        elif playing == 'no' and stopped == 'no':
                            pygame.mixer.music.unpause()
                            pvar.make('PLAYING', 'yes', 'MUSIC')
                            pvar.make('STOPPED', 'no', 'MUSIC')
                            playing = pvar.read('PLAYING', 'MUSIC')[0]
                            stopped = pvar.read('STOPPED', 'MUSIC')[0]
                        elif playing == 'no' and stopped == 'yes':
                            pygame.mixer.music.play()
                            pvar.make('PLAYING', 'yes', 'MUSIC')
                            pvar.make('STOPPED', 'no', 'MUSIC')
                            playing = pvar.read('PLAYING', 'MUSIC')[0]
                            stopped = pvar.read('STOPPED', 'MUSIC')[0]
                if mousepos[0] >= sb[0] and mousepos[0] <= sb[1]:
                    if mousepos[1] >= sb[2] and mousepos[1] <= sb[3]:
                        pygame.mixer.music.stop()
                        pvar.make('PLAYING', 'no', 'MUSIC')
                        pvar.make('STOPPED', 'yes', 'MUSIC')
                        playing = pvar.read('PLAYING', 'MUSIC')[0]
                        stopped = pvar.read('STOPPED', 'MUSIC')[0]
                if mousepos[0] >= bb[0] and mousepos[0] <= bb[1]:
                    if mousepos[1] >= bb[2] and mousepos[1] <= bb[3]:
                        lps().end()
                        return
                if mousepos[0] >= vub[0] and mousepos[0] <= vub[1]:
                    if mousepos[1] >= vub[2] and mousepos[1] <= vub[3]:
                        vol = vol + 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= vdb[0] and mousepos[0] <= vdb[1]:
                    if mousepos[1] >= vdb[2] and mousepos[1] <= vdb[3]:
                        vol = vol - 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        pygame.mixer.music.stop()
                        pvar.make('PLAYING', 'no', 'MUSIC')
                        pvar.make('STOPPED', 'yes', 'MUSIC')
                        return
def toolbargui():
    while True:
        clock.tick(fps)
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        bw = width - 35
        clickables = []
        for plg in toolbarplugs:
            ico = pygame.image.load(plg[0])
            bw = bw - ico.get_width()
            bw = bw - 2
            screen.blit(ico, [bw, 0])
            clickables.append([[bw, bw + ico.get_width(), 0, 20], plg[1]])
        mousepos = pygame.mouse.get_pos()
        components().mouse()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                for abutton in clickables:
                    if mousepos[0] >= abutton[0][0] and mousepos[0] <= abutton[0][1]:
                        if mousepos[1] >= abutton[0][2] and mousepos[1] <= abutton[0][3]:
                            if event.button == 1:
                                commands.parsecmd([abutton[1]])
                                return
                            if event.button == 3:
                                toolbarman(abutton[1])
        if mousepos[1] >= 20:
            return
def toolbarman(app):
    global toolbarplugstarters
    global toolbarplugs
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 150, mp[1]])
        rem = components().button(mp[0] - 148, mp[1] + 25, 'Unpin from Toolbar')
        run = components().button(mp[0] - 148, mp[1] + 48, 'Open App')
        new = components().button(mp[0] - 148, mp[1] + 71, 'New Link')
        screen.blit(appsurface, [2, 20])
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        components().menu()
        components().mouse()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                    if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                        toolbarplugs = []
                        tbapps = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
                        pvar.destroy('TOOLBARPLUGINS', 'GLOBAL')
                        pvar.make('TOOLBARPLUGINS', 'clock', 'GLOBAL')
                        for tbapp in tbapps:
                            if tbapp != app and tbapp != 'clock':
                                pvar.append('TOOLBARPLUGINS', tbapp, 'GLOBAL')
                        toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
                        for plg in toolbarplugstarters:
                            rf = c.readfile('res/' + plg + '/toolbar.txt')
                            icon = rf[0]
                            run = rf[1]
                            toolbarplugs.append([icon, run])
                        return
                if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                    if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                        commands.parsecmd([app])
                        return
                if mousepos[0] >= new[0] and mousepos[0] <= new[1]:
                    if mousepos[1] >= new[2] and mousepos[1] <= new[3]:
                        toolbaradd()
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 150 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 150 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 150 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
def toolbaradd(name = None):
    global toolbarplugstarters
    global toolbarplugs
    if name == None:
        if shell.grunning == True:
            name = components().inputbox('Plugin to Add.', ['Enter the executable path of the toolbar plugin.'])
        if shell.grunning == False:
            name = raw_input ('Plugin executable command: ')
    try:
        f = open('res/' + name + '/toolbar.txt', 'r')
        f.close()
    except:
        c.msg('Error. This app does not have a plugin.', 'ERROR')
        return
    pvar.append('TOOLBARPLUGINS', name, 'GLOBAL')
    toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
    toolbarplugs = []
    for plg in toolbarplugstarters:
        rf = c.readfile('res/' + plg + '/toolbar.txt')
        icon = rf[0]
        run = rf[1]
        toolbarplugs.append([icon, run])
def volumeplugin():
    global vol
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    mp = [mp[0] - 50, mp[1]]
    pygame.mixer.init()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 50, mp[1]])
        p = components().button(mp[0] - 48, mp[1] + 2, ' + ')
        m = components().button(mp[0] - 20, mp[1] + 2, ' - ')
        appsurface.blit(largefont.render(str(vol * 10), 1, (50, 50, 50)), [mp[0] - 46, mp[1] + 50])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= p[0] and mousepos[0] <= p[1]:
                    if mousepos[1] >= p[2] and mousepos[1] <= p[3]:
                        vol = vol + 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= m[0] and mousepos[0] <= m[1]:
                    if mousepos[1] >= m[2] and mousepos[1] <= m[3]:
                        vol = vol - 0.1
                        pygame.mixer.music.set_volume(vol)  
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 50 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
def sleep():
    clr = [200, 200, 200]
    while clr[0] != 0:
        clock.tick(fps)
        screen.fill(clr)
        pygame.display.update()
        clr = [clr[0] - 5, clr[1] - 5, clr[2] - 5]
    while True:
        clock.tick(1)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                return
            if event.type == pygame.MOUSEMOTION:
                return
            if event.type == pygame.MOUSEBUTTONDOWN:
                return
def search():
    pygame.image.save(appsurface, 'temp/search.jpg')
    sbg = pygame.image.load('temp/search.jpg')
    lps().new('Search')
    pygame.draw.rect(appsurface, [150, 150, 150], [width - 700, 0, 700, height - 20])
    pygame.display.update()
    q = components().getinput(width - 698, 2, '')
    pygame.draw.rect(appsurface, [150, 150, 150], [width - 700, 0, 700, height - 20])
    pygame.display.update()
    appsurface.blit(font.render(q, 1, (50, 50, 50)), [width - 680, 22])
    pygame.display.update()
    import re
    index = c.readfile('data/system/index.txt')
    ff = []
    ffi = []
    for item in index:
        if c.fileordir(item) == 'Directory':
            if item.endswith(q) == True:
                ff.append(item)
            elif item.endswith(q) == False:
                words = re.split('\W+', item)
                for word in words:
                    if word == q:
                        ff.append(item)
        if c.fileordir(item) == 'File':
            if item.endswith(q) == True:
                ffi.append(item)
            elif item.endswith(q) == False:
                words = re.split('\W+', item)
                for word in words:
                    if word == q:
                        ffi.append(item)
    displaylist = []
    displaylist.append('Folders:')
    if ff == []:
        displaylist.append('No Folders Found.')
    for item in ff:
        displaylist.append(item)
    displaylist.append('')
    displaylist.append('Files:')
    if ffi == []:
        displaylist.append('No Files Found.')
    for item in ffi:
        displaylist.append(item)
    rs = pygame.Surface((700, height - 20))
    rs.fill([150, 150, 150])
    appsurface.blit(rs, [width - 700, 0])
    pygame.display.update()
    pages = [[]]
    h = 22
    page = 0
    for item in displaylist:
        if h <= height - 40:
            pages[page].append(item)
        if h >= height - 40:
            pages.append([])
            page = page + 1
            pages[page].append(item)
            h = 22
        h = h + 15
    page = 0
    while True:
        clock.tick(fps)
        h = 22
        appsurface.blit(sbg, [0, 0])
        rs.fill([150, 150, 150])
        for i in pages[page]:
            rs.blit(font.render(i, 1, (50, 50, 50)), [20, h])
            h = h + 15
        rs.blit(font.render('Page ' + str(page + 1) + ' of ' + str(len(pages)), 1, [50, 50, 50]), [20, 2])
        appsurface.blit(rs, [width - 700, 0])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    mousepos = pygame.mouse.get_pos()
                    mwidth = mousepos[0]
                    mheight = mousepos[1]
                    if mwidth >= 0 and mwidth <= 20:
                        if mheight >= 0 and mheight <= 20:
                            lps().end()
                            return
                    if mwidth >= 0 and mwidth <= width - 700:
                        if mheight >= 20 and mheight <= height:
                            lps().end()
                            return
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return
                if event.key == pygame.K_LEFT:
                    if page != 0:
                        page = page - 1
                if event.key == pygame.K_RIGHT:
                    if len(pages) - 1 != page:
                        page = page + 1
class quickboot():
    def down(self):
        import shell
        if shell.grunning == True:
            rb = components().ynbox('Exit in QuickBoot Mode?', ['Do you wish to exit Python OS in QuickBoot Mode?', 'You will be logged in automatically at the next boot.'])
        if shell.grunning == False:
            uin = raw_input ('Exit in QuickBoot Mode (Y/n)')
            rb = 'No'
            if uin == 'Y' or uin == 'y':
                rb = 'Yes'
        if rb == 'Yes':
            ksf = c.makefile('boot.kstr')
            print >> ksf, shell.user
            ksf.close()
            commands.parsecmd(['killg'])
            return
    def up(self):
        rksf = c.readfile('boot.kstr', 'rU')
        c.deletefile('boot.kstr')
        return rksf[0]

