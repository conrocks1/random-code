import constants
import PythonOS
import shell

variables = [['shver', '1.1']]

def parsecmd(scmd):
    global PythonOS
    if len(scmd) - 1 == -1:
        return
    if scmd[0] == 'install':
        try:
            f = open(shell.cdir + scmd[1], 'r')
            f.close()
            constants.install(shell.cdir + scmd[1])
        except:
            constants.install(scmd[1])
    if scmd[0] == "index":
        constants.msg('Indexing File System.', 'INDEXSERVICE')
        constants.index()
    if scmd[0] == 'fod':
        print constants.fileordir(shell.cdir + scmd[1])
    if scmd[0] == 'compileall':
        for cdir in constants.misc().directories:
            constants.compiledir(cdir)
    if scmd[0] == 'appreqg':
        constants.appreqg(scmd[1])
    if scmd[0] == 'killg':
        PythonOS.pygame.quit()
        shell.grunning = False
    if scmd[0] == 'changescreen':
        constants.changescreen(scmd[1], scmd[2])
    if scmd[0] == 'fullscreen-toggle':
        constants.fst()
    if scmd[0] == 'print':
        toprint = ''
        cpil = 1
        while cpil <= len(scmd) - 1:
            toprint = toprint + scmd[cpil] + ' '
            cpil = cpil + 1
        print toprint
    if scmd[0] == 'pvar-make':
        import pvar
        towrite = ''
        cpil = 2
        while cpil <= len(scmd) - 1:
            towrite = towrite + ' ' + scmd[cpil]
            cpil = cpil + 1
        pvar.make(scmd[1], towrite, 'USERSHELL')
    if scmd[0] == 'pvar-read':
        import pvar
        string = ''
        for ln in pvar.read(scmd[1], 'USERSHELL'):
            string = string + ln
        print string
    if scmd[0] == 'pvar-append':
        import pvar
        pvar.append(scmd[1], scmd[2], 'USERSHELL')
    if scmd[0] == 'pvar-delete':
        import pvar
        pvar.destroy(scmd[1], 'USERSHELL')
    if scmd[0] == 'txtfman':
        try:
            constants.textfman(shell.cdir + scmd[1])
        except:
            constants.textfman(shell.cdir)
    if scmd[0] == 'txtedit':
        constants.txtedit(shell.cdir + scmd[1])
    if scmd[0] == 'cd':
        if scmd[1] != '~':
            if scmd[1].endswith('/') == False:
                scmd[1] = scmd[1] + '/'
            shell.cdir = shell.cdir + scmd[1]
        if scmd[1] == '~':
            shell.cdir = ''
    if scmd[0] == 'rm':
        constants.deletefile(shell.cdir + scmd[1])
    if scmd[0] == 'rmdir':
        import os
        if len(os.listdir(shell.cdir + scmd[1])) - 1 == -1:
            os.rmdir(shell.cdir + scmd[1])
        if len(os.listdir(shell.cdir + scmd[1])) - 1 > -1:
            try:
                if scmd[2] == '-r':
                    import shutil
                    shutil.rmtree(shell.cdir + scmd[1])
            except:
                print 'Directory not empty. Use rmdir path -r to override.'
    if scmd[0] == 'mkdir':
        import os
        os.mkdir(shell.cdir + scmd[1])
    if scmd[0] == 'scan-index':
        constants.securetools().scanall()
    if scmd[0] == 'ls':
        if shell.cdir != '':
            import os
            for itm in os.listdir(shell.cdir):
                print itm
    if scmd[0] == 'wget':
        constants.msg('Downloading ' + scmd[1] + ' to ' + scmd[2], 'WGET')
        import urllib
        urllib.urlretrieve(scmd[1], scmd[2])
    if scmd[0] == 'credits':
        constants.pyoscredits()
    if scmd[0] == 'uninstall':
        if len(scmd) - 1 == 2:
            constants.uninstall(scmd[1], scmd[2])
        elif len(scmd) - 1 != 2:
            constants.uninstall(scmd[1], '-n')
    if scmd[0] == 'startg':
        PythonOS.gserver()
        PythonOS.gui().boot()
    if scmd[0] == 'apps':
        PythonOS.gui().apps()
    if scmd[0] == 'cipher':
        tocipher = raw_input ('String to cipher: ')
        i = int(raw_input ('Integer to cipher by: '))
        fs = ''
        for char in tocipher:
            if char != ' ' and char != '!' and char != '.' and char != ',' and char != '?' and char != "'" and char != ':' and char != ';':
                fs = fs + constants.shiftchar(char, i)
            if char == ' ':
                fs = fs + ' '
            if char == '!':
                fs = fs + '!'
            if char == '.':
                fs = fs + '.'
            if char == ',':
                fs = fs + ','
            if char == '?':
                fs = fs + '?'
            if char == "'":
                fs = fs + "'"
            if char == ':':
                fs = fs + ':'
            if char == ';':
                fs = fs + ';'
        constants.msg(fs, 'RESULT')
    if scmd[0] == 'cp':
        constants.msg('Copying file ...', 'COPY')
        import shutil
        shutil.copy2(shell.cdir + scmd[1], scmd[2])
        constants.msg('Done.', 'COPY')
    if scmd[0] == 'mv':
        constants.msg('Moving file ...', 'MOVE')
        import shutil
        shutil.copy2(shell.cdir + scmd[1], scmd[2])
        constants.deletefile(shell.cdir + scmd[1])
        constants.msg('Done.', 'MOVE')
    if scmd[0] == 'var':
        import pvar
        if scmd[1] == '-m':
            ail = False
            for v in variables:
                if v[0] == scmd[2]:
                    v[1] = scmd[3]
                    ail = True
            if ail == False:
                s = scmd[3]
                cpos = 4
                while cpos <= len(scmd) - 1:
                    s = s + ' ' + scmd[cpos]
                    cpos = cpos + 1
                variables.append([scmd[2], s])
        if scmd[1] == '-p':
            for v in variables:
                if v[0] == scmd[2]:
                    print v[1]
        if scmd[1] == '-r':
            s = ''
            for v in variables:
                if v[0] == scmd[2]:
                    s = v[1]
            pvar.make('VAR', s, 'TRANSFER')
        if scmd[1] == '-rd':
            pvar.destroy('VAR', 'TRANSFER')
        if scmd[1] == '-d':
            cpos = 0
            while cpos <= len(variables) - 1:
                if variables[cpos][0] == scmd[2]:
                    variables.pop(cpos)
                    continue
                cpos = cpos + 1
    if scmd[0] == 'sandbox':
        if len(scmd) - 1 == 0:
            constants.securetools().sandbox()
        elif len(scmd) - 1 > 0:
            constants.securetools().sandbox(scmd[1], scmd[2])
    if scmd[0] == 'file-scanner':
        if len(scmd) - 1 == 0:
            constants.securetools().scanner()
        elif len(scmd) - 1 > 0:
            constants.securetools().scanner(scmd[1])
    if scmd[0] == 'backup':
        constants.makepoint()
    if scmd[0] == 'restore':
        if len(scmd) == 1:
            constants.restore()
        if len(scmd) == 2:
            constants.restore(scmd[1])
    if scmd[0] == 'reload-pyos':
        constants.deletefile('PythonOS.pyc')
        if shell.grunning == True:
            ops0 = PythonOS.lps().listall()
            ops1 = []
            for p in ops0:
                ops1.append(p)
        reload(PythonOS)
        if shell.grunning == True:
            PythonOS.gserver()
            PythonOS.gui().boot(True)
            for process in ops1:
                if process != 'Python OS System':
                    PythonOS.lps().new(process)
        return 'reload'
    if scmd[0] == 'lsapps':
        import pvar
        for app in pvar.read('INSTALLEDAPPS', 'GLOBAL'):
            print 'APP: ' + app
            print 'Name: ' + constants.readfile('res/' + app + '/req.txt')[4]
    if scmd[0] == 'gboot':
        PythonOS.gui().boot()
    if scmd[0] == 'exec':
        exec(scmd[1])
    if scmd[0].endswith('.p'):
        constants.pparse(shell.cdir + scmd[0])
    if scmd[0].endswith('.py') or scmd[0].endswith('.pyw'):
        execfile(shell.cdir + scmd[1])
    if scmd[0] == 'reload-all':
        constants.reload_all()
    if scmd[0] == 'lsdaemons':
        for d in PythonOS.daemonman().retall():
            constants.msg(d, 'DAEMON')
    if scmd[0] == 'daemonman':
        if scmd[1] == '-d':
            PythonOS.daemonman().delete(scmd[2])
        if scmd[1] == '-s':
            PythonOS.daemonman().add(scmd[2])
        if scmd[1] == '-a':
            PythonOS.daemonman().atstart()
    if scmd[0] == "system":
        PythonOS.sysinfo()
    if scmd[0] == "pyexit":
        PythonOS.pyexit()
    if scmd[0] == "settings":
        PythonOS.settings()
    if scmd[0] == 'pman':
        try:
            if scmd[1] == '-i':
                PythonOS.pman().install(scmd[2])
            if scmd[1] == '-cup':
                PythonOS.pman().updateapps()
            if scmd[1] == '-u':
                PythonOS.pman().uninstall(scmd[2])
            if scmd[1] == '-ri':
                PythonOS.pman().getapp(scmd[2])
            if scmd[1] == '-h':
                print 'PMan Help'
                print 'Help File res/pman/help.txt'
                print 'Open with txtedit res/pman/help.txt'
                print 'Getting errors? Update your Python OS to the latest versions.'
                print 'sysupdate'
            if scmd[1] == '-v':
                PythonOS.pman().printver()
            if scmd[1] == '-cat':
                if scmd[2] == '-v':
                    PythonOS.pman().category(scmd[3], 'v')
                if scmd[2] == '-ri':
                    PythonOS.pman().category(scmd[3], 'ri')
                if scmd[2] == '-u':
                    PythonOS.pman().category(scmd[3], 'u')
        except:
            if shell.grunning == True:
                PythonOS.pman().pgui()
            if shell.grunning == False:
                print 'Error'
    if scmd[0] == "files":
        PythonOS.files()
    if scmd[0] == "pictures":
        PythonOS.pictures('')
    if scmd[0] == "music":
        PythonOS.music()
    if scmd[0] == "cleanup":
        PythonOS.cleanup()
    if scmd[0] == "adduser":
        PythonOS.adduser()
    if scmd[0] == "remuser":
        PythonOS.remuser()
    if scmd[0] == "addtoindex":
        constants.addtoindexusr()
    if scmd[0] == "remfromindex":
        constants.removefromindexusr()
    if scmd[0] == "nestedsession":
        PythonOS.nestedsession()
    if scmd[0] == "gunzip":
        PythonOS.gunzip()
    if scmd[0] == "dirlink":
        PythonOS.dirlink()
    if scmd[0] == "sysshell":
        PythonOS.sysshell()
    if scmd[0] == "quickboot":
        PythonOS.quickboot().down()
    if scmd[0] == "runcmd":
        PythonOS.runcmd()
    if scmd[0] == "sleep":
        PythonOS.sleep()
    if scmd[0] == "toolbarman":
        PythonOS.toolbarman(None)
    if scmd[0] == "toolbargui":
        PythonOS.toolbargui()
    if scmd[0] == "search":
        PythonOS.search()
    if scmd[0] == "reload-data":
        PythonOS.reload_data()
    if scmd[0] == "clock":
        PythonOS.clockapp()
    if scmd[0] == "notifyd":
        PythonOS.notifyd().setup()
    if scmd[0] == "sandbox-gui":
        constants.securetools().sandbox()
    if scmd[0] == "calculator":
        PythonOS.calculator()
    if scmd[0] == "root-home":
        PythonOS.files("browse", "file", "user/root/")
    if scmd[0] == "volumeplugin":
        PythonOS.volumeplugin()
    if scmd[0] == "hola-greeter":
        PythonOS.hola_greeter()
    if scmd[0] == "volumeplugin":
        PythonOS.volumeplugin()
    if scmd[0] == "catchergame":
        PythonOS.CatcherGame()
    if scmd[0] == "toolbaradd":
        PythonOS.toolbaradd()
    if scmd[0] == "bgpd":
        PythonOS.bgpd().setup()
    if scmd[0] == "textedit":
        PythonOS.textedit().gui()
    if scmd[0] == "debugd":
        PythonOS.debugd().setup()
