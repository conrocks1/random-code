def toolbarman(app):
    global toolbarplugstarters
    global toolbarplugs
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 150, mp[1]])
        rem = components().button(mp[0] - 148, mp[1] + 25, 'Unpin from Toolbar')
        run = components().button(mp[0] - 148, mp[1] + 48, 'Open App')
        new = components().button(mp[0] - 148, mp[1] + 71, 'New Link')
        screen.blit(appsurface, [2, 20])
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        components().menu()
        components().mouse()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                    if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                        toolbarplugs = []
                        tbapps = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
                        pvar.destroy('TOOLBARPLUGINS', 'GLOBAL')
                        pvar.make('TOOLBARPLUGINS', 'clock', 'GLOBAL')
                        for tbapp in tbapps:
                            if tbapp != app and tbapp != 'clock':
                                pvar.append('TOOLBARPLUGINS', tbapp, 'GLOBAL')
                        toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
                        for plg in toolbarplugstarters:
                            rf = c.readfile('res/' + plg + '/toolbar.txt')
                            icon = rf[0]
                            run = rf[1]
                            toolbarplugs.append([icon, run])
                        return
                if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                    if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                        commands.parsecmd([app])
                        return
                if mousepos[0] >= new[0] and mousepos[0] <= new[1]:
                    if mousepos[1] >= new[2] and mousepos[1] <= new[3]:
                        toolbaradd()
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 150 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 150 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 150 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
