def settings():
    lps().new('Settings')
    global fps
    global bg
    global screen
    global width
    global height
    global full
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        b1 = components().button(20, 20, 'Change FPS of applications')
        b2 = components().button(20, b1[3] + 2, 'Change the Desktop background')
        b3 = components().button(20, b2[3] + 2, 'Change boot desktop at login')
        b4 = components().button(20, b3[3] + 2, 'Toggle Fullscreen')
        screen.blit(appsurface, [2, 20])
	components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.VIDEORESIZE:
                width = event.w
                height = event.h
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        return
                if mousepos[0] >= b1[0] and mousepos[0] <= b1[1]:
                    if mousepos[1] >= b1[2] and mousepos[1] <= b1[3]:
                        pvar.make('FPS', components().inputbox('Set the FPS of applications', ['Enter the Flips Per Second of applications.', 'Default: 10']), 'SETTINGS')
                        fps = int(pvar.read('FPS', 'SETTINGS')[0])
                if mousepos[0] >= b2[0] and mousepos[0] <= b2[1]:
                    if mousepos[1] >= b2[2] and mousepos[1] <= b2[3]:
                        path = files('return')
                        if c.readfile(path, 'r') != None:
                            pvar.make('DESKTOPBG', path, 'SETTINGS')
                            bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
                if mousepos[0] >= b3[0] and mousepos[0] <= b3[1]:
                    if mousepos[1] >= b3[2] and mousepos[1] <= b3[3]:
                        answer = components().ynbox('Start Graphics at login?', ['Do you want to start the Desktop when you log in?'])
                        if answer == 'Yes':
                            pvar.make('RUNG', 'True', 'SETTINGS')
                        if answer == 'No':
                            pvar.make('RUNG', 'False', 'SETTINGS')
                if mousepos[0] >= b4[0] and mousepos[0] <= b4[1]:
                    if mousepos[1] >= b4[2] and mousepos[1] <= b4[3]:
                        if full == False:
                            screen = pygame.display.set_mode([0, 0], pygame.FULLSCREEN | pygame.RESIZABLE | pygame.HWSURFACE)
                            full = True
                            pvar.make('FULLSCREEN', 'yes', 'SETTINGS')
                        elif full == True:
                            screen = pygame.display.set_mode([0, 0], pygame.RESIZABLE)
                            full = False
                            pvar.make('FULLSCREEN', 'no', 'SETTINGS')
                        width = screen.get_width()
                        height = screen.get_height()
