def files(mode = 'browse', retmode = 'file', startdir = ''):
    if mode == 'browse':
        lps().new('Files')
    elif mode == 'return' and retmode == 'file':
        lps().new('Files: Select a File.')
    elif mode == 'return' and retmode == 'directory':
        lps().new('Files: Select a Folder.')
    if startdir == '':
        starterdirs = ['res', 'user', 'var', 'temp', 'PythonOS.py', 'constants.py', 'kernel.py', 'commands.py', 'shell.py']
        path = ''
    elif startdir != '':
        starterdirs = os.listdir(startdir)
        path = startdir
    contents = starterdirs
    pages = [[starterdirs]]
    h = 70
    w = 22
    page = 0
    subpage = 0
    history = ['']
    hpos = 0
    while True:
        appsurface.fill([50, 50, 50])
        subpage = 0
        w = 2
        h = 70
        for i in pages[page][subpage]:
            appsurface.blit(font.render(i, 1, (200, 200, 200)), [w, h])
            if h >= height - 50:
                subpage = subpage + 1
                w = w + 300
                h = 70 - 15
            h = h + 15
        appsurface.blit(font.render('Page ' + str(page + 1) + ' of ' + str(len(pages)), 1, [200, 200, 200]), [2, 45])
        sbtn = components().button(2, 2, 'Select File/Folder')
        cbtn = components().button(sbtn[1] + 2, 2, 'Copy')
        mbtn = components().button(cbtn[1] + 2, 2, 'Move')
        dbtn = components().button(mbtn[1] + 2, 2, 'Delete')
        nbtn = components().button(dbtn[1] + 2, 2, 'New Folder')
        gbtn = components().button(nbtn[1] + 2, 2, 'Go To')
        bbtn = components().button(gbtn[1] + 2, 2, ' < Back')
        fbtn = components().button(bbtn[1] + 2, 2, 'Forward >')
        if retmode == 'directory':
            srbtn = components().button(fbtn[1] + 2, 2, 'Use This Folder')
        appsurface.blit(ifont.render(path, 1, (200, 200, 200)), [2, 25])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            lps().end()
                            return None
                    if mousepos[0] >= sbtn[0] and mousepos[0] <= sbtn[1]:
                        if mousepos[1] >= sbtn[2] and mousepos[1] <= sbtn[3]:
                            atp = components().getinput(2, 20, '')
                            if c.fileordir(path + atp) == 'File':
                                if mode == 'browse':
                                    c.openfile(path + atp)
                                if mode == 'return':
				    lps().end()
                                    return path + atp
                            if c.fileordir(path + atp) == 'Directory':
                                path = path + atp
                                if path.endswith('/') == False:
                                    path = path + '/'
                                contents = os.listdir(path)
                                history.append(path)
                                hpos = len(history) - 1
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                    if mousepos[0] >= cbtn[0] and mousepos[0] <= cbtn[1]:
                        if mousepos[1] >= cbtn[2] and mousepos[1] <= cbtn[3]:
                            components().okbox('Select file to copy', ['Select the file to copy from.'])
                            read = files('return', 'file', path)
                            components().okbox('Select destination directory', ['Please select the destination directory.'])
                            new = files('return', 'directory', path) + components().inputbox('Name new file.', ['Name the copy destination file.'])
                            commands.parsecmd(['cp', read, new])
                            components().okbox('File Copied', ['The file has finished copying.'])
                    if mousepos[0] >= mbtn[0] and mousepos[0] <= mbtn[1]:
                        if mousepos[1] >= mbtn[2] and mousepos[1] <= mbtn[3]:
                            components().okbox('Select file to move', ['Select the file to move.'])
                            read = files('return', 'file', path)
                            components().okbox('Select destination directory', ['Please select the destination directory.'])
                            new = files('return', 'directory', path) + components().inputbox('Name new file.', ['Name the move destination file.'])
                            commands.parsecmd(['mv', read, new])
                            components().okbox('File Moved', ['The file has finished moving.'])
                    if mousepos[0] >= dbtn[0] and mousepos[0] <= dbtn[1]:
                        if mousepos[1] >= dbtn[2] and mousepos[1] <= dbtn[3]:
                            sel = components().inputbox('Select the file to delete.', ['Please select the file or folder you wish to delete.'])
                            if c.fileordir(path + sel) == 'File':
                                if components().ynbox('Sure to delete?', ['Are you sure you want to delete the file?', 'The operation cannot be undone.']) == 'Yes':
                                    c.deletefile(path + sel)
                                    components().okbox('File deleted.', ['The file has been deleted.'])
                            if c.fileordir(path + sel) == 'Directory':
                                if components().ynbox('Sure to delete?', ['Are you sure you want to delete the folder?', 'The operation cannot be undone.', 'All the files within this folder will be removed.']) == 'Yes':
                                    for f in os.listdir(path + sel):
                                        c.deletefile(path + sel + f)
                                    os.rmdir(path + sel)
                                    components().okbox('Folder deleted.', ['The folder has been deleted.'])
                            contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if retmode == 'directory':
                        if mousepos[0] >= srbtn[0] and mousepos[0] <= srbtn[1]:
                            if mousepos[1] >= srbtn[2] and mousepos[1] <= srbtn[3]:
                                if mode == 'return':
                                    lps().end()
                                    return path
                    if mousepos[0] >= nbtn[0] and mousepos[0] <= nbtn[1]:
                        if mousepos[1] >= nbtn[2] and mousepos[1] <= nbtn[3]:
                            name = components().inputbox('Name the new directory', ['Enter the name for the new directory.', 'Please end with "/"'])
                            os.mkdir(path + name)
                            contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if mousepos[0] >= gbtn[0] and mousepos[0] <= gbtn[1]:
                        if mousepos[1] >= gbtn[2] and mousepos[1] <= gbtn[3]:
                            path = components().inputbox('Where to?', ['Enter the full path of the destination.', 'Please end with "/"'])
                            if c.fileordir(path) == 'File':
                                c.openfile(path)
                            if c.fileordir(path) == 'Directory':
                                if path.endswith('/') == False:
                                    path = path + '/'
                                contents = os.listdir(path)
                            history.append(path)
                            hpos = len(history) - 1
                            pages = [[[]]]
                            h = 70
                            w = 22
                            page = 0
                            subpage = 0
                            for item in contents:
                                if h < height - 20:
                                    pages[page][subpage].append(item)
                                elif h >= height - 20 and w < width - 300:
                                    h = 70
                                    w = w + 300
                                    subpage = subpage + 1
                                    pages[page].append([])
                                    pages[page][subpage].append(item)
                                elif w >= width - 300:
                                    pages.append([[]])
                                    page = page + 1
                                    subpage = 0
                                    pages[page][subpage].append(item)
                                    h = 70
                                    w = 22
                                h = h + 15
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                    if mousepos[0] >= bbtn[0] and mousepos[0] <= bbtn[1]:
                        if mousepos[1] >= bbtn[2] and mousepos[1] <= bbtn[3]:
                            if hpos == 1:
                                hpos = hpos - 1
                                path = ''
                                contents = starterdirs
                                pages = [[starterdirs]]
                            if hpos > 1:
                                hpos = hpos - 1
                                path = history[hpos]
                                contents = os.listdir(path)
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
                    if mousepos[0] >= fbtn[0] and mousepos[0] <= fbtn[1]:
                        if mousepos[1] >= fbtn[2] and mousepos[1] <= fbtn[3]:
                            if hpos + 1 <= len(history) - 1:
                                hpos = hpos + 1
                                path = history[hpos]
                                contents = os.listdir(path)
                                pages = [[[]]]
                                h = 70
                                w = 22
                                page = 0
                                subpage = 0
                                for item in contents:
                                    if h < height - 20:
                                        pages[page][subpage].append(item)
                                    elif h >= height - 20 and w < width - 300:
                                        h = 70
                                        w = w + 300
                                        subpage = subpage + 1
                                        pages[page].append([])
                                        pages[page][subpage].append(item)
                                    elif w >= width - 300:
                                        pages.append([[]])
                                        page = page + 1
                                        subpage = 0
                                        pages[page][subpage].append(item)
                                        h = 70
                                        w = 22
                                    h = h + 15
                                    h = 70
                                    w = 22
                                    page = 0
                                    subpage = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return None
                if event.key == pygame.K_LEFT:
                    if page != 0:
                        page = page - 1
                if event.key == pygame.K_RIGHT:
                    if len(pages) - 1 != page:
                        page = page + 1
                if event.key == pygame.K_RETURN:
                    pygame.mouse.set_pos([20, 40])
                    pygame.event.post(pygame.event.Event(pygame.MOUSEBUTTONDOWN, button = 1))
