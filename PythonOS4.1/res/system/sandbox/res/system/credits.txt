﻿PPPPPP Y    Y TTTTTT H    H  OOOO  N    N       OOOO   SSSSS 
P    P  Y  Y    TT   H    H O    O NN   N      O    O S     
PPPPPP   YY     TT   HHHHHH O    O N N  N      O    O SSSSS
P        YY     TT   H    H O    O N  N N      O    O      S
P        YY     TT   H    H  OOOO  N   NN       OOOO  SSSSS

Python OS 4.0.0 

CODE:		Adam Furman
GRAPHICS:	Adam Furman
ADVISOR:	Samandip Saini

Python OS is an operating system written in Python.

Python OS 4 uses these fine technologies:
	
	Python 2x: http://www.python.org  Distributed under the Python License
	Pygame:	   http://www.pygame.org  Published under the GPL License

Python OS can be downloaded at: http://adamfurman.privatepage.sk/PythonOS
Hosting provided by: ExoHosting © 2002-2012 EXO TECHNOLOGIES, spol. s r.o. All rights reserved

You may modify, redistribute, and run Python OS 4 for free. If you redistribute Python OS, please include this credits file intact and unchanged. The author (Adam Furman) may not be held accountable for any damage to your computer, operating system, or files. By running Python OS, you waive the right to sue the author for damages. If you experience problems with Python OS, contact pyoshelp@gmail.com indicating where the error occurred, your operating system, and any Python Traceback. For user instructions, see the documentation under res/system/documentation/ or the help.txt file in individual apps.

Thank you for choosing Python OS 4.
