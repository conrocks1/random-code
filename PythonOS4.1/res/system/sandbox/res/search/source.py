def search():
    pygame.image.save(appsurface, 'temp/search.jpg')
    sbg = pygame.image.load('temp/search.jpg')
    lps().new('Search')
    pygame.draw.rect(appsurface, [150, 150, 150], [width - 700, 0, 700, height - 20])
    pygame.display.update()
    q = components().getinput(width - 698, 2, '')
    pygame.draw.rect(appsurface, [150, 150, 150], [width - 700, 0, 700, height - 20])
    pygame.display.update()
    appsurface.blit(font.render(q, 1, (50, 50, 50)), [width - 680, 22])
    pygame.display.update()
    import re
    index = c.readfile('data/system/index.txt')
    ff = []
    ffi = []
    for item in index:
        if c.fileordir(item) == 'Directory':
            if item.endswith(q) == True:
                ff.append(item)
            elif item.endswith(q) == False:
                words = re.split('\W+', item)
                for word in words:
                    if word == q:
                        ff.append(item)
        if c.fileordir(item) == 'File':
            if item.endswith(q) == True:
                ffi.append(item)
            elif item.endswith(q) == False:
                words = re.split('\W+', item)
                for word in words:
                    if word == q:
                        ffi.append(item)
    displaylist = []
    displaylist.append('Folders:')
    if ff == []:
        displaylist.append('No Folders Found.')
    for item in ff:
        displaylist.append(item)
    displaylist.append('')
    displaylist.append('Files:')
    if ffi == []:
        displaylist.append('No Files Found.')
    for item in ffi:
        displaylist.append(item)
    rs = pygame.Surface((700, height - 20))
    rs.fill([150, 150, 150])
    appsurface.blit(rs, [width - 700, 0])
    pygame.display.update()
    pages = [[]]
    h = 22
    page = 0
    for item in displaylist:
        if h <= height - 40:
            pages[page].append(item)
        if h >= height - 40:
            pages.append([])
            page = page + 1
            pages[page].append(item)
            h = 22
        h = h + 15
    page = 0
    while True:
        clock.tick(fps)
        h = 22
        appsurface.blit(sbg, [0, 0])
        rs.fill([150, 150, 150])
        for i in pages[page]:
            rs.blit(font.render(i, 1, (50, 50, 50)), [20, h])
            h = h + 15
        rs.blit(font.render('Page ' + str(page + 1) + ' of ' + str(len(pages)), 1, [50, 50, 50]), [20, 2])
        appsurface.blit(rs, [width - 700, 0])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 1:
                    mousepos = pygame.mouse.get_pos()
                    mwidth = mousepos[0]
                    mheight = mousepos[1]
                    if mwidth >= 0 and mwidth <= 20:
                        if mheight >= 0 and mheight <= 20:
                            lps().end()
                            return
                    if mwidth >= 0 and mwidth <= width - 700:
                        if mheight >= 20 and mheight <= height:
                            lps().end()
                            return
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return
                if event.key == pygame.K_LEFT:
                    if page != 0:
                        page = page - 1
                if event.key == pygame.K_RIGHT:
                    if len(pages) - 1 != page:
                        page = page + 1
