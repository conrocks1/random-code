import constants as c
import pvar
import commands
try:
    import pygame
except:
    c.msg('No Pygame.', 'ERROR')
import datetime
import os
import random
import time
import shell
appsurface = None
screen = None
toolbarplugs = []

def gserver():
    c.msg('Gserver v1.1', 'GSERVER')
    import shell
    global width
    global height
    global font
    global largefont
    global smallfont
    global bfont
    global largebfont
    global smallbfont
    global ifont
    global largeifont
    global smallifont
    global clock
    global screen
    global appsurface
    global full
    global ctheme
    ctheme = None
    global processes
    processes = ['Python OS System']
    global daemons
    daemons = []
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pygame.init()
    pygame.mixer.init()
    pygame.display.set_caption('Python OS')
    pygame.display.set_icon(pygame.image.load('res/system/graphics/icons/windowicon.png'))
    try:
        width = pygame.display.Info().current_w
        height = pygame.display.Info().current_h
    except:
        width = 1366
        height = 768
    c.logentry('GSERVER', 'Width: ' + str(width) + ' Height: ' + str(height))
    c.logentry('GSERVER', 'Display Driver: ' + str(pygame.display.get_driver()))
    pfull = pvar.read('FULLSCREEN', 'SETTINGS')[0]
    if pfull != 'yes':
        screen = pygame.display.set_mode([width, height], pygame.RESIZABLE)
        full = False
    if pfull ==  'yes':
        screen = pygame.display.set_mode([width, height], pygame.RESIZABLE | pygame.HWSURFACE | pygame.FULLSCREEN)
        full = True
    appsurface = pygame.Surface([width, height - 20])
    font = pygame.font.Font('res/system/Ubuntu-R.ttf', 14)
    largefont = pygame.font.Font('res/system/Ubuntu-R.ttf', 40)
    smallfont = pygame.font.Font('res/system/Ubuntu-R.ttf', 8)
    bfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 14)
    largebfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 40)
    smallbfont = pygame.font.Font('res/system/Ubuntu-B.ttf', 8)
    ifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 14)
    largeifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 40)
    smallifont = pygame.font.Font('res/system/Ubuntu-RI.ttf', 8)
    clock = pygame.time.Clock()
    shell.grunning = True

def centerobj(ow, oh, surface):
    s1 = ow / 2
    s2 = oh / 2
    rbw = (surface.get_width() / 2) - s1
    rbh = (surface.get_height() / 2) - s2
    return [rbw, rbh]

class components():
    def button(self, x, y, text):
        ftxt = font.render(text, 1, [25, 25, 25])
        button = pygame.transform.smoothscale(buttonimg, (ftxt.get_width() + 8, ftxt.get_height() + 4))
        appsurface.blit(button, [x, y])
        center = centerobj(button.get_width(), button.get_height(), button)
        appsurface.blit(ftxt, [x + center[0], y + center[1]])
        tret = [x + 2, x + 2 + button.get_width(), y + 20, y + 20 + button.get_height()]
        return tret
    def toolbar(self):
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        bw = width - 35
        for plug in toolbarplugs:
            ico = pygame.image.load(plug[0])
            bw = (bw - 2) - ico.get_width()
            screen.blit(ico, [bw, 0])
        mp = pygame.mouse.get_pos()
        if mp[1] <= 20 and mp[0] >= bw - 10:
            toolbargui()
        daemonman().run()
        components().mouse()
    def mouse(self):
        if ctheme == 'darktheme':
            color = [100, 100, 100]
        if ctheme == 'lighttheme':
            color = [50, 50, 50]
        if ctheme == 'blacktheme':
            color = [200, 200, 200]
        if ctheme == 'whitetheme':
            color = [50, 50, 50]
        pygame.draw.circle(screen, color, pygame.mouse.get_pos(), 3)
    def hangscreen(self, title, content):
        screen.fill([200, 200, 200])
        screen.blit(largefont.render('Please Wait... Python OS is working.', 1, (0, 100, 20)), [20, 20])
        screen.blit(bfont.render(title, 1, (50, 50, 50)), [20, 60])
        bh = 100
        for i in content:
            i2 = font.render(i, 1, (50, 50, 50))
            screen.blit(i2, [20, bh])
            bh = bh + 16
        screen.blit(ifont.render('Python OS may appear to freeze during this process.', 1, (50, 50, 50)), [20, 2])
        pygame.display.update()
    def okbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/okbox-bg.jpg')
        obg = pygame.image.load('temp/okbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        dp = centerobj(400, 300, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'OK')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        lps().end()
                        return 'OK'
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return 'OK'
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def inputbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/inputbox-bg.jpg')
        obg = pygame.image.load('temp/inputbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        dp = centerobj(400, 300, screen)
        reply = ''
        txt = 'Enter text here.'
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            if reply != '':
                ib = components().inputbutton(dp[0] + centerobj(300, 250, dialogimg)[0], dp[1] + 200, reply)
            if reply == '':
                ib = components().inputbutton(dp[0] + centerobj(300, 250, dialogimg)[0], dp[1] + 200, txt)
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'OK')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        lps().end()
                        return reply
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                    if event.key == pygame.K_DELETE:
                        reply = ''
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return reply
                        if mousepos[0] >= ib[0] and mousepos[0] <= ib[1]:
                            if mousepos[1] >= ib[2] and mousepos[1] <= ib[3]:
                                reply = components().getinput(ib[0] - 2, ib[2] - 20, reply)
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def ynbox(self, title, content):
        lps().new(title)
        obg = pygame.image.save(appsurface, 'temp/ynbox-bg.jpg')
        obg = pygame.image.load('temp/ynbox-bg.jpg')
        title = bfont.render(title, 1, (200, 200, 200))
        citem = 0
        selected = 1
        dp = centerobj(400, 300, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(obg, [0, 0])
            appsurface.blit(dialogimg, dp)
            appsurface.blit(title, [dp[0] + 10, dp[1] + 5])
            bh = 50
            for i in content:
                i2 = font.render(i, 1, (200, 200, 200))
                appsurface.blit(i2, [dp[0] + 10, dp[1] + bh])
                bh = bh + 16
            bp0 = components().button(dp[0] + 10, dp[1] + 255, 'Yes')
            bp1 = components().button(dp[0] + 60, dp[1] + 255, 'No')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        if selected == 0:
                            lps().end()
                            return 'No'
                        if selected == 1:
                            lps().end()
                            return 'Yes'
                    if event.key == pygame.K_ESCAPE:
                        lps().end()
                        return 'Cancel'
                    if event.key == pygame.K_RIGHT:
                        if selected == 1:
                            selected = 0
                    if event.key == pygame.K_LEFT:
                        if selected == 0:
                            selected = 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= bp0[0] and mousepos[0] <= bp0[1]:
                            if mousepos[1] >= bp0[2] and mousepos[1] <= bp0[3]:
                                lps().end()
                                return 'Yes'
                        if mousepos[0] >= bp1[0] and mousepos[0] <= bp1[1]:
                            if mousepos[1] >= bp1[2] and mousepos[1] <= bp1[3]:
                                lps().end()
                                return 'No'
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                lps().end()
                                return 'Cancel'
    def menu(self):
        if pygame.mouse.get_pos()[0] <= 2 and pygame.mouse.get_pos()[1] > 21:
            mb = pygame.image.save(appsurface, 'temp/sidemenu-bg.jpg')
            mb = pygame.image.load('temp/sidemenu-bg.jpg')
            clickables = []
            cp = 21
            for mitem in sidemenufinalitems:
                clickables.append([[1, 49, cp, cp + 49], mitem[2]])
                cp = cp + 49
            while True:
                clock.tick(fps)
                screen.blit(pygame.transform.smoothscale(sidemenuimg, [50, height - 20]), [0, 20])
                screen.blit(mb, [50, 20])
                cp = 21
                for mitem in sidemenufinalitems:
                    screen.blit(pygame.image.load(mitem[1]), [1, cp])
                    cp = cp + 49
                components().toolbar()
                mousepos = pygame.mouse.get_pos()
                if mousepos[0] >= 80:
                    pygame.draw.rect(screen, [50, 50, 50], [0, 20, 2, height - 20])
                    pygame.display.update()
                    return
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        cc = 0
                        while cc <= len(clickables) - 1:
                            obj = clickables[cc]
                            if mousepos[0] >= obj[0][0] and mousepos[0] <= obj[0][1]:
                                if mousepos[1] >= obj[0][2] and mousepos[1] <= obj[0][3]:
                                    if event.button == 1:
                                        screen.fill([50, 50, 50])
                                        pygame.display.update()
                                        commands.parsecmd([obj[1]])
                                        screen.fill([50, 50, 50])
                                        pygame.display.update()
                                        return
                                    if event.button == 3:
                                        gui().rcm_sidemenu(obj[1])
                            cc = cc + 1
                        if mousepos[0] >= 50 and mousepos[0] <= width:
                            if mousepos[1] >= 20 and mousepos[1] <= height:
                                if event.button == 1:
                                    screen.fill([50, 50, 50])
                                    pygame.display.update()
                                    return
    def inputbutton(self, x, y, text):
        ftxt = font.render(text, 1, [25, 25, 25])
        button = pygame.transform.smoothscale(inputboximg, (ftxt.get_width() + 26, ftxt.get_height() + 2))
        appsurface.blit(button, [x, y])
        center = centerobj(button.get_width(), button.get_height(), button)
        appsurface.blit(ftxt, [x + center[0], y + center[1]])
        tret = [x + 2, x + 2 + button.get_width(), y + 20, y + 20 + button.get_height()]
        return tret
    def imagebutton(self, x, y, image, targetsurf=appsurface):
        global appsurface
        if targetsurf == None:
            appsurface.blit(image, [x, y])
            tret = [x + 2, x + 2 + image.get_width(), y + 20, y + 20 + image.get_height()]
        elif targetsurf != None:
            targetsurf.blit(image, [x, y])
            tret = [x, x + image.get_width(), y, y + image.get_height()]
        return tret
    def getinput(self, wpix, hpix, suin, odr = False):
        global copied
        uinput = []
        uin = suin
        uinput = []
        for i in suin:
            uinput.append(i)
        shift = False
        done = False
        pygame.image.save(appsurface, 'temp/getinput.jpg')
        gbg = pygame.image.load('temp/getinput.jpg')
        color = [50, 50, 50]
        ibi = inputboximg
        while done != True:
            clock.tick(10)
            appsurface.blit(gbg, [0, 0])
            text = font.render(str(uin), 1, color)
            if text.get_width() >= ibi.get_width():
                ibi = pygame.transform.smoothscale(ibi, [text.get_width() + 2, ibi.get_height()])
            appsurface.blit(ibi, [wpix, hpix])
            appsurface.blit(text, [wpix + 2, hpix + 2])
            screen.blit(appsurface, [2, 20])
            components().menu()
            components().toolbar()
            pygame.display.update()
            mousepos = pygame.mouse.get_pos()
            for event in pygame.event.get():
                if event.type == pygame.VIDEORESIZE:
                    width = event.w
                    height = event.h
                    gbg = pygame.transform.smoothscale(gbg, (width, height))
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] < wpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                    elif mousepos[0] > text.get_width() + 2:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                    elif mousepos[0] < hpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                    elif mousepos[0] > hpix:
                        uin = ''
                        for i in uinput:
                            uin = uin + i
                        c.varman().make('input-exitmode', 'click')
                        return uin
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        done = True
                    if event.key == pygame.K_RETURN:
                        done = True
                    if event.key == pygame.K_a:
                        if shift == False:
                            uinput.append('a')
                        if shift == True:
                            uinput.append('A')
                            shift = False
                    if event.key == pygame.K_b:
                        if shift == False:
                            uinput.append('b')
                        if shift == True:
                            uinput.append('B')
                            shift = False
                    if event.key == pygame.K_c:
                        if shift == False:
                            uinput.append('c')
                        if shift == True:
                            uinput.append('C')
                            shift = False
                    if event.key == pygame.K_d:
                        if shift == False:
                            uinput.append('d')
                        if shift == True:
                            uinput.append('D')
                            shift = False
                    if event.key == pygame.K_e:
                        if shift == False:
                            uinput.append('e')
                        if shift == True:
                            uinput.append('E')
                            shift = False
                    if event.key == pygame.K_f:
                        if shift == False:
                            uinput.append('f')
                        if shift == True:
                            uinput.append('F')
                            shift = False
                    if event.key == pygame.K_g:
                        if shift == False:
                            uinput.append('g')
                        if shift == True:
                            uinput.append('G')
                            shift = False
                    if event.key == pygame.K_h:
                        if shift == False:
                            uinput.append('h')
                        if shift == True:
                            uinput.append('H')
                            shift = False
                    if event.key == pygame.K_i:
                        if shift == False:
                            uinput.append('i')
                        if shift == True:
                            uinput.append('I')
                            shift = False
                    if event.key == pygame.K_j:
                        if shift == False:
                            uinput.append('j')
                        if shift == True:
                            uinput.append('J')
                            shift = False
                    if event.key == pygame.K_k:
                        if shift == False:
                            uinput.append('k')
                        if shift == True:
                            uinput.append('K')
                            shift = False
                    if event.key == pygame.K_l:
                        if shift == False:
                            uinput.append('l')
                        if shift == True:
                            uinput.append('L')
                            shift = False
                    if event.key == pygame.K_m:
                        if shift == False:
                            uinput.append('m')
                        if shift == True:
                            uinput.append('M')
                            shift = False
                    if event.key == pygame.K_n:
                        if shift == False:
                            uinput.append('n')
                        if shift == True:
                            uinput.append('N')
                            shift = False
                    if event.key == pygame.K_o:
                        if shift == False:
                            uinput.append('o')
                        if shift == True:
                            uinput.append('O')
                            shift = False
                    if event.key == pygame.K_p:
                        if shift == False:
                            uinput.append('p')
                        if shift == True:
                            uinput.append('P')
                            shift = False
                    if event.key == pygame.K_q:
                        if shift == False:
                            uinput.append('q')
                        if shift == True:
                            uinput.append('Q')
                            shift = False
                    if event.key == pygame.K_r:
                        if shift == False:
                            uinput.append('r')
                        if shift == True:
                            uinput.append('R')
                            shift = False
                    if event.key == pygame.K_s:
                        if shift == False:
                            uinput.append('s')
                        if shift == True:
                            uinput.append('S')
                            shift = False
                    if event.key == pygame.K_t:
                        if shift == False:
                            uinput.append('t')
                        if shift == True:
                            uinput.append('T')
                            shift = False
                    if event.key == pygame.K_u:
                        if shift == False:
                            uinput.append('u')
                        if shift == True:
                            uinput.append('U')
                            shift = False
                    if event.key == pygame.K_v:
                        if shift == False:
                            uinput.append('v')
                        if shift == True:
                            uinput.append('V')
                            shift = False
                    if event.key == pygame.K_w:
                        if shift == False:
                            uinput.append('w')
                        if shift == True:
                            uinput.append('W')
                            shift = False
                    if event.key == pygame.K_x:
                        if shift == False:
                            uinput.append('x')
                        if shift == True:
                            uinput.append('X')
                            shift = False
                    if event.key == pygame.K_y:
                        if shift == False:
                            uinput.append('y')
                        if shift == True:
                            uinput.append('Y')
                            shift = False
                    if event.key == pygame.K_z:
                        if shift == False:
                            uinput.append('z')
                        if shift == True:
                            uinput.append('Z')
                            shift = False
                    if event.key == pygame.K_LSHIFT or event.key == pygame.K_RSHIFT:
                        shift = True
                    if event.key == pygame.K_INSERT:
                        if shift == False:
                            if copied == '':
                                copied = uin
                            elif copied != '':
                                uinput.append(copied)
                        if shift == True:
                            copied = ''
                    if event.key == pygame.K_SPACE:
                        uinput.append(' ')
                    if event.key == pygame.K_DELETE:
                        uinput = []
                        uin = ''
                        c.varman().make('input-specialchar', 'delete')
                        if odr == True:
                            done = True
                    if event.key == pygame.K_TAB:
                        uinput.append(' ')
                    if event.key == pygame.K_1:
                        if shift == True:
                            uinput.append('!')
                            shift = False
                        elif shift == False:
                            uinput.append('1')
                    if event.key == pygame.K_2:
                        if shift == True:
                            uinput.append('@')
                            shift = False
                        elif shift == False:
                            uinput.append('2')
                    if event.key == pygame.K_3:
                        if shift == True:
                            uinput.append('#')
                            shift = False
                        elif shift == False:
                            uinput.append('3')
                    if event.key == pygame.K_4:
                        if shift == True:
                            uinput.append('$')
                            shift = False
                        elif shift == False:
                            uinput.append('4')
                    if event.key == pygame.K_5:
                        if shift == True:
                            uinput.append('%')
                            shift = False
                        elif shift == False:
                            uinput.append('5')
                    if event.key == pygame.K_6:
                        if shift == True:
                            uinput.append('^')
                            shift = False
                        elif shift == False:
                            uinput.append('6')
                    if event.key == pygame.K_7:
                        if shift == True:
                            uinput.append('&')
                            shift = False
                        elif shift == False:
                            uinput.append('7')
                    if event.key == pygame.K_8:
                        if shift == True:
                            uinput.append('*')
                            shift = False
                        elif shift == False:
                            uinput.append('8')
                    if event.key == pygame.K_9:
                        if shift == True:
                            uinput.append('(')
                            shift = False
                        elif shift == False:
                            uinput.append('9')
                    if event.key == pygame.K_0:
                        if shift == True:
                            uinput.append(')')
                            shift = False
                        elif shift == False:
                            uinput.append('0')
                    if event.key == pygame.K_PERIOD:
                        if shift == False:
                            uinput.append('.')
                        elif shift == True:
                            uinput.append('>')
                            shift = False
                    if event.key == pygame.K_COMMA:
                        if shift == False:
                            uinput.append(',')
                        elif shift == True:
                            uinput.append('<')
                            shift = False
                    if event.key == pygame.K_SLASH:
                        if shift == False:
                            uinput.append('/')
                        elif shift == True:
                            uinput.append('?')
                            shift = False
                    if event.key == pygame.K_MINUS:
                        if shift == False:
                            uinput.append('-')
                        elif shift == True:
                            uinput.append('_')
                            shift = False
                    if event.key == pygame.K_SEMICOLON:
                        if shift == False:
                            uinput.append(';')
                        elif shift == True:
                            uinput.append(':')
                            shift = False
                    if event.key == pygame.K_BACKSPACE:
                        if len(uinput) >= 1:
                            uinput.pop()
                    if event.key == ord("'"):
                        if shift == False:
                            uinput.append("'")
                        elif shift == True:
                            uinput.append('"')
                            shift = False
            uin = ''
            for i in uinput:
                uin = uin + i
        uin = ''
        for i in uinput:
            uin = uin + i
        c.varman().make('input-exitmode', 'enter')
        return uin
class lps():
    def new(self, name):
        processes.append(name)
        c.logentry('lps', 'Application ' + name + ' started')
        return name
    def end(self):
        c.logentry('lps', 'Application ' + processes[len(processes) - 1] + ' ended')
        return processes.pop()
    def listall(self):
        return processes
        
class gui():
    def boot(self, nod = False):
        try:
            c.msg('Booting Python OS GUI', 'POSGUI')
            c.logentry('GUI', 'Starting Python OS GUI load.')
            global ctheme
            ctheme = 'darktheme'
            global processes
            processes = ['Python OS System']
            global mousepos
            global vol
            global full
            global copied
            copied = ''
            full = False
            vol = 0.5
            mousepos = centerobj(4, 4, screen)
            pygame.mouse.set_pos(mousepos)
            pygame.mouse.set_visible(False)
            pygame.key.set_repeat(50, 50)
            global sidemenufinalitems
            global sidemenuitems
            global installedapps
            global installedappsfinal
            installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
            sidemenufinalitems = []
            for i in sidemenuitems:
                rf = c.readfile('res/' + i + '/req.txt', 'rU')
                icon = rf[3]
                name = rf[4]
                execp = rf[2]
                try:
                    pygame.image.load(icon)
                except:
                    icon = 'res/system/graphics/icons/noicon.png'
                sidemenufinalitems.append([name, icon, execp])
            installedappsfinal = []
            for inst in installedapps:
                rf = c.readfile('res/' + inst + '/req.txt', 'rU')
                icon = rf[3]
                name = rf[4]
                execp = rf[2]
                if rf[3].startswith('noicon') == False:
                    try:
                        pygame.image.load(icon)
                    except:
                        icon = 'res/system/graphics/icons/noicon.png'
                    installedappsfinal.append([name, icon, execp])
            global toolbarplugstarters
            toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
            for plg in toolbarplugstarters:
                rf = c.readfile('res/' + plg + '/toolbar.txt')
                icon = rf[0]
                run = rf[1]
                try:
                    pygame.image.load(icon)
                except:
                    icon = 'res/system/graphics/icons/noicon.png'
                toolbarplugs.append([icon, run])
            global pythonpoweredl
            global pythonpowereds
            global bg
            global buttonimg
            global toolbarimg
            global dialogimg
            global sidemenuimg
            global closeicon
            global rightclickmenuimg
            global inputboximg
            inputboximg = pygame.image.load('res/system/graphics/input.png')
            rightclickmenuimg = pygame.image.load('res/system/graphics/right-click-menu.png')
            closeicon = pygame.image.load('res/system/graphics/icons/close.png')
            sidemenuimg = pygame.image.load('res/system/graphics/menu.png')
            pythonpoweredl = pygame.image.load('res/system/python-powered-l.png')
            pythonpowereds = pygame.image.load('res/system/python-powered-s.png')
            bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
            buttonimg = pygame.image.load('res/system/graphics/button.png')
            toolbarimg = pygame.image.load('res/system/graphics/toolbar.png')
            dialogimg = pygame.image.load('res/system/graphics/dialog.png')
            screen.fill([50, 50, 50])
            screen.blit(pygame.transform.smoothscale(bg, [width, height]), [0, 0])
            screen.blit(pythonpoweredl, centerobj(200, 80, screen))
            pygame.display.update()
            global red
            global green
            global blue
            global black
            global white
            global gray
            global lightgray
            global darkwhite
            red = [255, 0, 0]
            green = [0, 255, 0]
            blue = [0, 0, 255]
            black = [0, 0, 0]
            white = [255, 255, 255]
            gray = [50, 50, 50]
            lightgray = [100, 100, 100]
            darkwhite = [200, 200, 200]
            global darktheme
            global lighttheme
            global blacktheme
            global whitetheme
            darktheme = [gray, lightgray, darkwhite]
            lighttheme = [darkwhite, lightgray, gray]
            blacktheme = [black, gray, lightgray]
            whitetheme = [white, black, darkwhite]
            global fps
            fps = int(pvar.read('FPS', 'SETTINGS')[0])
            c.logentry('GUI', 'Finished.')
        except:
            print 'Python OS Graphics Loader has experienced a fatal error!'
            print 'Saving error code to temp/graphics-boot-traceback.txt'
            import sys
            import traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            nf = c.makefile('temp/graphics-boot-traceback.txt')
            tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
            for ln in tb:
                print >> nf, ln
            nf.close()
            c.logentry('GUI', 'Error while booting. Traceback at temp/graphics-boot-traceback.txt')
            print 'Traceback saved.'
            print 'Killing graphics...'
            commands.parsecmd(['killg'])
            print 'Exiting.'
            return
        if nod == False:
            try:
                daemonman().atstart()
            except:
                print 'Cannot start daemons!'
                print 'Saving error code to temp/daemon-boot-traceback.txt'
                import sys
                import traceback
                exc_type, exc_value, exc_traceback = sys.exc_info()
                nf = c.makefile('temp/daemon-boot-traceback.txt')
                tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
                for ln in tb:
                    print >> nf, ln
                nf.close()
                c.logentry('GUI', 'Cannot start all daemons. Traceback at temp/daemon-boot-traceback.txt')
                print 'Traceback saved.'
                print 'Killing graphics...'
                commands.parsecmd(['killg'])
                print 'Exiting.'
                return
            gui().desktop()
        return
    def desktop(self):
        global ctheme
        global bg
        global width
        global height
        lps().new('Home')
        ctheme = 'darktheme'
        bg = pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0])
        apps = pvar.read('DESKTOPAPPS', 'SETTINGS')
        bg = pygame.transform.smoothscale(bg, [width - 2, height - 20])
        appicon = pygame.image.load('res/system/graphics/icons/grayapps.png')
        icntr = centerobj(49, 49, screen)
        while True:
            clock.tick(fps)
            mousepos = pygame.mouse.get_pos()
            appsurface.blit(bg, [0, 0])
            abtn = components().imagebutton(icntr[0], height - 149, appicon, appsurface)
            screen.blit(appsurface, [2, 20])
            try:
                components().toolbar()
                components().menu()
            except:
                error()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    commands.parsecmd(['killg'])
                    lps().end()
                    return
                if event.type == pygame.VIDEORESIZE:
                    width = event.w
                    height = event.h
                    icntr = centerobj(49, 49, screen)
                    bg = pygame.transform.smoothscale(bg, [width - 2, height - 20])
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        if lps().listall()[len(lps().listall()) - 2] != 'Python OS System':
                            lps().end()
                            return
                        if lps().listall()[len(lps().listall()) - 2] == 'Python OS System':
                            pyexit()
                    if event.key == pygame.K_MENU:
                        mousepos = [1, 200]
                        pygame.mouse.set_pos(mousepos)
                    if event.key == pygame.K_RSUPER or event.key == pygame.K_LSUPER:
                        gui().apps()
                    if event.key == pygame.K_F1:
                        lps().end()
                        lps().end()
                        screen.fill([0, 0, 0])
                        pygame.display.update()
                        return
                    if event.key == pygame.K_F2:
                        gotfps = clock.get_fps()
                        c.logentry('INFO', 'FPS of Home is: ' + str(gotfps))
                        components().okbox('FPS Evaluation', ['Your framerate is: ' + str(gotfps) + '.', 'Python OS is set to reach: ' + str(fps), 'Change this in Settings >> Change FPS of Applications.'])
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        if mousepos[0] >= 0 and mousepos[0] <= 20:
                            if mousepos[1] >= 0 and mousepos[1] <= 20:
                                if lps().listall()[len(lps().listall()) - 2] != 'Python OS System':
                                    lps().end()
                                    return
                                if lps().listall()[len(lps().listall()) - 2] == 'Python OS System':
                                    pyexit()
                        if mousepos[0] >= abtn[0] and mousepos[0] <= abtn[1]:
                            if mousepos[1] >= abtn[2] and mousepos[1] <= abtn[3]:
                                gui().apps()
                    
    def apps(self):
        global ctheme
        global width
        global height
        lps().new('Applications')
        ctheme = 'darktheme'
        try:
            if pvar.read('TRANSPARENT', 'APPS')[0] == 'False':
                bg = pygame.transform.smoothscale(pygame.image.load(pvar.read('DESKTOPBG', 'SETTINGS')[0]), [width, height - 20])
            if pvar.read('TRANSPARENT', 'APPS')[0] == 'True':
                pygame.image.save(appsurface, 'data/system/apps-bg.jpg')
                bg = pygame.transform.smoothscale(pygame.image.load('data/system/apps-bg.jpg'), [width, height - 20])
        except:
            components().okbox('Invalid Settings', ['Wallpaper/Transparency Settings invalid.'])
            bg = pygame.Surface([width - 2, height - 20])
            bg.fill([200, 200, 200])
        clickables = [[]]
        page = 0
        w = 40
        h = 10
        for a in installedappsfinal:
            clickables[page].append([[w, w + 50, h, h + 60], a[2], a[1], a[0]])
            if w + 100 >= width - 60 and h + 70 >= height - 80:
                clickables.append([])
                page = page + 1
                w = 40
                h = 10
            elif w + 100 < width - 60:
                w = w + 100
            elif w + 100 >= width - 60:
                w = 40
                h = h + 70
        page = 0
        while True:
            clock.tick(fps)
            appsurface.blit(bg, [0, 0])
            mousepos = pygame.mouse.get_pos()
            for app in clickables[page]:
                aicon = pygame.image.load(app[2])
                appsurface.blit(aicon, [app[0][0], app[0][2]])
                aname = font.render(app[3], 1, (25, 25, 25))
                tcenter = centerobj(aname.get_width(), aname.get_height(), aicon)
                appsurface.blit(aname, [app[0][0] + tcenter[0], app[0][2] + 49])
            npb = components().button(width - 50, height - 40, ' > ')
            ppb = components().button(30, height - 40, ' < ')
            indicator = font.render(str(page + 1) + ' of ' + str(len(clickables)), 1, (50, 50, 50))
            appsurface.blit(indicator, [centerobj(indicator.get_width(), indicator.get_height(), appsurface)[0], height - 40])
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        lps().end() 
                        return
                    if event.key == pygame.K_RIGHT:
                        if page < len(clickables) - 1:
                            page = page + 1
                    if event.key == pygame.K_LEFT:
                        if page > 0:
                            page = page - 1
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for abutton in clickables[page]:
                        if mousepos[0] >= abutton[0][0] and mousepos[0] <= abutton[0][1]:
                            if mousepos[1] >= abutton[0][2] and mousepos[1] <= abutton[0][3]:
                                if event.button == 1:
                                    try:
                                        commands.parsecmd([abutton[1]])
                                    except:
                                        error()
                                    lps().end()
                                    return
                                if event.button == 3:
                                    if gui().rcm_app(abutton[1]) == 'uninstall':
                                        lps().end()
                                        return
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            lps().end()
                            return
                    if mousepos[0] >= npb[0] and mousepos[0] <= npb[1]:
                        if mousepos[1] >= npb[2] and mousepos[1] <= npb[3]:
                            if page < len(clickables) - 1:
                                page = page + 1
                    if mousepos[0] >= ppb[0] and mousepos[0] <= ppb[1]:
                        if mousepos[1] >= ppb[2] and mousepos[1] <= ppb[3]:
                            if page > 0:
                                page = page - 1
    def rcm_app(self, execp):
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
        rcmbg = pygame.image.load('temp/rcmbg.jpg')
        mp = pygame.mouse.get_pos()
        while True:
            clock.tick(fps)
            appsurface.blit(rcmbg, [0, 0])
            appsurface.blit(rightclickmenuimg, mp)
            addtol = components().button(mp[0] + 2, mp[1] + 2, 'Add to Launcher')
            rem = components().button(mp[0] + 2, mp[1] + 25, 'Uninstall')
            run = components().button(mp[0] + 2, mp[1] + 48, 'Open')
            tlb = components().button(mp[0] + 2, mp[1] + 71, 'Add to Toolbar')
            hlp = components().button(mp[0] + 2, mp[1] + 94, 'Help')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            mousepos = pygame.mouse.get_pos()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] >= addtol[0] and mousepos[0] <= addtol[1]:
                        if mousepos[1] >= addtol[2] and mousepos[1] <= addtol[3]:
                            pvar.append('SIDEMENUITEMS', execp, 'SIDEMENU')
                            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            sidemenufinalitems = []
                            for i in sidemenuitems:
                                rf = c.readfile('res/' + i + '/req.txt', 'r')
                                icon = rf[3]
                                name = rf[4]
                                execp = rf[2]
                                sidemenufinalitems.append([name, icon, execp])
                            return 'done'
                    if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                        if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                            if components().ynbox('Do you really want to uninstall?', ['You are about to uninstall:', execp, 'Do you wish to proceed with uninstallation?']) == 'Yes':
                                commands.parsecmd(['uninstall', execp, '-n'])
                                name = execp
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                for a in sidemenuitems:
                                    if a == name:
                                        pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                        for ca in sidemenuitems:
                                            if ca != name and ca != 'appmenu':
                                                pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                sidemenufinalitems = []
                                for i in sidemenuitems:
                                    rf = c.readfile('res/' + i + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    sidemenufinalitems.append([name, icon, execp])
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                components().okbox('Application Uninstalled', ['The application has been uninstalled.'])
                                reload_data(True)
                                return 'uninstall'
                            return
                    if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                        if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                            commands.parsecmd([execp])
                            return 'done'
                    if mousepos[0] >= hlp[0] and mousepos[0] <= hlp[1]:
                        if mousepos[1] >= hlp[2] and mousepos[1] <= hlp[3]:
                            textedit().gui(True, 'res/' + execp + '/help.txt', 'Help')
                            return
                    if mousepos[0] >= tlb[0] and mousepos[0] <= tlb[1]:
                        if mousepos[1] >= tlb[2] and mousepos[1] <= tlb[3]:
                            toolbaradd(execp)
                            return 'done'
                    if mousepos[0] >= mp[0] + 152 and mousepos[0] <= width:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return 'done'
                    if mousepos[0] >= 2 and mousepos[0] <= mp[0] + 2:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return 'done'
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= 0 and mousepos[1] <= mp[1] - 20:
                            return 'done'
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                            return 'done'
    def rcm_sidemenu(self, execp):
        global sidemenufinalitems
        global sidemenuitems
        global installedapps
        global installedappsfinal
        rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
        rcmbg = pygame.image.load('temp/rcmbg.jpg')
        mp = pygame.mouse.get_pos()
        while True:
            clock.tick(fps)
            appsurface.blit(rcmbg, [0, 0])
            appsurface.blit(rightclickmenuimg, mp)
            rln = components().button(mp[0] + 2, mp[1] + 2, 'Remove Link')
            rem = components().button(mp[0] + 2, mp[1] + 25, 'Uninstall')
            run = components().button(mp[0] + 2, mp[1] + 48, 'Open')
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            mousepos = pygame.mouse.get_pos()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if mousepos[0] >= rln[0] and mousepos[0] <= rln[1]:
                        if mousepos[1] >= rln[2] and mousepos[1] <= rln[3]:
                            insm = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            pvar.destroy('SIDEMENUITEMS', 'SIDEMENU')
                            pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                            for app in insm:
                                if app != execp and app != 'appmenu':
                                    pvar.append('SIDEMENUITEMS', app, 'SIDEMENU')
                            sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                            sidemenufinalitems = []
                            for i in sidemenuitems:
                                rf = c.readfile('res/' + i + '/req.txt', 'r')
                                icon = rf[3]
                                name = rf[4]
                                execp = rf[2]
                                sidemenufinalitems.append([name, icon, execp])
                            return
                    if mousepos[0] >= rem[0] and mousepos[0] <= rem[1]:
                        if mousepos[1] >= rem[2] and mousepos[1] <= rem[3]:
                            if components().ynbox('Do you really want to uninstall?', ['You are about to uninstall:', execp, 'Do you wish to proceed with uninstallation?']) == 'Yes':
                                commands.parsecmd(['uninstall', execp, '-n'])
                                name = execp
                                installedapps = pvar.read('INSTALLEDAPPS', 'GLOBAL')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                for a in sidemenuitems:
                                    if a == name:
                                        pvar.make('SIDEMENUITEMS', 'appmenu', 'SIDEMENU')
                                        for ca in sidemenuitems:
                                            if ca != name and ca != 'appmenu':
                                                pvar.append('SIDEMENUITEMS', ca, 'SIDEMENU')
                                sidemenuitems = pvar.read('SIDEMENUITEMS', 'SIDEMENU')
                                sidemenufinalitems = []
                                for i in sidemenuitems:
                                    rf = c.readfile('res/' + i + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    sidemenufinalitems.append([name, icon, execp])
                                installedappsfinal = []
                                for inst in installedapps:
                                    rf = c.readfile('res/' + inst + '/req.txt', 'r')
                                    icon = rf[3]
                                    name = rf[4]
                                    execp = rf[2]
                                    if rf[3] != 'noicon':
                                        installedappsfinal.append([name, icon, execp])
                                components().okbox('Application Uninstalled', ['The application has been uninstalled.'])
                                reload_data(True)
                            return
                    if mousepos[0] >= run[0] and mousepos[0] <= run[1]:
                        if mousepos[1] >= run[2] and mousepos[1] <= run[3]:
                            commands.parsecmd([execp])
                            return
                    if mousepos[0] >= mp[0] + 152 and mousepos[0] <= width:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= 2 and mousepos[0] <= mp[0] + 2:
                        if mousepos[1] >= 0 and mousepos[1] <= height:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= 0 and mousepos[1] <= mp[1] - 20:
                            return
                    if mousepos[0] >= mp[0] + 2 and mousepos[0] <= mp[0] + 152:
                        if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                            return

def error():
    import traceback
    import sys
    exc_type, exc_value, exc_traceback = sys.exc_info()
    tb = traceback.format_exc()
    tbf = c.makefile('temp/traceback.txt')
    print >> tbf, tb
    tbf.close()
    c.logentry('ERROR', 'Python OS has experienced an error that stopped the normal flow of operations.')
    c.logentry('INFO', "A transcript of the error's traceback is at temp/traceback.txt.")
    rtb = c.readfile('temp/traceback.txt', 'r')
    screen.fill([50, 50, 50])
    screen.blit(font.render('The application ' + lps().listall()[len(lps().listall()) - 1] + ' has crashed. Press Space key to continue.', 1, (255, 100, 100)), [20, 20])
    h = 40
    for ln in rtb:
        screen.blit(font.render(ln, 1, (200, 200, 200)), [20, h])
        h = h + 16
    h = h + 40
    oh = h
    for app in lps().listall():
        screen.blit(font.render(app, 1, (200, 200, 200)), [20, h])
        h = h + 16
    pygame.display.update()
    done = False
    while done != True:
        clock.tick(5)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    done = True
    screen.blit(font.render('Python OS has experienced an error. Quit graphics? (Y/N)', 1, (200, 200, 200)), [20, h])
    pygame.display.update()
    done = False
    while done != True:
        clock.tick(5)
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_y:
                    qg = True
                    done = True
                if event.key == pygame.K_n:
                    qg = False
                    done = True
    if qg == True:
        commands.parsecmd(['killg'])
    lps().end()
    return
class daemonman():
    def atstart(self):
        dstarts = pvar.read('DAEMONS', 'DMAN')
        for dstart in dstarts:
            daemonman().add(dstart)
    def add(self, name):
        global daemons
        dinf = c.readfile('res/' + name + '/daemon.txt')
        eval(dinf[1])
        c.logentry('DMAN', 'Added daemon ' + name + ' @ ' + dinf[0])
        daemons.append([name, dinf[0]])
    def delete(self, name):
        global daemons
        nd = []
        for daemon in daemons:
            if daemon[0] == name:
                continue
            nd.append(daemon)
        daemons = nd
    def run(self):
        for daemon in daemons:
            eval(daemon[1])
    def retall(self):
        return pvar.read('DAEMONS', 'DMAN')
def sysinfo():
    components().okbox('System Information', ['Python OS 4.1', 'Version: 4.1.0', 'Kernel: 1.1-pyos4', 'GServer: 1.01'])
    return
