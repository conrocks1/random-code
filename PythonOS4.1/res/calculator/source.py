def calculator():
    lps().new('Calculator')
    pygame.image.save(appsurface, 'temp/cbg.jpg')
    cbg = pygame.image.load('temp/cbg.jpg')
    inf = pvar.read('INFO', 'CALCULATOR')
    cpos = [20, 20]
    if len(inf) - 1 > 0:
        if inf[1] != '0.0' and inf[1] != '':
            try:
                showing = inf[1]
                snum = int(inf[1])
                n2 = int(inf[2])
                op = inf[3]
                stage = int(inf[4])
            except:
                showing = inf[1]
                snum = float(inf[1])
                n2 = float(inf[2])
                op = inf[3]
                stage = int(inf[4])
        elif inf[1] == '0.0' or inf[1] == '':
            inf = ['calc-info']
    if len(inf) - 1 == 0:
        showing = ''
        snum = 0.0
    n2 = 0.0
    op = '+'
    stage = 0
    res = 0
    dragging = False
    while True:
        if fps <= 30 and fps >= 15:
            clock.tick(fps - 5)
        elif fps <= 10:
            clock.tick(fps)
        appsurface.blit(cbg, [0, 0])
        pygame.draw.rect(appsurface, [75, 75, 75], [cpos[0], cpos[1], 110, 240])
        appsurface.blit(font.render('Calculator', 1, (200, 200, 200)), [cpos[0] + 2, cpos[1] + 2])
        bar = components().inputbutton(cpos[0] + 2, cpos[1] + 20, showing)
        b1 = components().button(cpos[0] + 2, bar[3] + 2, ' 1 ')
        b2 = components().button(b1[1] + 2, bar[3] + 2, ' 2 ')
        b3 = components().button(b2[1] + 2, bar[3] + 2, ' 3 ')
        b4 = components().button(cpos[0] + 2, b1[3] + 2, ' 4 ')
        b5 = components().button(b4[1] + 2, b1[3] + 2, ' 5 ')
        b6 = components().button(b5[1] + 2, b1[3] + 2, ' 6 ')
        b7 = components().button(cpos[0] + 2, b4[3] + 2, ' 7 ')
        b8 = components().button(b7[1] + 2, b4[3] + 2, ' 8 ')
        b9 = components().button(b8[1] + 2, b4[3] + 2, ' 9 ')
        b0 = components().button(cpos[0] + 2, b7[3] + 2, ' 0 ')
        bp = components().button(b3[1] + 2, bar[3] + 2, ' + ')
        bm = components().button(b6[1] + 2, bp[3] + 2, '  - ')
        bmu = components().button(b9[1] + 2, bm[3] + 2, ' * ')
        bc = components().button(b0[1] + 2, b7[3] + 2, ' C ')
        be = components().button(bc[1] + 2, b7[3] + 2, ' = ')
        bd = components().button(be[1] + 2, bmu[3] + 2, ' / ')
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        if dragging == True:
            cpos = [mousepos[0] - 2, mousepos[1] - 20]
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONUP:
                if dragging == True:
                    dragging = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        return
                if mousepos[0] >= bar[0] and mousepos[0] <= bar[1]:
                    if mousepos[1] >= bar[2] and mousepos[1] <= bar[3]:
                        if stage == 0:
                            snum = components().getinput(bar[0], bar[2] - 20, str(snum))
                            if snum.endswith('.0'):
                                snum = int(float(snum))
                            elif snum.endswith('.0') == False:
                                snum = float(snum)
                            showing = str(snum)
                        if stage == 2:
                            n2 = components().getinput(bar[0], bar[2] - 20, str(n2))
                            if n2.endswith('.0'):
                                n2 = int(float(n2))
                            elif n2.endswith('.0') == False:
                                n2 = float(n2)
                            showing = str(n2)
                if mousepos[0] < cpos[0] + 2 and mousepos[0] >= 2:
                    if mousepos[1] >= 20 and mousepos[1] <= height:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] > cpos[0] + 127 and mousepos[0] <= width:
                    if mousepos[1] >= 20 and mousepos[1] <= height:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] >= 0 and mousepos[0] <= width:
                    if mousepos[1] >= 20 and mousepos[1] < cpos[1] + 20:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] >= 0 and mousepos[0] <= width:
                    if mousepos[1] > cpos[1] + 260 and mousepos[1] <= height:
                        pvar.make('INFO', 'calc-info', 'CALCULATOR')
                        pvar.append('INFO', str(snum), 'CALCULATOR')
                        pvar.append('INFO', str(n2), 'CALCULATOR')
                        pvar.append('INFO', op, 'CALCULATOR')
                        pvar.append('INFO', str(stage), 'CALCULATOR')
                        lps().end()
                        return
                if mousepos[0] >= cpos[0] + 2 and mousepos[0] <= cpos[0] + 112:
                    if mousepos[1] >= cpos[1] + 20 and mousepos[1] <= cpos[1] + 40:
                        dragging = True
                if mousepos[0] >= bp[0] and mousepos[0] <= bp[1]:
                    if mousepos[1] >= bp[2] and mousepos[1] <= bp[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '+'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bm[0] and mousepos[0] <= bm[1]:
                    if mousepos[1] >= bm[2] and mousepos[1] <= bm[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '-'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bmu[0] and mousepos[0] <= bmu[1]:
                    if mousepos[1] >= bmu[2] and mousepos[1] <= bmu[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '*'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bd[0] and mousepos[0] <= bd[1]:
                    if mousepos[1] >= bd[2] and mousepos[1] <= bd[3]:
                        if stage == 0:
                            stage = 1
                        if stage == 1:
                            op = '/'
                            stage = 2
                            showing = ''
                if mousepos[0] >= bc[0] and mousepos[0] <= bc[1]:
                    if mousepos[1] >= bc[2] and mousepos[1] <= bc[3]:
                        stage = 0
                        snum = 0.0
                        showing = ''
                        n2 = 0.0
                        op = '+'
                if mousepos[0] >= be[0] and mousepos[0] <= be[1]:
                    if mousepos[1] >= be[2] and mousepos[1] <= be[3]:
                        if stage == 2:
                            if op == '+':
                                res = snum + n2
                            if op == '-':
                                res = snum - n2
                            if op == '*':
                                res = snum * n2
                            if op == '/':
                                res = snum / n2
                            showing = str(res)
                            snum = res
                            stage = 0
                if mousepos[0] >= b1[0] and mousepos[0] <= b1[1]:
                    if mousepos[1] >= b1[2] and mousepos[1] <= b1[3]:
                        if stage == 0:
                            showing = showing + '1'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '1'
                            n2 = float(showing)
                if mousepos[0] >= b2[0] and mousepos[0] <= b2[1]:
                    if mousepos[1] >= b2[2] and mousepos[1] <= b2[3]:
                        if stage == 0:
                            showing = showing + '2'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '2'
                            n2 = float(showing)
                if mousepos[0] >= b3[0] and mousepos[0] <= b3[1]:
                    if mousepos[1] >= b3[2] and mousepos[1] <= b3[3]:
                        if stage == 0:
                            showing = showing + '3'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '3'
                            n2 = float(showing)
                if mousepos[0] >= b4[0] and mousepos[0] <= b4[1]:
                    if mousepos[1] >= b4[2] and mousepos[1] <= b4[3]:
                        if stage == 0:
                            showing = showing + '4'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '4'
                            n2 = float(showing)
                if mousepos[0] >= b5[0] and mousepos[0] <= b5[1]:
                    if mousepos[1] >= b5[2] and mousepos[1] <= b5[3]:
                        if stage == 0:
                            showing = showing + '5'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '5'
                            n2 = float(showing)
                if mousepos[0] >= b6[0] and mousepos[0] <= b6[1]:
                    if mousepos[1] >= b6[2] and mousepos[1] <= b6[3]:
                        if stage == 0:
                            showing = showing + '6'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '6'
                            n2 = float(showing)
                if mousepos[0] >= b7[0] and mousepos[0] <= b7[1]:
                    if mousepos[1] >= b7[2] and mousepos[1] <= b7[3]:
                        if stage == 0:
                            showing = showing + '7'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '7'
                            n2 = float(showing)
                if mousepos[0] >= b8[0] and mousepos[0] <= b8[1]:
                    if mousepos[1] >= b8[2] and mousepos[1] <= b8[3]:
                        if stage == 0:
                            showing = showing + '8'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '8'
                            n2 = float(showing)
                if mousepos[0] >= b9[0] and mousepos[0] <= b9[1]:
                    if mousepos[1] >= b9[2] and mousepos[1] <= b9[3]:
                        if stage == 0:
                            showing = showing + '9'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '9'
                            n2 = float(showing)
                if mousepos[0] >= b0[0] and mousepos[0] <= b0[1]:
                    if mousepos[1] >= b0[2] and mousepos[1] <= b0[3]:
                        if stage == 0:
                            showing = showing + '0'
                            snum = float(showing)
                        elif stage == 2:
                            showing = showing + '0'
                            n2 = float(showing)
                            
