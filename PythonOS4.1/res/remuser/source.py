def remuser():
    import shell
    import shutil
    if shell.grunning == False:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            name = raw_input ('Name of user to remove: ')
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            print 'You must be root to remove users.'
            return
    if shell.grunning == True:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            ask = components().ynbox('Sure to remove?', ['Are you sure you want to remove a user?'])
            if ask == 'Yes':
                name = components().inputbox('User Name', ['Name of user to remove:'])
            if ask != 'Yes':
                return
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            components().okbox('Must be root.', ['You must be root to remove users.'])
            return
    try:
        if name != 'root':
	    shutil.rmtree('user/' + name + '/')
	    shutil.rmtree('data/system/' + name + '/')
            users = pvar.read('USERS', 'GLOBAL')
            pvar.destroy('USERS', 'GLOBAL')
            pvar.make('USERS', 'root', 'GLOBAL')
            for user in users:
                if user != 'root' and user != name:
                    pvar.append('USERS', user, 'GLOBAL')
            return
    except:
        c.msg('Error removing user.', 'REMUSER')
