def toolbargui():
    while True:
        clock.tick(fps)
        toolbar = pygame.transform.smoothscale(toolbarimg, (width, 20))
        screen.blit(toolbar, [0, 0])
        screen.blit(font.render(str(datetime.datetime.now()).split()[1], 1, [200, 200, 200]), [width - 35, 2])
        screen.blit(font.render(lps().listall()[len(lps().listall()) - 1], 1, (200, 200, 200)), [25, 2])
        screen.blit(closeicon, [1, 0])
        bw = width - 35
        clickables = []
        for plg in toolbarplugs:
            ico = pygame.image.load(plg[0])
            bw = bw - ico.get_width()
            bw = bw - 2
            screen.blit(ico, [bw, 0])
            clickables.append([[bw, bw + ico.get_width(), 0, 20], plg[1]])
        mousepos = pygame.mouse.get_pos()
        components().mouse()
        components().menu()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                for abutton in clickables:
                    if mousepos[0] >= abutton[0][0] and mousepos[0] <= abutton[0][1]:
                        if mousepos[1] >= abutton[0][2] and mousepos[1] <= abutton[0][3]:
                            if event.button == 1:
                                commands.parsecmd([abutton[1]])
                                return
                            if event.button == 3:
                                toolbarman(abutton[1])
        if mousepos[1] >= 20:
            return
