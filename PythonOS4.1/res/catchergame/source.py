def CatcherGame():
    global width
    global height
    lps().new('Catcher Game')
    appsurface.fill([50, 50, 50])
    CatcherGameBackground = pygame.image.load('res/catchergame/background.png')
    CatcherGameUFO = pygame.image.load('res/catchergame/UFO.png')
    CatcherGameUFOWidth = 500
    CatcherGameUFOHeight = 500
    CatcherGamePlayer = pygame.image.load('res/catchergame/Player.png')
    CatcherGamePlayerWidth = 0
    CatcherGamePlayerHeight = 0
    CatcherGameItem = pygame.image.load('res/catchergame/Item.png')
    CatcherGameItemWidth = random.randint(100, 1366)
    CatcherGameItemHeight = random.randint(5, 768)
    CatcherGameGotItem = False
    cd = 3
    pygame.key.set_repeat(1, 1)
    while cd != 0:
        clock.tick(1)
        appsurface.blit(CatcherGameBackground, [0, 0])
        appsurface.blit(largebfont.render(str(cd), 1, (50, 50, 50)), centerobj(40, 40, appsurface))
        screen.blit(appsurface, [2, 20])
        pygame.display.update()
        cd = cd - 1
    while True:
        clock.tick(20)
        appsurface.blit(CatcherGameBackground, [0, 0])
        appsurface.blit(CatcherGamePlayer, [CatcherGamePlayerWidth, CatcherGamePlayerHeight])
        appsurface.blit(CatcherGameUFO, [CatcherGameUFOWidth, CatcherGameUFOHeight])
        appsurface.blit(CatcherGameItem, [CatcherGameItemWidth, CatcherGameItemHeight])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                    if mousepos[1] >= 0 and mousepos[1] <= 20:
                        lps().end()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    lps().end()
                    return
                if event.key == pygame.K_F4:
                    recovery(appsurface)
                if event.key == pygame.K_F3:
                    main.launcher(1, 1, [1, 1])
                if event.key == pygame.K_SPACE:
                    done = False
                    text = largefont.render('Paused.', 1, (100, 100, 100))
                    while done != True:
                        clock.tick(fps)
                        appsurface.blit(CatcherGameBackground, [0, 0])
                        appsurface.blit(CatcherGamePlayer, [CatcherGamePlayerWidth, CatcherGamePlayerHeight])
                        appsurface.blit(CatcherGameUFO, [CatcherGameUFOWidth, CatcherGameUFOHeight])
                        appsurface.blit(CatcherGameItem, [CatcherGameItemWidth, CatcherGameItemHeight])
                        appsurface.blit(text, [100, 20])
                        components().toolbar()
                        components().menu()
                        screen.blit(appsurface, [2, 20])
                        pygame.display.update()
                        for event in pygame.event.get():
                            if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_ESCAPE:
                                    lps().end()
                                    return
                                if event.key == pygame.K_SPACE:
                                    done = True
                if event.key == pygame.K_RIGHT:
                    CatcherGamePlayerWidth = CatcherGamePlayerWidth + 6
                if event.key == pygame.K_LEFT:
                    CatcherGamePlayerWidth = CatcherGamePlayerWidth - 6
                if event.key == pygame.K_DOWN:
                    CatcherGamePlayerHeight = CatcherGamePlayerHeight + 6
                if event.key == pygame.K_UP:
                    CatcherGamePlayerHeight = CatcherGamePlayerHeight - 6
        CatcherGameUFORangeWidth = CatcherGameUFOWidth + 40
        CatcherGameUFORangeHeight = CatcherGameUFOHeight + 40
        CatcherGamePlayerRangeWidth = CatcherGamePlayerWidth + 13
        CatcherGamePlayerRangeHeight = CatcherGamePlayerHeight + 10
        if CatcherGamePlayerWidth >= CatcherGameUFOWidth:
            CatcherGameUFOWidth = CatcherGameUFOWidth + 4
        if CatcherGamePlayerWidth <= CatcherGameUFOWidth:
            CatcherGameUFOWidth = CatcherGameUFOWidth - 4
        if CatcherGamePlayerHeight >= CatcherGameUFOHeight:
            CatcherGameUFOHeight = CatcherGameUFOHeight + 4
        if CatcherGamePlayerHeight <= CatcherGameUFOHeight:
            CatcherGameUFOHeight = CatcherGameUFOHeight - 4
        if CatcherGameUFOWidth <= 60:
            CatcherGameUFOWidth = CatcherGameUFOWidth + 5
        if CatcherGamePlayerWidth <= CatcherGameUFORangeWidth and CatcherGamePlayerWidth >= CatcherGameUFOWidth:
            if CatcherGamePlayerHeight <= CatcherGameUFORangeHeight and CatcherGamePlayerHeight >= CatcherGameUFOHeight:
                components().okbox('Game Over!', ['You got caught!'])
                lps().end()
                res = components().ynbox('Play Again?', [''])
                if res == 'Yes':
                    commands.parsecmd(['catchergame'])
                if res == 'No' or res == 'Exit':
                    return
                pygame.key.set_repeat(50, 50)
                return
        if CatcherGameItemWidth <= CatcherGamePlayerRangeWidth and CatcherGameItemWidth >= CatcherGamePlayerWidth:
            if CatcherGameItemHeight <= CatcherGamePlayerRangeHeight and CatcherGameItemHeight >= CatcherGamePlayerHeight:
                CatcherGameItemWidth = CatcherGamePlayerWidth
                CatcherGameItemHeight = CatcherGamePlayerHeight
                appsurface.blit(CatcherGameItem, [CatcherGamePlayerWidth, CatcherGamePlayerHeight])
        if CatcherGamePlayerWidth <= 0:
            CatcherGamePlayerWidth = CatcherGamePlayerWidth + 4
        if CatcherGamePlayerWidth >= width:
            CatcherGamePlayerWidth = CatcherGamePlayerWidth - 13
        if CatcherGamePlayerHeight <= 0:
            CatcherGamePlayerHeight = CatcherGamePlayerHeight + 4
        if CatcherGamePlayerHeight >= height:
            CatcherGamePlayerHeight = CatcherGamePlayerHeight - 10
        if CatcherGameItemWidth <= 60:
            components().okbox('You Won!', ['You evaded the Catcher!'])
            lps().end()
            res = components().ynbox('Play Again?', [''])
            if res == 'Yes':
                commands.parsecmd(['catchergame'])
            if res == 'No' or res == 'Exit':
                return
            pygame.key.set_repeat(50, 50)
            return
