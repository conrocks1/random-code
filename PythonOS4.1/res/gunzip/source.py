def gunzip(fp = 'find'):
    import zipfile
    if fp == 'find':
        components().okbox('Select the .zip file.', ['Select the .zip file you wish to unpack.'])
        fp = files('return')
    zf = zipfile.ZipFile(fp, 'r')
    nlst = zf.namelist()
    components().okbox('Select Destination', ['Select the directory to unpack to.'])
    dest = files('return', 'directory')
    for item in nlst:
        c.msg('Extracting ' + item, 'ZEXTRACT')
        zf.extract(item, dest)
    zf.close()
    components().okbox('Extract Complete.', ['.zip extraction complete.'])
