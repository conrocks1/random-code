def pyexit():
    r = components().ynbox('Sure to Exit?', ['Are you sure you want to exit?', 'This will close all:', 'Active Files', 'Active Applications'])
    if r == 'Yes':
        try:
            pvar.read('NSR', 'NESTEDSESSION')
            commands.parsecmd(['exit'])
        except:
            commands.parsecmd(['killg'])
            return
