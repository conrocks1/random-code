def cleanup():
    import shell
    c.msg('Looking for files to delete.', 'CLEANUP')
    todel = ['commands.pyc', 'constants.pyc', 'shell.pyc', 'PythonOS.pyc']
    index = c.readfile('data/system/index.txt')
    for cand in index:
        if c.fileordir(cand) == 'File':
            if cand.endswith('.pyc') == True:
                todel.append(cand)
            if cand.endswith('~') == True:
                todel.append(cand)
            if cand.startswith('temp/') == True:
                todel.append(cand)
    c.msg('Will delete ' + str(len(todel)) + ' files.', 'CLEANUP')
    for f in todel:
        try:
            os.remove(f)
            c.msg('Removed ' + f, 'CLEANUP')
        except:
            c.msg('Failed to remove ' + f, 'CLEANUP')
    if shell.grunning == True:
        components().okbox('Cleanup finished.', ['Cleanup has removed ' + str(len(todel)) + ' junk files.'])
    if shell.grunning != True:
        c.msg('Deleted ' + str(len(todel)) + ' junk files.', 'CLEANUP') 
