def toolbaradd(name = None):
    global toolbarplugstarters
    global toolbarplugs
    if name == None:
        if shell.grunning == True:
            name = components().inputbox('Plugin to Add.', ['Enter the executable path of the toolbar plugin.'])
        if shell.grunning == False:
            name = raw_input ('Plugin executable command: ')
    try:
        f = open('res/' + name + '/toolbar.txt', 'r')
        f.close()
    except:
        c.msg('Error. This app does not have a plugin.', 'ERROR')
        return
    pvar.append('TOOLBARPLUGINS', name, 'GLOBAL')
    toolbarplugstarters = pvar.read('TOOLBARPLUGINS', 'GLOBAL')
    toolbarplugs = []
    for plg in toolbarplugstarters:
        rf = c.readfile('res/' + plg + '/toolbar.txt')
        icon = rf[0]
        run = rf[1]
        toolbarplugs.append([icon, run])
