def volumeplugin():
    global vol
    rcmbg = pygame.image.save(appsurface, 'temp/rcmbg.jpg')
    rcmbg = pygame.image.load('temp/rcmbg.jpg')
    mp = pygame.mouse.get_pos()
    mp = [mp[0] - 50, mp[1]]
    pygame.mixer.init()
    while True:
        clock.tick(fps)
        appsurface.blit(rcmbg, [0, 0])
        appsurface.blit(rightclickmenuimg, [mp[0] - 50, mp[1]])
        p = components().button(mp[0] - 48, mp[1] + 2, ' + ')
        m = components().button(mp[0] - 20, mp[1] + 2, ' - ')
        appsurface.blit(largefont.render(str(vol * 10), 1, (50, 50, 50)), [mp[0] - 46, mp[1] + 50])
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        mousepos = pygame.mouse.get_pos()
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= p[0] and mousepos[0] <= p[1]:
                    if mousepos[1] >= p[2] and mousepos[1] <= p[3]:
                        vol = vol + 0.1
                        pygame.mixer.music.set_volume(vol)
                if mousepos[0] >= m[0] and mousepos[0] <= m[1]:
                    if mousepos[1] >= m[2] and mousepos[1] <= m[3]:
                        vol = vol - 0.1
                        pygame.mixer.music.set_volume(vol)  
                if mousepos[0] >= mp[0] and mousepos[0] <= width:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] <= mp[0] - 50 and mousepos[0] >= 2:
                    if mousepos[1] >= 0 and mousepos[1] <= height:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= 0 and mousepos[1] <= mp[1]:
                        return
                if mousepos[0] >= mp[0] - 50 and mousepos[0] <= mp[0]:
                    if mousepos[1] >= mp[1] + 280 and mousepos[1] <= height:
                        return
