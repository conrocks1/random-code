def sysshell():
    import shell
    import subprocess
    opsys = pvar.read('CURROS', 'KERNEL')[0]
    if opsys == 'win32':
        sym = '> '
    elif opsys == 'linux2':
        sym = '$ '
    elif opsys != 'win32' or opsys != 'linux2':
        sym = '$ '
    if shell.grunning == False:
        cmd = raw_input (sym)
    if shell.grunning == True:
        cmd = components().inputbox('Enter Command.', ['Enter the command you wish to execute.'])
        if cmd == 'Cancel':
            return
    os.system(cmd)
