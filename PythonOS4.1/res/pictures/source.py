def pictures(path):
    lps().new('Pictures')
    if path == '':
        path = files('return')
    op = pygame.image.load(path)
    if op.get_width() > width:
        if op.get_height > height - 20:
            op = pygame.transform.smoothscale(op, [width, height])
    iw = op.get_width()
    ih = op.get_height()
    bp = centerobj(iw, ih, appsurface)
    while True:
        clock.tick(fps)
        appsurface.fill([50, 50, 50])
        appsurface.blit(op, bp)
        screen.blit(appsurface, [2, 20])
        components().toolbar()
        components().menu()
        pygame.display.update()
        mousepos = pygame.mouse.get_pos()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
		    lps().end()
                    return
            if event.type == pygame.MOUSEBUTTONDOWN:
                if mousepos[0] >= 0 and mousepos[0] <= 20:
                   if mousepos[1] >= 0 and mousepos[1] <= 20:
		       lps().end()
                       return
