def adduser():
    import shell
    if shell.grunning == False:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            name = raw_input ('Desired Username: ')
            rname = raw_input ('Your name: ')
            pwd = c.shiftchar(raw_input ('Your desired password (lowercase letters only): '))
            other = []
            print 'Enter any other desired information, then write "done"'
            uin = ''
            while uin != 'done':
                uin = raw_input ()
                other.append(uin)
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            print 'You must be root to add users.'
    if shell.grunning == True:
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] == 'root':
            ask = components().ynbox('Add a new user?', ['Do you want to add a new user?']) 
            if ask == 'Yes':
                name = components().inputbox('Username', ['Your desired username:'])
                rname = components().inputbox('Real Name', ['Your name:'])
                pwd = c.shiftchar(components().inputbox('Password', ['Your desired password (lowercase letters only):']))
                other = []
                components().okbox('Other Information', ['Enter any other desired information, then write "done"'])
                uin = ''
                h = 86
                while uin != 'done':
                    uin = components().inputbox('Other info', ['Any other information you wish to submit:'])
                    other.append(uin)
                    h = h + 28
            if ask != 'Yes':
                return
        if pvar.read('CURRENTUSER', 'GLOBAL')[len(pvar.read('CURRENTUSER', 'GLOBAL')) - 1] != 'root':
            components().okbox('Must be root.', ['You must be root to add users.'])
    try:
        import shutil
        shutil.copytree('res/system/default-vars/', 'data/system/' + name + '/')
        os.mkdir('user/' + name + '/')
        up = 'user/' + name + '/'
        os.mkdir(up + 'apps/')
        os.mkdir(up + 'Downloads/')
        os.mkdir(up + 'Documents/')
        os.mkdir(up + 'Pictures/')
        os.mkdir(up + 'Music/')
        random_str = ['pythonos', 'password', '123456', 'l0g1n', 'pyth0nos', '123abc']
        import random
        fp1 = c.shiftchar(random_str[random.randint(0, 5)])
        fp2 = c.shiftchar(random_str[random.randint(0, 5)])
        pf = c.makefile(up + name + '.pwd')
        print >> pf, fp1
        print >> pf, pwd
        print >> pf, fp2
        pf.close()
        infof = c.makefile(up + 'user.txt')
        print >> infof, rname
        for ln in other:
	    if ln != 'done':
                print >> infof, ln
        infof.close()
        pvar.append('USERS', name, 'GLOBAL')
    except:
        c.msg('Error creating user.', 'ADDUSR')
