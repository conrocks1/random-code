class notifyd():
    def setup(self):
        c.logentry('NOTIFYD', 'Notification Daemon Set Up.')
        qfile = c.makefile('data/notifyd/queue.txt')
        print >> qfile, 'Welcome, ' + c.readfile('user/' + shell.user + '/user.txt')[0]
        print >> qfile, 'Python OS'
        qfile.close()
        global notifyd_var_ticks
        global notifyd_var_trem
        global notifyd_var_count
        notifyd_var_ticks = 0
        notifyd_var_trem = fps * 10
        notifyd_var_count = 0
        c.logentry('NOTIFYD', 'Queue: data/notifyd/queue.txt')
        c.logentry('NOTIFYD', 'Trem: ' + str(notifyd_var_trem))
        c.logentry('NOTIFYD', 'Count: ' + str(notifyd_var_count))
        c.logentry('NOTIFYD', 'Ticks: ' + str(notifyd_var_ticks))
    def notify(self, text, app = 'Notification'):
        qfile = open('data/notifyd/queue.txt', 'a+')
        print >> qfile, text
        print >> qfile, app
        qfile.close()
    def run(self):
        global notifyd_var_ticks
        global notifyd_var_trem
        global notifyd_var_count
        if notifyd_var_ticks == 100 or notifyd_var_count > 0:
            notifyd_var_ticks = 0
            queue = c.readfile('data/notifyd/queue.txt')
            if len(queue) - 1 >= 0:
                if notifyd_var_count <= notifyd_var_trem:
                    cpos = 0
                    w = width - 400
                    h = 25
                    while cpos <= len(queue) - 2:
                        pygame.draw.rect(screen, [75, 75, 75], [w, h, 390, 60])
                        screen.blit(font.render(queue[cpos], 1, (200, 200, 200)), [w + 4, h + 5])
                        screen.blit(bfont.render(queue[cpos + 1], 1, (200, 200, 200)), [w + 4, h + 35])
                        pygame.display.update()
                        cpos = cpos + 2
                        h = h + 62
                    notifyd_var_count = notifyd_var_count + 1
                elif notifyd_var_count > notifyd_var_trem:
                    queue = c.makefile('data/notifyd/queue.txt')
                    queue.close()
                    notifyd_var_count = 0
                    notifyd_var_ticks = 0
        elif notifyd_var_ticks < 100:
            notifyd_var_ticks = notifyd_var_ticks + 1
                
