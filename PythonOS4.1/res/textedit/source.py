class textedit():
    def gui(self, vom = False, f = None, title = 'Text Editor'):
        lps().new(title)
        if f == None and vom == False:
            fo = files('return', 'directory')
            f = fo + components().inputbox('Name', ['Enter the file name and extension.'])
        try:
            open(f, 'rU').close()
            b = c.readfile(f)
            b.append('')
        except:
            b = ['Delete and start typing.']
        page = 0
        typing = False
        oh = 25
        c.varman().rem('input-exitmode')
        while True:
            clock.tick(fps)
            appsurface.fill([200, 200, 200])
            pygame.draw.rect(appsurface, [50, 50, 50], [0, 0, width, 23])
            tod = textedit().displaygen(b)
            if vom == False:
                sb = components().button(2, 1, 'Save')
            nb = components().button(200, 1, 'Next Page')
            pb = components().button(280, 1, 'Previous Page')
            db = components().button(380, 1, 'Delete a Line')
            cb = []
            h = 25
            for itm in tod[page]:
                if vom == False:
                    cb.append([components().button(2, h, itm), itm])
                    h = h + 25
                    if typing == False:
                        oh = (len(cb) - 1) * 25
                if vom == True:
                    appsurface.blit(font.render(itm, 1, (50, 50, 50)), [2, h])
                    h = h + 25
            screen.blit(appsurface, [2, 20])
            components().toolbar()
            components().menu()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    mousepos = pygame.mouse.get_pos()
                    if vom == False:
                        if mousepos[0] >= sb[0] and mousepos[0] <= sb[1]:
                            if mousepos[1] >= sb[2] and mousepos[1] <= sb[3]:
                                if vom == False:
                                    import shutil
                                    try:
                                        shutil.copy(f, 'temp/txteditf.bak')
                                    except:
                                        pass
                                    nf = c.makefile(f)
                                    for ln in b:
                                        print >> nf, ln
                                    nf.close()
                    if mousepos[0] >= nb[0] and mousepos[0] <= nb[1]:
                        if mousepos[1] >= nb[2] and mousepos[1] <= nb[3]:
                            if page + 1 <= len(tod) - 1:
                                page = page + 1
                    if mousepos[0] >= pb[0] and mousepos[0] <= pb[1]:
                        if mousepos[1] >= pb[2] and mousepos[1] <= pb[3]:
                            if page - 1 >= 0:
                                page = page - 1
                    if mousepos[0] >= db[0] and mousepos[0] <= db[1]:
                        if mousepos[1] >= db[2] and mousepos[1] <= db[3]:
                            if vom == False:
                                l = int(components().inputbox('Line to Delete', ['Number of the line to delete.'])) - 1
                                nb = []
                                p = 0
                                for item in b:
                                    if p != l:
                                        nb.append(item)
                                    p = p + 1
                                c.copylist(nb)
                    if mousepos[0] >= 0 and mousepos[0] <= 20:
                        if mousepos[1] >= 0 and mousepos[1] <= 20:
                            if vom == False:
                                import shutil
                                try:
                                    shutil.copy(f, 'temp/txteditf.bak')
                                except:
                                    pass
                                nf = c.makefile(f)
                                for ln in b:
                                    print >> nf, ln
                                nf.close()
                            lps().end()
                            return
                    for btn in cb:
                        if mousepos[0] >= btn[0][0] and mousepos[0] <= width:
                            if mousepos[1] >= btn[0][2] and mousepos[1] <= btn[0][3]:
                                oh = btn[0][2]
                                txt = components().getinput(0, oh - 20, btn[1])
                                if btn[1] == '':
                                    pos = c.findinlist(cb, btn)
                                if btn[1] != '':
                                    pos = c.findinlist(b, btn[1])
                                b[pos] = txt
                                c.varman().make('typing-cpos', pos)
                                oh = oh + 25
            if c.varman().get('input-exitmode') == 'enter':
                typing = True
            if c.varman().get('input-exitmode') == 'click':
                typing = False
            if typing == True:
                pos = c.varman().get('typing-cpos')
                while typing == True:
                    clock.tick(fps)
                    appsurface.fill([200, 200, 200])
                    pygame.draw.rect(appsurface, [50, 50, 50], [0, 0, width, 23])
                    sb = components().button(2, 1, 'Save')
                    nb = components().button(200, 1, 'Next Page')
                    pb = components().button(280, 1, 'Previous Page')
                    db = components().button(380, 1, 'Delete a Line')
                    ci = 0
                    bh = 25
                    for itm in b:
                        if ci != pos + 1:
                            appsurface.blit(font.render(itm, 1, (50, 50, 50)), [2, bh])
                            bh = bh + 25
                        if ci == pos + 1:
                            appsurface.blit(font.render(itm, 1, (50, 50, 50)), [2, bh + 25])
                            bh = bh + 50
                        ci = ci + 1
                    txt = components().getinput(0, oh - 20, '')
                    oh = oh + 25
                    if pos < len(b) - 1:
                        nb = []
                        p = 0
                        for item in b:
                            nb.append(item)
                            if p == pos:
                                nb.append(txt)
                            p = p + 1
                        b = c.copylist(nb)
                        pos = pos + 1
                    elif pos == len(b) - 1:
                        b.append(txt)
                        pos = pos + 1
                    if c.varman().get('input-exitmode') == 'enter':
                        typing = True
                    if c.varman().get('input-exitmode') == 'click':
                        typing = False
    def displaygen(self, b):
        pages = [[]]
        page = 0
        h = 25
        for itm in b:
            if h + 50 <= height:
                pages[page].append(itm)
                h = h + 25
            elif h + 50 > height:
                page = page + 1
                pages.append([])
                pages[page].append(itm)
                h = 25
        return pages
