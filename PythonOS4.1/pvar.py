import constants
constants.logentry('pvar', 'Permanent variable system used.')
def make(name, content, app):
    var = constants.makefile('var/' + app + '-' + name + '.var')
    print >> var, content
    var.close()
    path = 'var/' + app + '-' + name + '.var'
    return path

def read(name, app):
    try:
        var = open('var/' + app + '-' + name + '.var', 'r')
        var.close()
    except:
        raise IOError
    return constants.readfile('var/' + app + '-' + name + '.var')

def append(name, content, app):
    lines = constants.readfile('var/' + app + '-' + name + '.var')
    lines.append(content)
    if read('CURROS', 'KERNEL')[0] == 'win32':
        var = open('var/' + app + '-' + name + '.var', 'a')
        print >> var, content
        var.close()
    if read('CURROS', 'KERNEL')[0] != 'win32':
        destroy(name, app)
        var = open('var/' + app + '-' + name + '.var', 'w')
        for line in lines:
            print >> var, line
        var.close()

def destroy(name, app):
    constants.deletefile('var/' + app + '-' + name + '.var')
    
