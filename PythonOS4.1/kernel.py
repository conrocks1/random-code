try:
    import constants
    log = constants.makefile('data/system/syslog.txt')
    print >> log, 'Python OS 4.1.0 System Log'
    print >> log, '--------------------------'
    print >> log, '     '
    log.close()
    constants.msg('Python OS 4.1.0', 'KERNEL')
    constants.msg('Kernel Version 1.1-pyos4', 'KERNEL')
    constants.logentry('KERNEL', 'kernel.py started.')
    constants.msg('Importing required modules', 'KERNEL')
    import pvar
    pvar.make('CURRENTUSER', 'root', 'GLOBAL')
    import shell
    import sys
    import os
    import gc
    gc.enable()
    constants.msg('Running on top of ' + sys.platform, 'KERNEL')
    pvar.make('CURROS', sys.platform, 'KERNEL')
    pvar.make('VERSION', '4.1.00\n4\n1.0\n1.1', 'KERNEL')
    constants.msg('Starting Shell', 'KERNEL')
    constants.msg('Indexing File System', 'KERNEL')
    constants.index()
    constants.logentry('KERNEL', 'filesystem indexed.')
    try:
        f = open('firstrun.inf', 'r')
        f.close()
        constants.logentry('KERNEL', 'First run detected.')
        but = constants.readfile('res/system/documentation/Basic Usage.txt')
        if but != ['Cannot read the file']:
            for ln in but:
                print ln
        if but == ['Cannot read the file']:
            constants.msg('Welcome to Python OS 4!', 'INFO')
            constants.msg('Log in as: root, password is pythonos.', 'INFO')
            constants.msg('Set up your user account by typing "adduser" into the command prompt.', 'INFO')
        constants.msg('Documentation for use can be found under res/system/documentation/', 'INFO')
        constants.msg('Python OS is configuring...', 'PYCONFIG')
        pvar.make('DESKTOPBG', 'res/system/graphics/bg.png', 'SETTINGS')
        pvar.make('DESKTOPAPPS', '', 'SETTINGS')
        pvar.make('RUNG', 'False', 'SETTINGS')
        pvar.make('SIDEMENUITEMS', 'pyexit', 'SIDEMENU')
        constants.addcmd('system', 'print "Python OS v4.0.0"')
        constants.logentry('KERNEL', 'System configured.')
        constants.deletefile('firstrun.inf')
    except:
        constants.msg('Configured.', 'KERNEL')
    constants.logentry('KERNEL', 'Going to user signon.')
    shell.interact().signon()
    tsbu = int(pvar.read('TIMES_FROM_BACKUP', 'KERNEL')[0])
    if tsbu >= 5:
        constants.makepoint()
        pvar.make('TIMES_FROM_BACKUP', '0', 'KERNEL')
    elif tsbu < 5:
        pvar.make('TIMES_FROM_BACKUP', str(tsbu + 1), 'KERNEL')
    dbm = pvar.read('DEBUGMODE', 'KERNEL')[0]
    if dbm == 'True':
        dbgm = True
    if dbm == 'False':
        dbgm = False
    constants.varman().make('debugmode', dbgm)
    if dbgm == True:
        constants.msg('Debug Mode is on. Turn off in Settings.')
    constants.varman().make('debug-out', ['Debugging Started'])
    constants.logentry('KERNEL', 'Starting shell environment.')
    shell.interact().sh()
    gc.collect()
    constants.logentry('KERNEL', 'Garbage collector deleted ' + str(gc.get_count()))
    gcg = constants.makefile('temp/gc_garbage.txt')
    for line in gc.garbage:
        print >> gcg, line
    gcg.close()
    constants.logentry('KERNEL', 'Exit.')
except:
    print 'Kernel Panic! An error has occurred in Python OS!'
    print 'Trying to evaluate the error:'
    try:
        import sys
        import traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print 'Traceback:'
        traceback.print_exception(exc_type, exc_value, exc_traceback)
        nf = open('temp/traceback.txt', 'w')
        tb = traceback.format_exception(exc_type, exc_value, exc_traceback)
        for ln in tb:
            print >> nf, ln
        nf.close()
        print 'Saved to temp/traceback.txt'
    except:
        print 'Save Failed!'
    print 'Trying panic shell. "exit" to quit.'
    import commands
    cmd = ''
    while cmd != 'exit':
        cmd = raw_input ('> ')
        try:
            commands.parsecmd(cmd.split())
        except:
            print 'Error Executing.'
