__author__ = 'Connor'


import math
import SqauresRectangles
import Triangles
import CubeCuboid
import Circles
import Prisms
import Spheres

from tkinter import *

# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):

        # parameters that you want to send through the Frame class.
        Frame.__init__(self, master)

        #reference to the master widget, which is the tk window
        self.master = master

        #with that, we want to then run init_window, which doesn't yet exist
        self.init_window()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget
        self.master.title("AREA,PERIMETER & VOLUME CALCULATOR")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a button instance
        quitButton = Button(self, text="Exit",command=self.client_exit)
        squareButton = Button(self, text="Squares And Rectangles",command=self.client_square)
        triangleButton = Button(self,text="Triangles",command=self.client_triangle)
        circleButton = Button(self, text="Circles",command=self.client_circle)
        cubeButton = Button(self, text="Cubes And Cuboids",command=self.client_cube)
        prismButton = Button(self, text="Prisms",command=self.client_prism)
        sphereButton = Button(self, text="Spheres",command=self.client_sphere)

        # placing the button on my window
        quitButton.place(x=200, y=200)
        squareButton.place(x=75, y=40)
        triangleButton.place(x=100, y=70)
        cubeButton.place(x=80, y=100)
        prismButton.place(x=105, y=130)
        sphereButton.place(x=101, y=160)



    def client_exit(self):
        exit()
    def client_square(self):
        SqauresRectangles.SqauresRectangles()
    def client_triangle(self):
        Triangles.Triangles()
    def client_circle(self):
        Circles.Circle()
    def client_cube(self):
        CubeCuboid.CubeCuboid()
    def client_prism(self):
        Prisms.Prisms()
    def client_sphere(self):
        Spheres.Spheres()



# root window created. Here, that would be the only window, but
# you can later have windows within windows.
root = Tk()

root.geometry("300x300")

#creation of an instance
app = Window(root)

#mainloop
root.mainloop()

###################################################################################

print ('            Area, Perimeter And Volume Calculator!')
print ('Welcome To The Area, Perimeter And Volume Calculator!')
print ('Please Select An Option From Below')
print ('To Work Out Squares And Rectangles Press 1')
print ('To Work Out Triangles Press 2')
print ('To Work Out Circles Press 3')
print ('To Work Out Cubes And Cuboids  Press 4')
print ('To Work Out Prisms Press 5')
print ('To Work Out Spheres Press 6')

option = int(input())

if option == 1:
    SqauresRectangles.SqauresRectangles()

if option == 2:
     Triangles.Triangles()

if option == 3:
     Circles.Circle()

if option == 4:
     CubeCuboid.CubeCuboid()

if option == 5:
    Prisms.Prisms()

if option == 6:
     Spheres.Sphere()





