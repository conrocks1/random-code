2.
import simplegui

3.
# define global variables

4.
timer=None

5.
time=0

6.
message="0:00.0"

7.
attempts=0

8.
success=0

9.
started=False

10.
 

11.
# define helper function format that converts time

12.
# in tenths of seconds into formatted string A:BC.D

13.
def format(t):

14.
    global message

15.
    D=t%10

16.
    b=t/10

17.
    A=int(b/60)

18.
    BC=int(b%60)    

19.
    message =str(A)+":"+('%02d' % BC)+"."+str(D)

20.
    

21.
# define event handlers for buttons; "Start", "Stop", "Reset"

 22.
def start():

 23.
    timer.start()

 24.
    global started

 25.
    started=True

 26.
 

 27.
def stop():

 28.
    timer.stop()

 29.
    global attempts

 30.
    global success

 31.
    global label

 32.
    global started

 33.
    if started:

 34.
        if time%10==0:

 35.
            success=success+1

 36.
        started=False

 37.
        attempts=attempts+1

 38.
        

39.
def reset():

 40.
    global time

 41.
    global started

 42.
    global attempts

 43.
    global success

 44.
    started=0

 45.
    attempts=0

 46.
    success=0

 47.
    timer.stop()

 48.
    time=0

 49.
    format(time)

 50.
 

 51.
# define event handler for timer with 0.1 sec interval

 52.
def timer_handler():

 53.
    global time

 54.
    time=time+1

 55.
    format(time)

 56.
 

 57.
# define draw handler

 58.
def draw(canvas):

 59.
    canvas.draw_text(message, [60,90], 30, "Black")

 60.
    canvas.draw_text(str(success)+"/"+str(attempts), [150,30], 20, "Blue")

 61.
    

62.
# create frame

 63.
frame = simplegui.create_frame("Home", 200, 150)

 64.
frame.set_canvas_background("White")

 65.
 

 66.
# register event handlers

 67.
frame.add_button("Start", start, 100)

 68.
frame.add_button("Stop", stop, 100)

 69.
frame.add_button("Reset", reset, 100)

 70.
 

 71.
frame.set_draw_handler(draw)

 72.
timer = simplegui.create_timer(100, timer_handler)

 73.
# start frame

 74.
frame.start()

 75.
 

 76.
# Please remember to review the grading rubric
